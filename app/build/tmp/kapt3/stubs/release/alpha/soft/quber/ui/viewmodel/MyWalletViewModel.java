package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000bJ\u0006\u0010\u0006\u001a\u00020\u0005R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/MyWalletViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_openAddCard", "Landroidx/lifecycle/MutableLiveData;", "", "openAddCard", "Landroidx/lifecycle/LiveData;", "getOpenAddCard", "()Landroidx/lifecycle/LiveData;", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "init", "preferencesUtil", "app_release"})
public final class MyWalletViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.utils.PreferencesUtil preference;
    private final androidx.lifecycle.MutableLiveData<kotlin.Unit> _openAddCard = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<kotlin.Unit> openAddCard = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<kotlin.Unit> getOpenAddCard() {
        return null;
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil) {
    }
    
    public final void openAddCard() {
    }
    
    public MyWalletViewModel() {
        super();
    }
}