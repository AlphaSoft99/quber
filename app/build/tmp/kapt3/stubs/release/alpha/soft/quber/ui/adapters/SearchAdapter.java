package alpha.soft.quber.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0002\u0013\u0014B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010\t\u001a\u00020\nH\u0016J\u001c\u0010\u000b\u001a\u00020\f2\n\u0010\r\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u001c\u0010\u000f\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0016R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lalpha/soft/quber/ui/adapters/SearchAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lalpha/soft/quber/ui/adapters/SearchAdapter$ViewHolder;", "data", "", "Lcom/yandex/mapkit/GeoObjectCollection$Item;", "itemInterface", "Lalpha/soft/quber/ui/adapters/SearchAdapter$ItemInterface;", "(Ljava/util/List;Lalpha/soft/quber/ui/adapters/SearchAdapter$ItemInterface;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ItemInterface", "ViewHolder", "app_release"})
public final class SearchAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<alpha.soft.quber.ui.adapters.SearchAdapter.ViewHolder> {
    private final java.util.List<com.yandex.mapkit.GeoObjectCollection.Item> data = null;
    private final alpha.soft.quber.ui.adapters.SearchAdapter.ItemInterface itemInterface = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public alpha.soft.quber.ui.adapters.SearchAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.adapters.SearchAdapter.ViewHolder holder, int position) {
    }
    
    public SearchAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends com.yandex.mapkit.GeoObjectCollection.Item> data, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.adapters.SearchAdapter.ItemInterface itemInterface) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lalpha/soft/quber/ui/adapters/SearchAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lalpha/soft/quber/ui/adapters/SearchAdapter;Landroid/view/View;)V", "bind", "", "item", "Lcom/yandex/mapkit/GeoObjectCollection$Item;", "app_release"})
    public final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.yandex.mapkit.GeoObjectCollection.Item item) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lalpha/soft/quber/ui/adapters/SearchAdapter$ItemInterface;", "", "itemClick", "", "pos", "", "app_release"})
    public static abstract interface ItemInterface {
        
        public abstract void itemClick(int pos);
    }
}