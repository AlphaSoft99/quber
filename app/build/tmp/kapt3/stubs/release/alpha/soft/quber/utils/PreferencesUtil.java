package alpha.soft.quber.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b.\u0018\u00002\u00020\u0001:\u0001SB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\u0006\u0010!\u001a\u00020 J\u0006\u0010\"\u001a\u00020\u001eJ\u0006\u0010#\u001a\u00020 J\b\u0010$\u001a\u0004\u0018\u00010\u0007J\u0006\u0010%\u001a\u00020&J\u0006\u0010\'\u001a\u00020\u0007J\b\u0010(\u001a\u0004\u0018\u00010\u0007J\u0006\u0010)\u001a\u00020 J\u0006\u0010*\u001a\u00020 J\u0006\u0010+\u001a\u00020\u0007J\b\u0010,\u001a\u0004\u0018\u00010\u0007J\n\u0010-\u001a\u0004\u0018\u00010\u001cH\u0002J\u0006\u0010.\u001a\u00020\u0007J\u0006\u0010/\u001a\u00020\u0007J\u0006\u00100\u001a\u00020 J\u0006\u00101\u001a\u00020 J\u0006\u00102\u001a\u00020 J\u0006\u00103\u001a\u00020 J\u000e\u00103\u001a\u00020\u001e2\u0006\u00104\u001a\u00020 J\u0006\u00105\u001a\u00020 J\u0006\u00106\u001a\u00020 J \u00107\u001a\u00020\u001e2\u0006\u00108\u001a\u00020\u00072\u0006\u00109\u001a\u00020\u00072\u0006\u0010:\u001a\u00020\u0007H\u0007J\u000e\u0010;\u001a\u00020\u001e2\u0006\u0010<\u001a\u00020\u0007J\u000e\u0010=\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\u0010\u0010>\u001a\u00020\u001e2\u0006\u00108\u001a\u00020\u0007H\u0002J\u000e\u0010?\u001a\u00020\u001e2\u0006\u0010@\u001a\u00020 J\u000e\u0010A\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\u000e\u0010B\u001a\u00020\u001e2\u0006\u00108\u001a\u00020\u0007J\u000e\u0010C\u001a\u00020\u001e2\u0006\u0010D\u001a\u00020 J\u0010\u0010E\u001a\u00020\u001e2\b\u0010F\u001a\u0004\u0018\u00010\u0007J\u000e\u0010G\u001a\u00020\u001e2\u0006\u0010H\u001a\u00020&J\u000e\u0010I\u001a\u00020\u001e2\u0006\u0010J\u001a\u00020 J\u000e\u0010K\u001a\u00020\u001e2\u0006\u0010L\u001a\u00020 J\u000e\u0010M\u001a\u00020\u001e2\u0006\u0010N\u001a\u00020\u0007J\u000e\u0010O\u001a\u00020\u001e2\u0006\u0010P\u001a\u00020\u0007J\u0010\u0010Q\u001a\u00020\u001e2\u0006\u0010R\u001a\u00020\u0007H\u0007R\u000e\u0010\t\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006T"}, d2 = {"Lalpha/soft/quber/utils/PreferencesUtil;", "", "context", "Landroid/content/Context;", "preferences", "Landroid/content/SharedPreferences;", "securedPreferencesName", "", "(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;)V", "ABOUT_ROAD", "COMMENTS_NOTIFICATION_ACTIVE", "FIREBASE_TOKEN", "FRIEND_PROMO_CODE", "HAS_NEW_ORDER", "HAS_PROFILE", "IS_CHOOSE_LANGUAGE", "LOCATION", "NEW_COMMENT_NOTIFICATION_ID", "NEW_ORDER_NOTIFICATION", "NOTIFICATION_CHECKED", "PREFS_FIRST_NAME", "PREFS_IS_FIRST_LAUNCH", "PREFS_PHONE_NUMBER", "PREFS_UI_LOCALE", "PROMO_CODE", "VERSION_APP", "authTokenCached", "secure", "Lalpha/soft/quber/utils/PreferencesUtil$Secure;", "aboutRoad", "", "b", "", "checkOnline", "clearAll", "getAboutRoad", "getAuthToken", "getCommentsNotification", "", "getFToken", "getLocation", "getNewOrderNotification", "getNotification", "getPhoneNumber", "getPromoCode", "getSecure", "getUiLocale", "getVersionApp", "hasAccount", "hasAuthToken", "hasNewOrder", "hasProfile", "has", "isChooseLanguage", "isFirstLaunch", "saveTokenData", "token", "phone", "fullName", "saveUser", "user", "setAccount", "setAuthToken", "setChooseLanguage", "choose", "setCommentsNotificationActive", "setFToken", "setFirstLaunch", "isFirst", "setLocation", "location", "setNewCommentsNotification", "longExtra", "setNewOrderNotification", "isBoolean", "setNotification", "checked", "setPhoneNumber", "phoneNumber", "setPromoCode", "promoCode", "setUiLocale", "locale", "Secure", "app_release"})
public final class PreferencesUtil {
    private final java.lang.String ABOUT_ROAD = "About road";
    private final java.lang.String NEW_ORDER_NOTIFICATION = "NEW_ORDER_NOTIFICATION";
    private final java.lang.String COMMENTS_NOTIFICATION_ACTIVE = "COMMENTS_NOTIFICATION_ACTIVE";
    private final java.lang.String PREFS_IS_FIRST_LAUNCH = "isFirstLaunch";
    private final java.lang.String PREFS_PHONE_NUMBER = "phoneNumber";
    private final java.lang.String PREFS_FIRST_NAME = "firstName";
    private final java.lang.String FIREBASE_TOKEN = "firebase token";
    private final java.lang.String NEW_COMMENT_NOTIFICATION_ID = "NEW_COMMENT_NOTIFICATION_ID";
    private final java.lang.String PREFS_UI_LOCALE = "uiLocale";
    private final java.lang.String HAS_PROFILE = "has profile";
    private final java.lang.String NOTIFICATION_CHECKED = "isChecked";
    private final java.lang.String LOCATION = "location";
    private final java.lang.String FRIEND_PROMO_CODE = "friend promo code";
    private final java.lang.String PROMO_CODE = "promo code";
    private final java.lang.String VERSION_APP = "VERSION_APP";
    private final java.lang.String IS_CHOOSE_LANGUAGE = "is language";
    private final java.lang.String HAS_NEW_ORDER = "has new order";
    private alpha.soft.quber.utils.PreferencesUtil.Secure secure;
    private java.lang.String authTokenCached;
    private final android.content.Context context = null;
    private final android.content.SharedPreferences preferences = null;
    private final java.lang.String securedPreferencesName = null;
    
    @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.GINGERBREAD)
    public final void saveTokenData(@org.jetbrains.annotations.NotNull()
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    java.lang.String phone, @org.jetbrains.annotations.NotNull()
    java.lang.String fullName) {
    }
    
    public final void clearAll() {
    }
    
    public final boolean hasAuthToken() {
        return false;
    }
    
    private final void setAuthToken(java.lang.String token) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAuthToken() {
        return null;
    }
    
    public final boolean isFirstLaunch() {
        return false;
    }
    
    public final void setFirstLaunch(boolean isFirst) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPhoneNumber() {
        return null;
    }
    
    public final void setPhoneNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String phoneNumber) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUiLocale() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"NewApi"})
    public final void setUiLocale(@org.jetbrains.annotations.NotNull()
    java.lang.String locale) {
    }
    
    private final alpha.soft.quber.utils.PreferencesUtil.Secure getSecure() {
        return null;
    }
    
    public final boolean hasProfile() {
        return false;
    }
    
    public final void hasProfile(boolean has) {
    }
    
    public final boolean getNotification() {
        return false;
    }
    
    public final void setNotification(boolean checked) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLocation() {
        return null;
    }
    
    public final void setLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String location) {
    }
    
    public final boolean isChooseLanguage() {
        return false;
    }
    
    public final void setChooseLanguage(boolean choose) {
    }
    
    public final void setPromoCode(@org.jetbrains.annotations.NotNull()
    java.lang.String promoCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPromoCode() {
        return null;
    }
    
    public final boolean hasNewOrder() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFToken() {
        return null;
    }
    
    public final void setFToken(@org.jetbrains.annotations.NotNull()
    java.lang.String token) {
    }
    
    public final void setNewCommentsNotification(long longExtra) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getVersionApp() {
        return null;
    }
    
    public final void setCommentsNotificationActive(boolean b) {
    }
    
    public final long getCommentsNotification() {
        return 0L;
    }
    
    public final void aboutRoad(boolean b) {
    }
    
    public final boolean getAboutRoad() {
        return false;
    }
    
    public final boolean getNewOrderNotification() {
        return false;
    }
    
    public final void setNewOrderNotification(boolean isBoolean) {
    }
    
    public final boolean hasAccount() {
        return false;
    }
    
    public final void setAccount(boolean b) {
    }
    
    public final boolean checkOnline() {
        return false;
    }
    
    public final void saveUser(@org.jetbrains.annotations.NotNull()
    java.lang.String user) {
    }
    
    public PreferencesUtil(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences preferences, @org.jetbrains.annotations.NotNull()
    java.lang.String securedPreferencesName) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0002\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u0010\u001a\u00020\u0011J\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0013R(\u0010\t\u001a\u0004\u0018\u00010\u00052\b\u0010\b\u001a\u0004\u0018\u00010\u00058F@GX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lalpha/soft/quber/utils/PreferencesUtil$Secure;", "", "context", "Landroid/content/Context;", "name", "", "password", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V", "token", "authToken", "getAuthToken", "()Ljava/lang/String;", "setAuthToken", "(Ljava/lang/String;)V", "securePreferences", "Lcom/securepreferences/SecurePreferences;", "clearAll", "", "hasAccount", "", "setAccount", "b", "Companion", "app_release"})
    static final class Secure {
        private com.securepreferences.SecurePreferences securePreferences;
        private static final java.lang.String AUTH_TOKEN = "auth_token";
        private static final java.lang.String LANGUAGE = "language";
        private static final java.lang.String TYPE_ROLE = "role";
        public static final alpha.soft.quber.utils.PreferencesUtil.Secure.Companion Companion = null;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAuthToken() {
            return null;
        }
        
        @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.GINGERBREAD)
        public final void setAuthToken(@org.jetbrains.annotations.Nullable()
        java.lang.String token) {
        }
        
        public final void clearAll() {
        }
        
        public final boolean hasAccount() {
            return false;
        }
        
        public final void setAccount(boolean b) {
        }
        
        public Secure(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String name, @org.jetbrains.annotations.NotNull()
        java.lang.String password) {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lalpha/soft/quber/utils/PreferencesUtil$Secure$Companion;", "", "()V", "AUTH_TOKEN", "", "LANGUAGE", "TYPE_ROLE", "app_release"})
        public static final class Companion {
            
            private Companion() {
                super();
            }
        }
    }
}