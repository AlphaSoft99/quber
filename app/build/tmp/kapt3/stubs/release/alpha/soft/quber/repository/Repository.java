package alpha.soft.quber.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\n"}, d2 = {"Lalpha/soft/quber/repository/Repository;", "", "apiService", "Lalpha/soft/quber/network/PostApi;", "(Lalpha/soft/quber/network/PostApi;)V", "getDataFromApi", "Lretrofit2/Response;", "", "Lalpha/soft/quber/model/PostData;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_release"})
@javax.inject.Singleton()
public final class Repository {
    private final alpha.soft.quber.network.PostApi apiService = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getDataFromApi(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<java.util.List<alpha.soft.quber.model.PostData>>> p0) {
        return null;
    }
    
    @javax.inject.Inject()
    public Repository(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi apiService) {
        super();
    }
}