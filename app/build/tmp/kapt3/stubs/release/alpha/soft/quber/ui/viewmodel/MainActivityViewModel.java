package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0012\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\r\u001a\u00020\u0006J\u0006\u0010\u0011\u001a\u00020\u0006J\u000e\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u001f\u001a\u00020\tJ\u0006\u0010\u0018\u001a\u00020\u0006J\u0006\u0010\u001a\u001a\u00020\u0006J\u0006\u0010\u001c\u001a\u00020\u0006R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00060\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\t0\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0010R\u001a\u0010\u0014\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0017\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00060\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0010R\u001d\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0010R\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00060\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0010\u00a8\u0006 "}, d2 = {"Lalpha/soft/quber/ui/viewmodel/MainActivityViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_closeDrawer", "Landroidx/lifecycle/MutableLiveData;", "Lalpha/soft/quber/utils/Event;", "", "_hideProgress", "_isLockDrawer", "", "_logout", "_openDrawer", "_showProgress", "closeDrawer", "Landroidx/lifecycle/LiveData;", "getCloseDrawer", "()Landroidx/lifecycle/LiveData;", "hideProgress", "getHideProgress", "isLockDrawer", "isOpen", "()Z", "setOpen", "(Z)V", "logout", "getLogout", "openDrawer", "getOpenDrawer", "showProgress", "getShowProgress", "isLock", "b", "app_release"})
public final class MainActivityViewModel extends androidx.lifecycle.ViewModel {
    private boolean isOpen;
    private final androidx.lifecycle.MutableLiveData<kotlin.Unit> _showProgress = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<kotlin.Unit> showProgress = null;
    private final androidx.lifecycle.MutableLiveData<kotlin.Unit> _hideProgress = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<kotlin.Unit> hideProgress = null;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _closeDrawer = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> closeDrawer = null;
    private final androidx.lifecycle.MutableLiveData<kotlin.Unit> _logout = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<kotlin.Unit> logout = null;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _openDrawer = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> openDrawer = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _isLockDrawer = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Boolean> isLockDrawer = null;
    
    public final boolean isOpen() {
        return false;
    }
    
    public final void setOpen(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<kotlin.Unit> getShowProgress() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<kotlin.Unit> getHideProgress() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> getCloseDrawer() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<kotlin.Unit> getLogout() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> getOpenDrawer() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> isLockDrawer() {
        return null;
    }
    
    public final void isLock(boolean b) {
    }
    
    public final void closeDrawer() {
    }
    
    public final void showProgress() {
    }
    
    public final void hideProgress() {
    }
    
    public final void openDrawer() {
    }
    
    public final void logout() {
    }
    
    public MainActivityViewModel() {
        super();
    }
}