package alpha.soft.quber.ui.screens;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00e4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010>\u001a\u00020?H\u0002J\u0010\u0010@\u001a\u00020?2\u0006\u0010A\u001a\u00020\u0000H\u0002J\b\u0010B\u001a\u00020?H\u0002J\u0016\u0010C\u001a\u00020?2\f\u0010D\u001a\b\u0012\u0004\u0012\u00020F0EH\u0016J\u0010\u0010G\u001a\u00020?2\u0006\u0010D\u001a\u00020HH\u0016J\u0010\u0010I\u001a\u00020?2\u0006\u0010J\u001a\u00020KH\u0016J\u0010\u0010L\u001a\u00020?2\u0006\u0010M\u001a\u00020KH\u0016J\u0018\u0010N\u001a\u00020?2\u0006\u0010M\u001a\u00020K2\u0006\u0010O\u001a\u00020PH\u0016J\b\u0010Q\u001a\u00020?H\u0016J\u0010\u0010R\u001a\u00020?2\u0006\u0010D\u001a\u00020HH\u0016J\u0010\u0010S\u001a\u00020?2\u0006\u0010D\u001a\u00020TH\u0016J\b\u0010U\u001a\u00020?H\u0016J\b\u0010V\u001a\u00020?H\u0016J\u001a\u0010W\u001a\u00020?2\u0006\u0010M\u001a\u00020X2\b\u0010Y\u001a\u0004\u0018\u00010ZH\u0016J\b\u0010[\u001a\u00020?H\u0002J\u0010\u0010\\\u001a\u00020?2\u0006\u0010]\u001a\u00020\u0007H\u0002J\u0010\u0010\\\u001a\u00020?2\u0006\u0010]\u001a\u00020-H\u0002J\b\u0010^\u001a\u00020?H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010&\u001a\u00020\'8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u0014\u0010,\u001a\b\u0012\u0004\u0012\u00020-0$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020/X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u00100\u001a\u0004\u0018\u000101X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00102\u001a\b\u0012\u0004\u0012\u0002030\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u00104\u001a\u0004\u0018\u000105X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00106\u001a\u0004\u0018\u000107X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u00108\u001a\u0002098BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b<\u0010=\u001a\u0004\b:\u0010;\u00a8\u0006_"}, d2 = {"Lalpha/soft/quber/ui/screens/MapScreen;", "Landroidx/fragment/app/Fragment;", "Lcom/yandex/mapkit/search/Session$SearchListener;", "Lcom/yandex/mapkit/user_location/UserLocationObjectListener;", "Lcom/yandex/mapkit/directions/driving/DrivingSession$DrivingRouteListener;", "()V", "FINAL_LOCATION", "Lcom/yandex/mapkit/geometry/Point;", "TARGET_LOCATION", "api", "Lalpha/soft/quber/network/PostApi;", "getApi", "()Lalpha/soft/quber/network/PostApi;", "setApi", "(Lalpha/soft/quber/network/PostApi;)V", "bottomSheetBehavior", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "carsAdapter", "Lalpha/soft/quber/ui/adapters/CarsAdapter;", "carsData", "Ljava/util/ArrayList;", "Lalpha/soft/quber/model/CardData;", "drivingRouter", "Lcom/yandex/mapkit/directions/driving/DrivingRouter;", "drivingSession", "Lcom/yandex/mapkit/directions/driving/DrivingSession;", "mapObjects", "Lcom/yandex/mapkit/map/MapObjectCollection;", "mapView", "Lcom/yandex/mapkit/mapview/MapView;", "marker", "Lcom/yandex/mapkit/map/PlacemarkMapObject;", "myLocation", "", "postLiveDataObserver", "Landroidx/lifecycle/Observer;", "Lalpha/soft/quber/model/AddressData;", "preferencesUtil", "Lalpha/soft/quber/utils/PreferencesUtil;", "getPreferencesUtil", "()Lalpha/soft/quber/utils/PreferencesUtil;", "setPreferencesUtil", "(Lalpha/soft/quber/utils/PreferencesUtil;)V", "queryLiveDataObserver", "", "resultAdapter", "Lalpha/soft/quber/ui/adapters/SearchAdapter;", "searchManager", "Lcom/yandex/mapkit/search/SearchManager;", "searchResult", "Lcom/yandex/mapkit/GeoObjectCollection$Item;", "searchSession", "Lcom/yandex/mapkit/search/Session;", "userLocationLayer", "Lcom/yandex/mapkit/user_location/UserLocationLayer;", "viewModel", "Lalpha/soft/quber/ui/viewmodel/MapViewModel;", "getViewModel", "()Lalpha/soft/quber/ui/viewmodel/MapViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "hideSoftKeyboard", "", "injectDependency", "fragment", "loadData", "onDrivingRoutes", "p0", "", "Lcom/yandex/mapkit/directions/driving/DrivingRoute;", "onDrivingRoutesError", "Lcom/yandex/runtime/Error;", "onObjectAdded", "userLocationView", "Lcom/yandex/mapkit/user_location/UserLocationView;", "onObjectRemoved", "view", "onObjectUpdated", "event", "Lcom/yandex/mapkit/layers/ObjectEvent;", "onResume", "onSearchError", "onSearchResponse", "Lcom/yandex/mapkit/search/Response;", "onStart", "onStop", "onViewCreated", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "showSoftKeyboard", "submitQuery", "query", "submitRequest", "app_release"})
public final class MapScreen extends androidx.fragment.app.Fragment implements com.yandex.mapkit.search.Session.SearchListener, com.yandex.mapkit.user_location.UserLocationObjectListener, com.yandex.mapkit.directions.driving.DrivingSession.DrivingRouteListener {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.network.PostApi api;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.utils.PreferencesUtil preferencesUtil;
    private com.yandex.mapkit.geometry.Point TARGET_LOCATION;
    private com.yandex.mapkit.geometry.Point FINAL_LOCATION;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.yandex.mapkit.mapview.MapView mapView;
    private com.yandex.mapkit.map.PlacemarkMapObject marker;
    private com.yandex.mapkit.map.MapObjectCollection mapObjects;
    private com.yandex.mapkit.user_location.UserLocationLayer userLocationLayer;
    private com.yandex.mapkit.search.SearchManager searchManager;
    private com.yandex.mapkit.search.Session searchSession;
    private com.yandex.mapkit.directions.driving.DrivingRouter drivingRouter;
    private com.yandex.mapkit.directions.driving.DrivingSession drivingSession;
    private alpha.soft.quber.ui.adapters.SearchAdapter resultAdapter;
    private alpha.soft.quber.ui.adapters.CarsAdapter carsAdapter;
    private final java.util.ArrayList<com.yandex.mapkit.GeoObjectCollection.Item> searchResult = null;
    private final java.util.ArrayList<alpha.soft.quber.model.CardData> carsData = null;
    private boolean myLocation;
    private com.google.android.material.bottomsheet.BottomSheetBehavior<androidx.constraintlayout.widget.ConstraintLayout> bottomSheetBehavior;
    private final androidx.lifecycle.Observer<alpha.soft.quber.model.AddressData> postLiveDataObserver = null;
    private final androidx.lifecycle.Observer<java.lang.String> queryLiveDataObserver = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.network.PostApi getApi() {
        return null;
    }
    
    public final void setApi(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.utils.PreferencesUtil getPreferencesUtil() {
        return null;
    }
    
    public final void setPreferencesUtil(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil p0) {
    }
    
    private final alpha.soft.quber.ui.viewmodel.MapViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void injectDependency(alpha.soft.quber.ui.screens.MapScreen fragment) {
    }
    
    private final void loadData() {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onObjectAdded(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.user_location.UserLocationView userLocationView) {
    }
    
    @java.lang.Override()
    public void onObjectRemoved(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.user_location.UserLocationView view) {
    }
    
    @java.lang.Override()
    public void onObjectUpdated(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.user_location.UserLocationView view, @org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.layers.ObjectEvent event) {
    }
    
    private final void submitQuery(java.lang.String query) {
    }
    
    private final void submitQuery(com.yandex.mapkit.geometry.Point query) {
    }
    
    @java.lang.Override()
    public void onSearchError(@org.jetbrains.annotations.NotNull()
    com.yandex.runtime.Error p0) {
    }
    
    @java.lang.Override()
    public void onSearchResponse(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.search.Response p0) {
    }
    
    private final void showSoftKeyboard() {
    }
    
    private final void hideSoftKeyboard() {
    }
    
    private final void submitRequest() {
    }
    
    @java.lang.Override()
    public void onDrivingRoutesError(@org.jetbrains.annotations.NotNull()
    com.yandex.runtime.Error p0) {
    }
    
    @java.lang.Override()
    public void onDrivingRoutes(@org.jetbrains.annotations.NotNull()
    java.util.List<com.yandex.mapkit.directions.driving.DrivingRoute> p0) {
    }
    
    public MapScreen() {
        super();
    }
}