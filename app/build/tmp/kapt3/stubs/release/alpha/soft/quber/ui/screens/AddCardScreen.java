package alpha.soft.quber.ui.screens;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u0000H\u0002J\b\u0010\u0014\u001a\u00020\u000bH\u0002J\b\u0010\u0015\u001a\u00020\u000bH\u0016J\u001a\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\f\u001a\u00020\r8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u001b"}, d2 = {"Lalpha/soft/quber/ui/screens/AddCardScreen;", "Landroidx/fragment/app/Fragment;", "()V", "preferencesUtil", "Lalpha/soft/quber/utils/PreferencesUtil;", "getPreferencesUtil", "()Lalpha/soft/quber/utils/PreferencesUtil;", "setPreferencesUtil", "(Lalpha/soft/quber/utils/PreferencesUtil;)V", "saveCardObserver", "Landroidx/lifecycle/Observer;", "", "viewModel", "Lalpha/soft/quber/ui/viewmodel/AddCardViewModel;", "getViewModel", "()Lalpha/soft/quber/ui/viewmodel/AddCardViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "injectDependency", "fragment", "loadData", "onDestroy", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "app_release"})
public final class AddCardScreen extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.utils.PreferencesUtil preferencesUtil;
    private final kotlin.Lazy viewModel$delegate = null;
    private final androidx.lifecycle.Observer<kotlin.Unit> saveCardObserver = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.utils.PreferencesUtil getPreferencesUtil() {
        return null;
    }
    
    public final void setPreferencesUtil(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil p0) {
    }
    
    private final alpha.soft.quber.ui.viewmodel.AddCardViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void loadData() {
    }
    
    private final void injectDependency(alpha.soft.quber.ui.screens.AddCardScreen fragment) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public AddCardScreen() {
        super();
    }
}