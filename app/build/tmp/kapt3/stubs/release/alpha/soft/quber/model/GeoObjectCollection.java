package alpha.soft.quber.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Lalpha/soft/quber/model/GeoObjectCollection;", "", "featureMember", "", "Lalpha/soft/quber/model/FeatureMember;", "metaDataProperty", "Lalpha/soft/quber/model/MetaDataPropertyX;", "(Ljava/util/List;Lalpha/soft/quber/model/MetaDataPropertyX;)V", "getFeatureMember", "()Ljava/util/List;", "getMetaDataProperty", "()Lalpha/soft/quber/model/MetaDataPropertyX;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_release"})
public final class GeoObjectCollection {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<alpha.soft.quber.model.FeatureMember> featureMember = null;
    @org.jetbrains.annotations.NotNull()
    private final alpha.soft.quber.model.MetaDataPropertyX metaDataProperty = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<alpha.soft.quber.model.FeatureMember> getFeatureMember() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.MetaDataPropertyX getMetaDataProperty() {
        return null;
    }
    
    public GeoObjectCollection(@org.jetbrains.annotations.NotNull()
    java.util.List<alpha.soft.quber.model.FeatureMember> featureMember, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.MetaDataPropertyX metaDataProperty) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<alpha.soft.quber.model.FeatureMember> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.MetaDataPropertyX component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.GeoObjectCollection copy(@org.jetbrains.annotations.NotNull()
    java.util.List<alpha.soft.quber.model.FeatureMember> featureMember, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.MetaDataPropertyX metaDataProperty) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}