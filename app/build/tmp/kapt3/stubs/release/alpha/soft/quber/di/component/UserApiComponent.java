package alpha.soft.quber.di.component;

import java.lang.System;

@alpha.soft.quber.di.scope.AppScope()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\bH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\tH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\nH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u000bH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\fH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\rH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u000eH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u000fH&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0010H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0011H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0012H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0013H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0014H&J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0015H&\u00a8\u0006\u0016"}, d2 = {"Lalpha/soft/quber/di/component/UserApiComponent;", "", "inject", "", "app", "Lalpha/soft/quber/App;", "fragment", "Lalpha/soft/quber/ui/screens/AddCardScreen;", "Lalpha/soft/quber/ui/screens/ChatScreen;", "Lalpha/soft/quber/ui/screens/HistoryScreen;", "Lalpha/soft/quber/ui/screens/HistoryTypeFragment;", "Lalpha/soft/quber/ui/screens/LoginScreen;", "Lalpha/soft/quber/ui/screens/MapScreen;", "Lalpha/soft/quber/ui/screens/MyWalletScreen;", "Lalpha/soft/quber/ui/screens/NotificationScreen;", "Lalpha/soft/quber/ui/screens/OnBoardingTypeFragment;", "Lalpha/soft/quber/ui/screens/OrderDetailsScreen;", "Lalpha/soft/quber/ui/screens/SettingsScreen;", "Lalpha/soft/quber/ui/screens/SplashScreen;", "Lalpha/soft/quber/ui/screens/TVScreen;", "Lalpha/soft/quber/ui/screens/VerifyScreen;", "Lalpha/soft/quber/ui/screens/onBoardingScreens/OnBoardingScreen;", "app_release"})
@dagger.Component(modules = {alpha.soft.quber.di.module.UserApiModule.class})
public abstract interface UserApiComponent {
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.App app);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.SplashScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.LoginScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.onBoardingScreens.OnBoardingScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.VerifyScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.MyWalletScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.AddCardScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.ChatScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.NotificationScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.SettingsScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.HistoryScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.HistoryTypeFragment fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.OnBoardingTypeFragment fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.OrderDetailsScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.TVScreen fragment);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.screens.MapScreen fragment);
}