package alpha.soft.quber.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006J\u0012\u0010\u0007\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0014J\u0006\u0010\n\u001a\u00020\u0006J\u0006\u0010\u000b\u001a\u00020\u0006R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lalpha/soft/quber/base/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "progress", "Lalpha/soft/quber/utils/ProgressBar;", "hideProgress", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setFullScreen", "showProgress", "app_release"})
public abstract class BaseActivity extends androidx.appcompat.app.AppCompatActivity {
    private alpha.soft.quber.utils.ProgressBar progress;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void setFullScreen() {
    }
    
    public final void showProgress() {
    }
    
    public final void hideProgress() {
    }
    
    public BaseActivity() {
        super();
    }
}