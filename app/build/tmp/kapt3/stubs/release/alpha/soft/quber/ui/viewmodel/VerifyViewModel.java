package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010!\u001a\u00020\u00062\u0006\u0010\"\u001a\u00020\u00152\u0006\u0010#\u001a\u00020\u00172\u0006\u0010\u0013\u001a\u00020\b2\u0006\u0010$\u001a\u00020%J\u0010\u0010&\u001a\u00020\u00062\u0006\u0010\'\u001a\u00020\bH\u0002J\u0010\u0010(\u001a\u00020\u00062\u0006\u0010)\u001a\u00020*H\u0002J\u000e\u0010+\u001a\u00020\u00062\u0006\u0010\'\u001a\u00020\bR\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\b0\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0012R\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\n0\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0012R\u000e\u0010\u001e\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/VerifyViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_openMain", "Landroidx/lifecycle/MutableLiveData;", "Lalpha/soft/quber/utils/Event;", "", "_showFailedMessage", "", "_showSMSCodeError", "", "auth", "Lcom/google/firebase/auth/FirebaseAuth;", "callbacks", "Lcom/google/firebase/auth/PhoneAuthProvider$OnVerificationStateChangedCallbacks;", "openMain", "Landroidx/lifecycle/LiveData;", "getOpenMain", "()Landroidx/lifecycle/LiveData;", "phoneNumber", "postApi", "Lalpha/soft/quber/network/PostApi;", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "resendToken", "Lcom/google/firebase/auth/PhoneAuthProvider$ForceResendingToken;", "showFailedMessage", "getShowFailedMessage", "showSMSCodeError", "getShowSMSCodeError", "storedVerificationId", "verificationInProgress", "", "init", "api", "preferencesUtil", "activity", "Landroidx/appcompat/app/AppCompatActivity;", "sendSMSCode", "sms_code", "signInWithPhoneAuthCredential", "credential", "Lcom/google/firebase/auth/PhoneAuthCredential;", "verify", "app_release"})
public final class VerifyViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.network.PostApi postApi;
    private alpha.soft.quber.utils.PreferencesUtil preference;
    private boolean verificationInProgress;
    private java.lang.String storedVerificationId;
    private com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken resendToken;
    private com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private com.google.firebase.auth.FirebaseAuth auth;
    private java.lang.String phoneNumber;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> _showFailedMessage = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> showFailedMessage = null;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _openMain = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> openMain = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Integer> _showSMSCodeError = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Integer> showSMSCodeError = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getShowFailedMessage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> getOpenMain() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getShowSMSCodeError() {
        return null;
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi api, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil, @org.jetbrains.annotations.NotNull()
    java.lang.String phoneNumber, @org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity activity) {
    }
    
    private final void sendSMSCode(java.lang.String sms_code) {
    }
    
    public final void verify(@org.jetbrains.annotations.NotNull()
    java.lang.String sms_code) {
    }
    
    private final void signInWithPhoneAuthCredential(com.google.firebase.auth.PhoneAuthCredential credential) {
    }
    
    public VerifyViewModel() {
        super();
    }
}