package alpha.soft.quber.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002\'(B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u001d\u001a\u00020\u001eH\u0003J\b\u0010\u001f\u001a\u00020\u001eH\u0002J\b\u0010 \u001a\u00020\u001eH\u0003J\b\u0010!\u001a\u00020\u001eH\u0003J\b\u0010\"\u001a\u00020\u001eH\u0007J\b\u0010#\u001a\u00020\u001eH\u0007J\u0010\u0010$\u001a\u00020\u001e2\u0006\u0010%\u001a\u00020&H\u0002R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lalpha/soft/quber/utils/EasyLocationProvider;", "Landroidx/lifecycle/LifecycleObserver;", "builder", "Lalpha/soft/quber/utils/EasyLocationProvider$Builder;", "(Lalpha/soft/quber/utils/EasyLocationProvider$Builder;)V", "callback", "Lalpha/soft/quber/utils/EasyLocationProvider$EasyLocationCallback;", "context", "Landroid/content/Context;", "fastestInterval", "", "interval", "latitude", "", "longitude", "mFusedLocationClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "mGoogleApiClient", "Lcom/google/android/gms/common/api/GoogleApiClient;", "mLocationCallback", "Lcom/google/android/gms/location/LocationCallback;", "mLocationRequest", "Lcom/google/android/gms/location/LocationRequest;", "mLocationSettingsRequest", "Lcom/google/android/gms/location/LocationSettingsRequest;", "mSettingsClient", "Lcom/google/android/gms/location/SettingsClient;", "priority", "", "buildGoogleApiClient", "", "connectGoogleClient", "onCreateLocationProvider", "onLocationResume", "removeUpdates", "requestLocationUpdate", "showLog", "message", "", "Builder", "EasyLocationCallback", "app_release"})
public final class EasyLocationProvider implements androidx.lifecycle.LifecycleObserver {
    private final alpha.soft.quber.utils.EasyLocationProvider.EasyLocationCallback callback = null;
    private final android.content.Context context = null;
    private com.google.android.gms.location.FusedLocationProviderClient mFusedLocationClient;
    private com.google.android.gms.location.SettingsClient mSettingsClient;
    private com.google.android.gms.location.LocationCallback mLocationCallback;
    private com.google.android.gms.location.LocationRequest mLocationRequest;
    private com.google.android.gms.common.api.GoogleApiClient mGoogleApiClient;
    private com.google.android.gms.location.LocationSettingsRequest mLocationSettingsRequest;
    private final long interval = 0L;
    private final long fastestInterval = 0L;
    private final int priority = 0;
    private double latitude;
    private double longitude;
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    public final void requestLocationUpdate() {
    }
    
    private final void connectGoogleClient() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_CREATE)
    private final void onCreateLocationProvider() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_RESUME)
    private final void onLocationResume() {
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    private final synchronized void buildGoogleApiClient() {
    }
    
    @android.annotation.SuppressLint(value = {"MissingPermission"})
    public final void removeUpdates() {
    }
    
    private final void showLog(java.lang.String message) {
    }
    
    private EasyLocationProvider(alpha.soft.quber.utils.EasyLocationProvider.Builder builder) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\b\u0010\b\u001a\u00020\u0003H&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH&\u00a8\u0006\r"}, d2 = {"Lalpha/soft/quber/utils/EasyLocationProvider$EasyLocationCallback;", "", "onGoogleAPIClient", "", "googleApiClient", "Lcom/google/android/gms/common/api/GoogleApiClient;", "message", "", "onLocationUpdateRemoved", "onLocationUpdated", "latitude", "", "longitude", "app_release"})
    public static abstract interface EasyLocationCallback {
        
        public abstract void onGoogleAPIClient(@org.jetbrains.annotations.Nullable()
        com.google.android.gms.common.api.GoogleApiClient googleApiClient, @org.jetbrains.annotations.NotNull()
        java.lang.String message);
        
        public abstract void onLocationUpdated(double latitude, double longitude);
        
        public abstract void onLocationUpdateRemoved();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010\u0011\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u0017J\u000e\u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u000eJ\u000e\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0017R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0010\"\u0004\b\u0015\u0010\u0012R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001b\u00a8\u0006\u001f"}, d2 = {"Lalpha/soft/quber/utils/EasyLocationProvider$Builder;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "callback", "Lalpha/soft/quber/utils/EasyLocationProvider$EasyLocationCallback;", "getCallback", "()Lalpha/soft/quber/utils/EasyLocationProvider$EasyLocationCallback;", "setCallback", "(Lalpha/soft/quber/utils/EasyLocationProvider$EasyLocationCallback;)V", "getContext", "()Landroid/content/Context;", "fastestInterval", "", "getFastestInterval", "()J", "setFastestInterval", "(J)V", "interval", "getInterval", "setInterval", "priority", "", "getPriority", "()I", "setPriority", "(I)V", "build", "Lalpha/soft/quber/utils/EasyLocationProvider;", "setListener", "app_release"})
    public static final class Builder {
        @org.jetbrains.annotations.Nullable()
        private alpha.soft.quber.utils.EasyLocationProvider.EasyLocationCallback callback;
        private long interval;
        private long fastestInterval;
        private int priority;
        @org.jetbrains.annotations.NotNull()
        private final android.content.Context context = null;
        
        @org.jetbrains.annotations.Nullable()
        public final alpha.soft.quber.utils.EasyLocationProvider.EasyLocationCallback getCallback() {
            return null;
        }
        
        public final void setCallback(@org.jetbrains.annotations.Nullable()
        alpha.soft.quber.utils.EasyLocationProvider.EasyLocationCallback p0) {
        }
        
        public final long getInterval() {
            return 0L;
        }
        
        public final void setInterval(long p0) {
        }
        
        public final long getFastestInterval() {
            return 0L;
        }
        
        public final void setFastestInterval(long p0) {
        }
        
        public final int getPriority() {
            return 0;
        }
        
        public final void setPriority(int p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final alpha.soft.quber.utils.EasyLocationProvider build() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final alpha.soft.quber.utils.EasyLocationProvider.Builder setListener(@org.jetbrains.annotations.NotNull()
        alpha.soft.quber.utils.EasyLocationProvider.EasyLocationCallback callback) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final alpha.soft.quber.utils.EasyLocationProvider.Builder setInterval(long interval) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final alpha.soft.quber.utils.EasyLocationProvider.Builder setFastestInterval(int fastestInterval) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final alpha.soft.quber.utils.EasyLocationProvider.Builder setPriority(int priority) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Context getContext() {
            return null;
        }
        
        public Builder(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            super();
        }
    }
}