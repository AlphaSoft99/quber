package alpha.soft.quber.ui.screens;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0000H\u0002J\b\u0010\u0012\u001a\u00020\u0010H\u0002J\b\u0010\u0013\u001a\u00020\u0010H\u0016J\u001a\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0019"}, d2 = {"Lalpha/soft/quber/ui/screens/HistoryScreen;", "Landroidx/fragment/app/Fragment;", "()V", "preferencesUtil", "Lalpha/soft/quber/utils/PreferencesUtil;", "getPreferencesUtil", "()Lalpha/soft/quber/utils/PreferencesUtil;", "setPreferencesUtil", "(Lalpha/soft/quber/utils/PreferencesUtil;)V", "viewModel", "Lalpha/soft/quber/ui/viewmodel/HistoryViewModel;", "getViewModel", "()Lalpha/soft/quber/ui/viewmodel/HistoryViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "injectDependency", "", "fragment", "loadData", "onDestroy", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "app_release"})
public final class HistoryScreen extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.utils.PreferencesUtil preferencesUtil;
    private final kotlin.Lazy viewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.utils.PreferencesUtil getPreferencesUtil() {
        return null;
    }
    
    public final void setPreferencesUtil(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil p0) {
    }
    
    private final alpha.soft.quber.ui.viewmodel.HistoryViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void loadData() {
    }
    
    private final void injectDependency(alpha.soft.quber.ui.screens.HistoryScreen fragment) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public HistoryScreen() {
        super();
    }
}