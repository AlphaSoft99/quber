package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/OnBoardingTypeViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "init", "", "preferencesUtil", "api", "Lalpha/soft/quber/network/PostApi;", "app_release"})
public final class OnBoardingTypeViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.utils.PreferencesUtil preference;
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi api) {
    }
    
    public OnBoardingTypeViewModel() {
        super();
    }
}