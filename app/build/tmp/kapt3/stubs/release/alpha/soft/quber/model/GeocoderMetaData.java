package alpha.soft.quber.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J;\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\b\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010\u00a8\u0006\u001f"}, d2 = {"Lalpha/soft/quber/model/GeocoderMetaData;", "", "Address", "Lalpha/soft/quber/model/Address;", "AddressDetails", "Lalpha/soft/quber/model/AddressDetails;", "kind", "", "precision", "text", "(Lalpha/soft/quber/model/Address;Lalpha/soft/quber/model/AddressDetails;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAddress", "()Lalpha/soft/quber/model/Address;", "getAddressDetails", "()Lalpha/soft/quber/model/AddressDetails;", "getKind", "()Ljava/lang/String;", "getPrecision", "getText", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release"})
public final class GeocoderMetaData {
    @org.jetbrains.annotations.NotNull()
    private final alpha.soft.quber.model.Address Address = null;
    @org.jetbrains.annotations.NotNull()
    private final alpha.soft.quber.model.AddressDetails AddressDetails = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String kind = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String precision = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String text = null;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.Address getAddress() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.AddressDetails getAddressDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getKind() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPrecision() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getText() {
        return null;
    }
    
    public GeocoderMetaData(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.Address Address, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.AddressDetails AddressDetails, @org.jetbrains.annotations.NotNull()
    java.lang.String kind, @org.jetbrains.annotations.NotNull()
    java.lang.String precision, @org.jetbrains.annotations.NotNull()
    java.lang.String text) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.Address component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.AddressDetails component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.GeocoderMetaData copy(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.Address Address, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.AddressDetails AddressDetails, @org.jetbrains.annotations.NotNull()
    java.lang.String kind, @org.jetbrains.annotations.NotNull()
    java.lang.String precision, @org.jetbrains.annotations.NotNull()
    java.lang.String text) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}