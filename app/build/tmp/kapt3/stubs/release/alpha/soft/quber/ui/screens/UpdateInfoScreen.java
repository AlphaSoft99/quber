package alpha.soft.quber.ui.screens;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001c\u001a\u00020\u000eH\u0002J\b\u0010\u001d\u001a\u00020\u000eH\u0016J\u001a\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u0010\u0010#\u001a\u00020\u000e2\u0006\u0010$\u001a\u00020%H\u0002J\u0010\u0010&\u001a\u00020\u000e2\u0006\u0010$\u001a\u00020%H\u0002J\u0012\u0010\'\u001a\u00020\u000e2\b\u0010(\u001a\u0004\u0018\u00010\u0005H\u0002J\b\u0010)\u001a\u00020\u000eH\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u0012\u001a\u0004\u0018\u00010\u00052\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005@BX\u0082\u000e\u00a2\u0006\b\n\u0000\"\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0016\u001a\u00020\u00178BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u0018\u0010\u0019\u00a8\u0006*"}, d2 = {"Lalpha/soft/quber/ui/screens/UpdateInfoScreen;", "Landroidx/fragment/app/Fragment;", "()V", "addressObserver", "Landroidx/lifecycle/Observer;", "", "bottomSheetBehavior", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Landroid/widget/LinearLayout;", "emailObserver", "fullNameObserver", "imageFile", "Ljava/io/File;", "logOutObserver", "", "mobilePhoneObserver", "saveInfoObserver", "value", "tempImgUrl", "setTempImgUrl", "(Ljava/lang/String;)V", "updatePhotoObserver", "viewModel", "Lalpha/soft/quber/ui/viewmodel/UpdateInfoModel;", "getViewModel", "()Lalpha/soft/quber/ui/viewmodel/UpdateInfoModel;", "viewModel$delegate", "Lkotlin/Lazy;", "loadView", "onDestroy", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "pickImage", "type", "Lcom/vansuita/pickimage/enums/EPickType;", "pictureClicked", "setPic", "imgUrl", "showLogOutDialog", "app_release"})
public final class UpdateInfoScreen extends androidx.fragment.app.Fragment {
    private final kotlin.Lazy viewModel$delegate = null;
    private java.io.File imageFile;
    private com.google.android.material.bottomsheet.BottomSheetBehavior<android.widget.LinearLayout> bottomSheetBehavior;
    private java.lang.String tempImgUrl;
    private final androidx.lifecycle.Observer<kotlin.Unit> updatePhotoObserver = null;
    private final androidx.lifecycle.Observer<java.lang.String> fullNameObserver = null;
    private final androidx.lifecycle.Observer<java.lang.String> mobilePhoneObserver = null;
    private final androidx.lifecycle.Observer<java.lang.String> emailObserver = null;
    private final androidx.lifecycle.Observer<java.lang.String> addressObserver = null;
    private final androidx.lifecycle.Observer<kotlin.Unit> logOutObserver = null;
    private final androidx.lifecycle.Observer<kotlin.Unit> saveInfoObserver = null;
    private java.util.HashMap _$_findViewCache;
    
    private final alpha.soft.quber.ui.viewmodel.UpdateInfoModel getViewModel() {
        return null;
    }
    
    private final void setTempImgUrl(java.lang.String value) {
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void loadView() {
    }
    
    private final void pictureClicked(com.vansuita.pickimage.enums.EPickType type) {
    }
    
    private final void showLogOutDialog() {
    }
    
    private final void pickImage(com.vansuita.pickimage.enums.EPickType type) {
    }
    
    private final void setPic(java.lang.String imgUrl) {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    public UpdateInfoScreen() {
        super();
    }
}