package alpha.soft.quber.ui.activities;

import java.lang.System;

@android.annotation.SuppressLint(value = {"RtlHardcoded"})
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0017\u001a\u00020\u0014H\u0002J\b\u0010\u0018\u001a\u00020\u0014H\u0002J\b\u0010\u0019\u001a\u00020\u0014H\u0003J\b\u0010\u001a\u001a\u00020\u0014H\u0002J\"\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001d2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\b\u0010!\u001a\u00020\u0014H\u0016J\u0012\u0010\"\u001a\u00020\u00142\b\u0010#\u001a\u0004\u0018\u00010$H\u0014J-\u0010%\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u001d2\u000e\u0010&\u001a\n\u0012\u0006\b\u0001\u0012\u00020(0\'2\u0006\u0010)\u001a\u00020*H\u0016\u00a2\u0006\u0002\u0010+J\b\u0010,\u001a\u00020\u0014H\u0014R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\n0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0012\u0010\b\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"}, d2 = {"Lalpha/soft/quber/ui/activities/MainActivity;", "Lalpha/soft/quber/base/BaseActivity;", "()V", "appBarConfiguration", "Landroidx/navigation/ui/AppBarConfiguration;", "getAppBarConfiguration", "()Landroidx/navigation/ui/AppBarConfiguration;", "appBarConfiguration$delegate", "Lkotlin/Lazy;", "doubleBackToExitPressedOnce", "", "isGPS", "isLockDrawerObserver", "Landroidx/lifecycle/Observer;", "navController", "Landroidx/navigation/NavController;", "getNavController", "()Landroidx/navigation/NavController;", "navController$delegate", "showLogoutDialogObserver", "", "viewModel", "Lalpha/soft/quber/ui/viewmodel/MainActivityViewModel;", "checkPermission", "hideSoftKeyboard", "loadViews", "navigationItemClick", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onRestart", "app_release"})
public final class MainActivity extends alpha.soft.quber.base.BaseActivity {
    private final kotlin.Lazy navController$delegate = null;
    private final kotlin.Lazy appBarConfiguration$delegate = null;
    private alpha.soft.quber.ui.viewmodel.MainActivityViewModel viewModel;
    private boolean doubleBackToExitPressedOnce;
    private boolean isGPS;
    private final androidx.lifecycle.Observer<java.lang.Boolean> isLockDrawerObserver = null;
    private final androidx.lifecycle.Observer<kotlin.Unit> showLogoutDialogObserver = null;
    private java.util.HashMap _$_findViewCache;
    
    private final androidx.navigation.NavController getNavController() {
        return null;
    }
    
    private final androidx.navigation.ui.AppBarConfiguration getAppBarConfiguration() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @android.annotation.SuppressLint(value = {"LogNotTimber"})
    private final void loadViews() {
    }
    
    private final void navigationItemClick() {
    }
    
    @java.lang.Override()
    protected void onRestart() {
    }
    
    private final void hideSoftKeyboard() {
    }
    
    private final void checkPermission() {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public MainActivity() {
        super();
    }
}