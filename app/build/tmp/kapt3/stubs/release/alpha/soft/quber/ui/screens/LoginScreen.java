package alpha.soft.quber.ui.screens;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010!\u001a\u00020\r2\u0006\u0010\"\u001a\u00020\u0000H\u0002J\b\u0010#\u001a\u00020\rH\u0002J\"\u0010$\u001a\u00020\r2\u0006\u0010%\u001a\u00020\u00192\u0006\u0010&\u001a\u00020\u00192\b\u0010\'\u001a\u0004\u0018\u00010\u0017H\u0016J\u001a\u0010(\u001a\u00020\r2\u0006\u0010)\u001a\u00020*2\b\u0010+\u001a\u0004\u0018\u00010,H\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u00020\u000f8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001b\u001a\u00020\u001c8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b\u001d\u0010\u001e\u00a8\u0006-"}, d2 = {"Lalpha/soft/quber/ui/screens/LoginScreen;", "Landroidx/fragment/app/Fragment;", "()V", "api", "Lalpha/soft/quber/network/PostApi;", "getApi", "()Lalpha/soft/quber/network/PostApi;", "setApi", "(Lalpha/soft/quber/network/PostApi;)V", "callbackManager", "Lcom/facebook/CallbackManager;", "hideProgressObserver", "Landroidx/lifecycle/Observer;", "", "preferencesUtil", "Lalpha/soft/quber/utils/PreferencesUtil;", "getPreferencesUtil", "()Lalpha/soft/quber/utils/PreferencesUtil;", "setPreferencesUtil", "(Lalpha/soft/quber/utils/PreferencesUtil;)V", "showFailedMessageObserver", "", "showGoogleSignDialogObserver", "Landroid/content/Intent;", "showPhoneNumberErrorObserver", "", "showProgressObserver", "viewModel", "Lalpha/soft/quber/ui/viewmodel/LoginViewModel;", "getViewModel", "()Lalpha/soft/quber/ui/viewmodel/LoginViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "injectDependency", "fragment", "loadData", "onActivityResult", "requestCode", "resultCode", "data", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "app_release"})
public final class LoginScreen extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.network.PostApi api;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.utils.PreferencesUtil preferencesUtil;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.facebook.CallbackManager callbackManager;
    private final androidx.lifecycle.Observer<java.lang.Integer> showPhoneNumberErrorObserver = null;
    private final androidx.lifecycle.Observer<android.content.Intent> showGoogleSignDialogObserver = null;
    private final androidx.lifecycle.Observer<kotlin.Unit> showProgressObserver = null;
    private final androidx.lifecycle.Observer<kotlin.Unit> hideProgressObserver = null;
    private final androidx.lifecycle.Observer<java.lang.String> showFailedMessageObserver = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.network.PostApi getApi() {
        return null;
    }
    
    public final void setApi(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.utils.PreferencesUtil getPreferencesUtil() {
        return null;
    }
    
    public final void setPreferencesUtil(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil p0) {
    }
    
    private final alpha.soft.quber.ui.viewmodel.LoginViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void injectDependency(alpha.soft.quber.ui.screens.LoginScreen fragment) {
    }
    
    private final void loadData() {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public LoginScreen() {
        super();
    }
}