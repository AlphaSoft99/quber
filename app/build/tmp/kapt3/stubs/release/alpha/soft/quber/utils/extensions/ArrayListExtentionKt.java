package alpha.soft.quber.utils.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0000\u001a#\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\u0006\u0010\u0004\u001a\u0002H\u0002\u00a2\u0006\u0002\u0010\u0005\u001a$\u0010\u0006\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00020\b\u00a8\u0006\t"}, d2 = {"clearAndAdd", "", "E", "Ljava/util/ArrayList;", "value", "(Ljava/util/ArrayList;Ljava/lang/Object;)Z", "clearAndAddAll", "values", "", "app_release"})
public final class ArrayListExtentionKt {
    
    public static final <E extends java.lang.Object>boolean clearAndAddAll(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<E> $this$clearAndAddAll, @org.jetbrains.annotations.NotNull()
    java.util.Collection<? extends E> values) {
        return false;
    }
    
    public static final <E extends java.lang.Object>boolean clearAndAdd(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<E> $this$clearAndAdd, E value) {
        return false;
    }
}