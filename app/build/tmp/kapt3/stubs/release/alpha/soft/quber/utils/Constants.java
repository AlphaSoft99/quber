package alpha.soft.quber.utils;

import java.lang.System;

/**
 * Utility class for constants
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u0006\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u001b\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u00020\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u00020\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\r\"\u0004\b\u0012\u0010\u000fR\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u00020\u0015X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\bR\u0014\u0010\u001a\u001a\u00020\u0015X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0017R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\'\u001a\u00020\u0015X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u0017R\u0014\u0010)\u001a\u00020\u0015X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\u0017R\u000e\u0010+\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0014\u0010-\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010\bR\u000e\u0010/\u001a\u00020\u0015X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lalpha/soft/quber/utils/Constants;", "", "()V", "CARD_DATE_FORMAT", "", "CARD_SERIAL_FORMAT", "CHECK_ONLINE", "getCHECK_ONLINE", "()Ljava/lang/String;", "COUNTRY_CODE_UZB", "DEFAULT_LATITUDE", "", "getDEFAULT_LATITUDE", "()D", "setDEFAULT_LATITUDE", "(D)V", "DEFAULT_LONGITUDE", "getDEFAULT_LONGITUDE", "setDEFAULT_LONGITUDE", "EXTRA_PHONE", "GPS_REQUEST", "", "getGPS_REQUEST", "()I", "HAS_ACCOUNT", "getHAS_ACCOUNT", "INTRO_PAGE_COUNT", "getINTRO_PAGE_COUNT", "KEY", "LANGUAGE_CODE_EN", "LANGUAGE_CODE_RU", "LANGUAGE_CODE_UZ", "LATITUDE", "LONGITUDE", "MONEY_FORMAT", "NUMBER_FORMAT", "PHONE_FORMAT_UZ", "PHONE_PREFIX_UZ", "PROMO_CODE_FORMAT", "RC_SIGN_IN", "getRC_SIGN_IN", "REQUEST_PERMISSION", "getREQUEST_PERMISSION", "SMS_FORMAT", "TAG", "USER", "getUSER", "VERIFICATION_CODE_LENGTH", "app_release"})
public final class Constants {
    private static final int GPS_REQUEST = 998;
    private static final int REQUEST_PERMISSION = 999;
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String USER = "USER";
    private static final int RC_SIGN_IN = 9901;
    private static final int INTRO_PAGE_COUNT = 3;
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String CHECK_ONLINE = "CHECK_ONLINE";
    @org.jetbrains.annotations.NotNull()
    private static final java.lang.String HAS_ACCOUNT = "HAS ACCOUNT";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY = "qPNIYxwpmT6jWEWYurxjGLIJKX7tNSCq";
    public static final int VERIFICATION_CODE_LENGTH = 6;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PHONE_FORMAT_UZ = "+998[00]-[000]-[00]-[00]";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MONEY_FORMAT = "[999] [999] [999]";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NUMBER_FORMAT = "[000] [000] [000]";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SMS_FORMAT = "[0] [0] [0] [0] [0] [0]";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROMO_CODE_FORMAT = "[0] [0] [0] [0] [0]";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PHONE_PREFIX_UZ = "+998";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_PHONE = "phone";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TAG = "TTT";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LATITUDE = "latitude";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LONGITUDE = "longitude";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CARD_SERIAL_FORMAT = "[0000] [0000] [0000] [0000]";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CARD_DATE_FORMAT = "[00] {/} [00]";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LANGUAGE_CODE_EN = "en";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LANGUAGE_CODE_RU = "ru";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LANGUAGE_CODE_UZ = "uz";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String COUNTRY_CODE_UZB = "UZ";
    private static double DEFAULT_LATITUDE;
    private static double DEFAULT_LONGITUDE;
    public static final alpha.soft.quber.utils.Constants INSTANCE = null;
    
    public final int getGPS_REQUEST() {
        return 0;
    }
    
    public final int getREQUEST_PERMISSION() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUSER() {
        return null;
    }
    
    public final int getRC_SIGN_IN() {
        return 0;
    }
    
    public final int getINTRO_PAGE_COUNT() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCHECK_ONLINE() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getHAS_ACCOUNT() {
        return null;
    }
    
    public final double getDEFAULT_LATITUDE() {
        return 0.0;
    }
    
    public final void setDEFAULT_LATITUDE(double p0) {
    }
    
    public final double getDEFAULT_LONGITUDE() {
        return 0.0;
    }
    
    public final void setDEFAULT_LONGITUDE(double p0) {
    }
    
    private Constants() {
        super();
    }
}