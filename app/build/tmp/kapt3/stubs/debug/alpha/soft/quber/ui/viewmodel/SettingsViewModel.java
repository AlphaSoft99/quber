package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\t\u001a\u00020\u0013J\u0006\u0010\r\u001a\u00020\u0013J\u0006\u0010\u000f\u001a\u00020\u0013J\u0006\u0010\u0011\u001a\u00020\u0013R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\f\u00a8\u0006\u0014"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/SettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_aboutAppDropDown", "Landroidx/lifecycle/MutableLiveData;", "", "_aboutFactoryDropDown", "_changeLanguageDropDown", "_helpDropDown", "aboutAppDropDown", "Landroidx/lifecycle/LiveData;", "getAboutAppDropDown", "()Landroidx/lifecycle/LiveData;", "aboutFactoryDropDown", "getAboutFactoryDropDown", "changeLanguageDropDown", "getChangeLanguageDropDown", "helpDropDown", "getHelpDropDown", "", "app_debug"})
public final class SettingsViewModel extends androidx.lifecycle.ViewModel {
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _changeLanguageDropDown = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Boolean> changeLanguageDropDown = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _aboutAppDropDown = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Boolean> aboutAppDropDown = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _helpDropDown = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Boolean> helpDropDown = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _aboutFactoryDropDown = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Boolean> aboutFactoryDropDown = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getChangeLanguageDropDown() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getAboutAppDropDown() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getHelpDropDown() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getAboutFactoryDropDown() {
        return null;
    }
    
    public final void changeLanguageDropDown() {
    }
    
    public final void aboutAppDropDown() {
    }
    
    public final void helpDropDown() {
    }
    
    public final void aboutFactoryDropDown() {
    }
    
    public SettingsViewModel() {
        super();
    }
}