package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u0007J\u0006\u0010\b\u001a\u00020\u0005R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/AddCardViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_saveCard", "Landroidx/lifecycle/MutableLiveData;", "", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "saveCard", "Landroidx/lifecycle/LiveData;", "getSaveCard", "()Landroidx/lifecycle/LiveData;", "init", "preferencesUtil", "app_debug"})
public final class AddCardViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.utils.PreferencesUtil preference;
    private final androidx.lifecycle.MutableLiveData<kotlin.Unit> _saveCard = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<kotlin.Unit> saveCard = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<kotlin.Unit> getSaveCard() {
        return null;
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil) {
    }
    
    public final void saveCard() {
    }
    
    public AddCardViewModel() {
        super();
    }
}