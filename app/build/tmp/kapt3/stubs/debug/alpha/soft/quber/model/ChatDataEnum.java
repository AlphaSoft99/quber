package alpha.soft.quber.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0086\u0001\u0018\u0000 \u00072\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0007B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000j\u0002\b\u0005j\u0002\b\u0006\u00a8\u0006\b"}, d2 = {"Lalpha/soft/quber/model/ChatDataEnum;", "", "type", "", "(Ljava/lang/String;II)V", "ClientChat", "DriverChatData", "Companion", "app_debug"})
public enum ChatDataEnum {
    /*public static final*/ ClientChat /* = new ClientChat(0) */,
    /*public static final*/ DriverChatData /* = new DriverChatData(0) */;
    private final int type = 0;
    public static final alpha.soft.quber.model.ChatDataEnum.Companion Companion = null;
    
    ChatDataEnum(int type) {
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0003\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004\u00a8\u0006\u0007"}, d2 = {"Lalpha/soft/quber/model/ChatDataEnum$Companion;", "", "()V", "getType", "", "type", "Lalpha/soft/quber/model/ChatDataEnum;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final alpha.soft.quber.model.ChatDataEnum getType(int type) {
            return null;
        }
        
        public final int getType(@org.jetbrains.annotations.NotNull()
        alpha.soft.quber.model.ChatDataEnum type) {
            return 0;
        }
        
        private Companion() {
            super();
        }
    }
}