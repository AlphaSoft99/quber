package alpha.soft.quber.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0007J\b\u0010\u0013\u001a\u00020\u0014H\u0007J\b\u0010\u0015\u001a\u00020\u000fH\u0007R\u0016\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lalpha/soft/quber/di/module/UserApiModule;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "AUTH_HEADER_EXCEPTED_LIST", "", "", "[Ljava/lang/String;", "provideRetrofit", "Lretrofit2/Retrofit;", "okHttpClient", "Lokhttp3/OkHttpClient;", "provideRetrofitBuilder", "preferencesUtil", "Lalpha/soft/quber/utils/PreferencesUtil;", "provideUserApiService", "Lalpha/soft/quber/network/PostApi;", "retrofit", "providesGson", "Lcom/google/gson/Gson;", "providesPreferences", "app_debug"})
@dagger.Module()
public final class UserApiModule {
    private final java.lang.String[] AUTH_HEADER_EXCEPTED_LIST = null;
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.NotNull()
    @alpha.soft.quber.di.scope.AppScope()
    @dagger.Provides()
    public final alpha.soft.quber.network.PostApi provideUserApiService(@org.jetbrains.annotations.NotNull()
    retrofit2.Retrofit retrofit) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @alpha.soft.quber.di.scope.AppScope()
    @dagger.Provides()
    public final retrofit2.Retrofit provideRetrofit(@org.jetbrains.annotations.NotNull()
    okhttp3.OkHttpClient okHttpClient) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @alpha.soft.quber.di.scope.AppScope()
    @dagger.Provides()
    public final okhttp3.OkHttpClient provideRetrofitBuilder(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @alpha.soft.quber.di.scope.AppScope()
    @dagger.Provides()
    public final alpha.soft.quber.utils.PreferencesUtil providesPreferences() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @alpha.soft.quber.di.scope.AppScope()
    @dagger.Provides()
    public final com.google.gson.Gson providesGson() {
        return null;
    }
    
    public UserApiModule(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}