package alpha.soft.quber.utils.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\\\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u001a(\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0007\u001a\n\u0010\u0011\u001a\u00020\u0012*\u00020\u0013\u001a\n\u0010\u0014\u001a\u00020\u0015*\u00020\u0015\u001a\n\u0010\u0016\u001a\u00020\u0015*\u00020\u0015\u001a\u0012\u0010\u0017\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0019\u001a\u001a\u0010\u001a\u001a\u00020\u0012*\u00020\u00012\u0006\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\u0001\u001a\n\u0010\u001d\u001a\u00020\u0001*\u00020\u0013\u001a\u000e\u0010\u001e\u001a\u0004\u0018\u00010\u0001*\u00020\u000eH\u0002\u001a\n\u0010\u001f\u001a\u00020 *\u00020\u0001\u001a\u0012\u0010!\u001a\u00020\t*\u00020\u000e2\u0006\u0010\"\u001a\u00020\u0001\u001a\n\u0010#\u001a\u00020\u0015*\u00020\u0015*(\u0010$\u001a\u0004\b\u0000\u0010%\"\u000e\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u00020\u00120&2\u000e\u0012\u0004\u0012\u0002H%\u0012\u0004\u0012\u00020\u00120&\u00a8\u0006\'"}, d2 = {"doubleToStringNoDecimal", "", "d", "", "drawableToBitmap", "Landroid/graphics/Bitmap;", "drawable", "Landroid/graphics/drawable/Drawable;", "prepareFilePart", "Lokhttp3/MultipartBody$Part;", "context", "Landroid/content/Context;", "partName", "file", "Ljava/io/File;", "uri", "Landroid/net/Uri;", "clear", "", "Landroid/widget/EditText;", "gone", "Landroid/view/View;", "invisible", "onTextChanged", "textView", "Landroid/widget/TextView;", "showSnacber", "view", "text", "to", "toMimeType", "toRequestBody", "Lokhttp3/RequestBody;", "toRequestData", "keyName", "visible", "ButtonClick", "T", "Lkotlin/Function1;", "app_debug"})
public final class ViewExtentionKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View gone(@org.jetbrains.annotations.NotNull()
    android.view.View $this$gone) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.NonNull()
    @kotlin.Suppress(names = {"SameParameterValue"})
    public static final okhttp3.MultipartBody.Part prepareFilePart(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String partName, @org.jetbrains.annotations.NotNull()
    java.io.File file, @org.jetbrains.annotations.NotNull()
    android.net.Uri uri) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View invisible(@org.jetbrains.annotations.NotNull()
    android.view.View $this$invisible) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View visible(@org.jetbrains.annotations.NotNull()
    android.view.View $this$visible) {
        return null;
    }
    
    public static final void onTextChanged(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $this$onTextChanged, @org.jetbrains.annotations.NotNull()
    android.widget.TextView textView) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String to(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $this$to) {
        return null;
    }
    
    public static final void clear(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $this$clear) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final okhttp3.RequestBody toRequestBody(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$toRequestBody) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final okhttp3.MultipartBody.Part toRequestData(@org.jetbrains.annotations.NotNull()
    java.io.File $this$toRequestData, @org.jetbrains.annotations.NotNull()
    java.lang.String keyName) {
        return null;
    }
    
    private static final java.lang.String toMimeType(@org.jetbrains.annotations.NotNull()
    java.io.File $this$toMimeType) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String doubleToStringNoDecimal(double d) {
        return null;
    }
    
    public static final void showSnacber(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$showSnacber, @org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final android.graphics.Bitmap drawableToBitmap(@org.jetbrains.annotations.NotNull()
    android.graphics.drawable.Drawable drawable) {
        return null;
    }
}