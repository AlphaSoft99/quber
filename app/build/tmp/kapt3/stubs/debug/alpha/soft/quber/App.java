package alpha.soft.quber;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0016\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\fH\u0002J\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014J\b\u0010\u0015\u001a\u00020\u0004H\u0016J\u0010\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u000eH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0005\u001a\u00020\u00068\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lalpha/soft/quber/App;", "Landroid/app/Application;", "()V", "apiComponent", "Lalpha/soft/quber/di/component/UserApiComponent;", "preferencesUtil", "Lalpha/soft/quber/utils/PreferencesUtil;", "getPreferencesUtil", "()Lalpha/soft/quber/utils/PreferencesUtil;", "setPreferencesUtil", "(Lalpha/soft/quber/utils/PreferencesUtil;)V", "userLanguage", "", "applyLocale", "", "context", "Landroid/content/Context;", "locale", "Ljava/util/Locale;", "language", "attachBaseContext", "getApiComponent", "onConfigurationChanged", "newConfig", "Landroid/content/res/Configuration;", "onCreate", "Companion", "app_debug"})
public class App extends android.app.Application {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.utils.PreferencesUtil preferencesUtil;
    private alpha.soft.quber.di.component.UserApiComponent apiComponent;
    private java.lang.String userLanguage;
    @org.jetbrains.annotations.Nullable()
    private static alpha.soft.quber.App instance;
    public static final alpha.soft.quber.App.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.utils.PreferencesUtil getPreferencesUtil() {
        return null;
    }
    
    public final void setPreferencesUtil(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil p0) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    @java.lang.Override()
    protected void attachBaseContext(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onConfigurationChanged(@org.jetbrains.annotations.NotNull()
    android.content.res.Configuration newConfig) {
    }
    
    public final void applyLocale(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    private final void applyLocale(android.content.Context context, java.lang.String language) {
    }
    
    private final void applyLocale(android.content.Context context, java.util.Locale locale) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public alpha.soft.quber.di.component.UserApiComponent getApiComponent() {
        return null;
    }
    
    public App() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R(\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004@BX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\n"}, d2 = {"Lalpha/soft/quber/App$Companion;", "", "()V", "<set-?>", "Lalpha/soft/quber/App;", "instance", "getInstance", "()Lalpha/soft/quber/App;", "setInstance", "(Lalpha/soft/quber/App;)V", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final alpha.soft.quber.App getInstance() {
            return null;
        }
        
        private final void setInstance(alpha.soft.quber.App p0) {
        }
        
        private Companion() {
            super();
        }
    }
}