package alpha.soft.quber.utils.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0002\u001a\n\u0010\u0005\u001a\u00020\u0006*\u00020\u0002\u001a\n\u0010\u0007\u001a\u00020\b*\u00020\u0002\u001a\n\u0010\t\u001a\u00020\n*\u00020\u0002\u00a8\u0006\u000b"}, d2 = {"activity", "Lalpha/soft/quber/ui/activities/MainActivity;", "Landroidx/fragment/app/Fragment;", "activitySplash", "Lalpha/soft/quber/ui/activities/SplashActivity;", "mainViewModel", "Lalpha/soft/quber/ui/viewmodel/MainActivityViewModel;", "onCreateMenu", "", "welcomeViewModel", "Lalpha/soft/quber/ui/viewmodel/SplashActivityViewModel;", "app_debug"})
public final class FragmentExtentionKt {
    
    public static final void onCreateMenu(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$onCreateMenu) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final alpha.soft.quber.ui.viewmodel.SplashActivityViewModel welcomeViewModel(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$welcomeViewModel) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final alpha.soft.quber.ui.viewmodel.MainActivityViewModel mainViewModel(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$mainViewModel) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final alpha.soft.quber.ui.activities.MainActivity activity(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$activity) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final alpha.soft.quber.ui.activities.SplashActivity activitySplash(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$activitySplash) {
        return null;
    }
}