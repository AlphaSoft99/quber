package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\t\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0005R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/HistoryTypeViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_openHistoryDetails", "Landroidx/lifecycle/MutableLiveData;", "", "_postLiveData", "", "Lalpha/soft/quber/model/PostData;", "openHistoryDetails", "Landroidx/lifecycle/LiveData;", "getOpenHistoryDetails", "()Landroidx/lifecycle/LiveData;", "postLiveData", "getPostLiveData", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "init", "", "preferencesUtil", "api", "Lalpha/soft/quber/network/PostApi;", "pos", "app_debug"})
public final class HistoryTypeViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.utils.PreferencesUtil preference;
    private final androidx.lifecycle.MutableLiveData<java.util.List<alpha.soft.quber.model.PostData>> _postLiveData = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<alpha.soft.quber.model.PostData>> postLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Integer> _openHistoryDetails = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Integer> openHistoryDetails = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<alpha.soft.quber.model.PostData>> getPostLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getOpenHistoryDetails() {
        return null;
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi api) {
    }
    
    public final void openHistoryDetails(int pos) {
    }
    
    public HistoryTypeViewModel() {
        super();
    }
}