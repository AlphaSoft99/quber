package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000bJ\u0012\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u000fJ\u0012\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u000fJ\u0012\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u000fJ\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u000fR\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/SplashViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_openLogInLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lalpha/soft/quber/utils/Event;", "", "_openMainLiveData", "_openOnBoardingLiveData", "_responseWidgetLiveData", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "init", "preferencesUtil", "openLogInLiveData", "Landroidx/lifecycle/LiveData;", "openMainLiveData", "openOnBoardingLiveData", "responseWidgetLiveData", "app_debug"})
public final class SplashViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.utils.PreferencesUtil preference;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _openOnBoardingLiveData = null;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _responseWidgetLiveData = null;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _openLogInLiveData = null;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _openMainLiveData = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> openOnBoardingLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> responseWidgetLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> openLogInLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> openMainLiveData() {
        return null;
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil) {
    }
    
    public SplashViewModel() {
        super();
    }
}