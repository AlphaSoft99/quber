package alpha.soft.quber.utils.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000<\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0004\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a%\u0010\u0000\u001a\u00020\u0001\"\b\b\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u001a\u0014\u0010\b\u001a\u00020\t*\u00020\n2\b\b\u0001\u0010\u000b\u001a\u00020\f\u001a\u0014\u0010\b\u001a\u00020\t*\u00020\r2\b\b\u0001\u0010\u000b\u001a\u00020\f\u001a\u0012\u0010\u000e\u001a\u00020\u000f*\u00020\u00012\u0006\u0010\u0010\u001a\u00020\t\u001a\u0014\u0010\u0011\u001a\u00020\u000f*\u00020\u00012\b\u0010\u0012\u001a\u0004\u0018\u00010\n\u001a\u0014\u0010\u0013\u001a\u00020\u000f*\u00020\u00142\b\b\u0001\u0010\u0015\u001a\u00020\f\u00a8\u0006\u0016"}, d2 = {"formatMoney", "", "T", "", "value", "showDecimal", "", "(Ljava/lang/Number;Z)Ljava/lang/String;", "inflate", "Landroid/view/View;", "Landroid/content/Context;", "resId", "", "Landroid/view/ViewGroup;", "showSnacbar", "", "view", "showToast", "context", "textError", "Landroid/widget/TextView;", "message", "app_debug"})
public final class LayoutInflaterKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View inflate(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup $this$inflate, @androidx.annotation.LayoutRes()
    int resId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View inflate(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$inflate, @androidx.annotation.LayoutRes()
    int resId) {
        return null;
    }
    
    public static final void showSnacbar(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$showSnacbar, @org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public static final void showToast(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$showToast, @org.jetbrains.annotations.Nullable()
    android.content.Context context) {
    }
    
    public static final void textError(@org.jetbrains.annotations.NotNull()
    android.widget.TextView $this$textError, @androidx.annotation.StringRes()
    int message) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final <T extends java.lang.Number>java.lang.String formatMoney(@org.jetbrains.annotations.NotNull()
    T value, boolean showDecimal) {
        return null;
    }
}