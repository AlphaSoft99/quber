package alpha.soft.quber.ui.screens;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0086\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006B\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010@\u001a\u00020AH\u0002J\u0010\u0010B\u001a\u00020A2\u0006\u0010C\u001a\u00020\u0000H\u0002J\b\u0010D\u001a\u00020AH\u0002J(\u0010E\u001a\u00020A2\u0006\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020I2\u0006\u0010J\u001a\u00020K2\u0006\u0010L\u001a\u00020$H\u0016J\u0016\u0010M\u001a\u00020A2\f\u0010F\u001a\b\u0012\u0004\u0012\u00020O0NH\u0016J\u0010\u0010P\u001a\u00020A2\u0006\u0010F\u001a\u00020QH\u0016J\u0018\u0010R\u001a\u00020$2\u0006\u0010F\u001a\u00020S2\u0006\u0010H\u001a\u00020\tH\u0016J\u0010\u0010T\u001a\u00020A2\u0006\u0010U\u001a\u00020VH\u0016J\u0010\u0010W\u001a\u00020A2\u0006\u0010X\u001a\u00020VH\u0016J\u0018\u0010Y\u001a\u00020A2\u0006\u0010X\u001a\u00020V2\u0006\u0010Z\u001a\u00020[H\u0016J\b\u0010\\\u001a\u00020AH\u0016J\u0010\u0010]\u001a\u00020A2\u0006\u0010F\u001a\u00020QH\u0016J\u0010\u0010^\u001a\u00020A2\u0006\u0010F\u001a\u00020_H\u0016J\b\u0010`\u001a\u00020AH\u0016J\b\u0010a\u001a\u00020AH\u0016J\u001a\u0010b\u001a\u00020A2\u0006\u0010X\u001a\u00020c2\b\u0010d\u001a\u0004\u0018\u00010eH\u0016J\b\u0010f\u001a\u00020AH\u0002J\u0010\u0010g\u001a\u00020A2\u0006\u0010h\u001a\u00020\tH\u0002J\u0010\u0010g\u001a\u00020A2\u0006\u0010h\u001a\u00020/H\u0002J\b\u0010i\u001a\u00020AH\u0002R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\b\u0012\u0004\u0012\u00020\'0&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010(\u001a\u00020)8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u0014\u0010.\u001a\b\u0012\u0004\u0012\u00020/0&X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u00102\u001a\u0004\u0018\u000103X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00104\u001a\b\u0012\u0004\u0012\u0002050\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u00106\u001a\u0004\u0018\u000107X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00108\u001a\u0004\u0018\u000109X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010:\u001a\u00020;8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b>\u0010?\u001a\u0004\b<\u0010=\u00a8\u0006j"}, d2 = {"Lalpha/soft/quber/ui/screens/MapScreen;", "Landroidx/fragment/app/Fragment;", "Lcom/yandex/mapkit/search/Session$SearchListener;", "Lcom/yandex/mapkit/user_location/UserLocationObjectListener;", "Lcom/yandex/mapkit/directions/driving/DrivingSession$DrivingRouteListener;", "Lcom/yandex/mapkit/map/CameraListener;", "Lcom/yandex/mapkit/map/MapObjectTapListener;", "()V", "FINAL_LOCATION", "Lcom/yandex/mapkit/geometry/Point;", "TARGET_LOCATION", "api", "Lalpha/soft/quber/network/PostApi;", "getApi", "()Lalpha/soft/quber/network/PostApi;", "setApi", "(Lalpha/soft/quber/network/PostApi;)V", "bottomSheetBehavior", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "carsAdapter", "Lalpha/soft/quber/ui/adapters/CarsAdapter;", "carsData", "Ljava/util/ArrayList;", "Lalpha/soft/quber/model/CardData;", "drivingRouter", "Lcom/yandex/mapkit/directions/driving/DrivingRouter;", "drivingSession", "Lcom/yandex/mapkit/directions/driving/DrivingSession;", "mapObjects", "Lcom/yandex/mapkit/map/MapObjectCollection;", "mapView", "Lcom/yandex/mapkit/mapview/MapView;", "marker", "Lcom/yandex/mapkit/map/PlacemarkMapObject;", "myLocation", "", "postLiveDataObserver", "Landroidx/lifecycle/Observer;", "Lalpha/soft/quber/model/AddressData;", "preferencesUtil", "Lalpha/soft/quber/utils/PreferencesUtil;", "getPreferencesUtil", "()Lalpha/soft/quber/utils/PreferencesUtil;", "setPreferencesUtil", "(Lalpha/soft/quber/utils/PreferencesUtil;)V", "queryLiveDataObserver", "", "resultAdapter", "Lalpha/soft/quber/ui/adapters/SearchAdapter;", "searchManager", "Lcom/yandex/mapkit/search/SearchManager;", "searchResult", "Lcom/yandex/mapkit/GeoObjectCollection$Item;", "searchSession", "Lcom/yandex/mapkit/search/Session;", "userLocationLayer", "Lcom/yandex/mapkit/user_location/UserLocationLayer;", "viewModel", "Lalpha/soft/quber/ui/viewmodel/MapViewModel;", "getViewModel", "()Lalpha/soft/quber/ui/viewmodel/MapViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "hideSoftKeyboard", "", "injectDependency", "fragment", "loadData", "onCameraPositionChanged", "p0", "Lcom/yandex/mapkit/map/Map;", "p1", "Lcom/yandex/mapkit/map/CameraPosition;", "p2", "Lcom/yandex/mapkit/map/CameraUpdateSource;", "p3", "onDrivingRoutes", "", "Lcom/yandex/mapkit/directions/driving/DrivingRoute;", "onDrivingRoutesError", "Lcom/yandex/runtime/Error;", "onMapObjectTap", "Lcom/yandex/mapkit/map/MapObject;", "onObjectAdded", "userLocationView", "Lcom/yandex/mapkit/user_location/UserLocationView;", "onObjectRemoved", "view", "onObjectUpdated", "event", "Lcom/yandex/mapkit/layers/ObjectEvent;", "onResume", "onSearchError", "onSearchResponse", "Lcom/yandex/mapkit/search/Response;", "onStart", "onStop", "onViewCreated", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "showSoftKeyboard", "submitQuery", "query", "submitRequest", "app_debug"})
public final class MapScreen extends androidx.fragment.app.Fragment implements com.yandex.mapkit.search.Session.SearchListener, com.yandex.mapkit.user_location.UserLocationObjectListener, com.yandex.mapkit.directions.driving.DrivingSession.DrivingRouteListener, com.yandex.mapkit.map.CameraListener, com.yandex.mapkit.map.MapObjectTapListener {
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.network.PostApi api;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public alpha.soft.quber.utils.PreferencesUtil preferencesUtil;
    private com.yandex.mapkit.geometry.Point TARGET_LOCATION;
    private com.yandex.mapkit.geometry.Point FINAL_LOCATION;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.yandex.mapkit.mapview.MapView mapView;
    private com.yandex.mapkit.map.PlacemarkMapObject marker;
    private com.yandex.mapkit.map.MapObjectCollection mapObjects;
    private com.yandex.mapkit.user_location.UserLocationLayer userLocationLayer;
    private com.yandex.mapkit.search.SearchManager searchManager;
    private com.yandex.mapkit.search.Session searchSession;
    private com.yandex.mapkit.directions.driving.DrivingRouter drivingRouter;
    private com.yandex.mapkit.directions.driving.DrivingSession drivingSession;
    private alpha.soft.quber.ui.adapters.SearchAdapter resultAdapter;
    private alpha.soft.quber.ui.adapters.CarsAdapter carsAdapter;
    private final java.util.ArrayList<com.yandex.mapkit.GeoObjectCollection.Item> searchResult = null;
    private final java.util.ArrayList<alpha.soft.quber.model.CardData> carsData = null;
    private boolean myLocation;
    private com.google.android.material.bottomsheet.BottomSheetBehavior<androidx.constraintlayout.widget.ConstraintLayout> bottomSheetBehavior;
    private final androidx.lifecycle.Observer<alpha.soft.quber.model.AddressData> postLiveDataObserver = null;
    private final androidx.lifecycle.Observer<java.lang.String> queryLiveDataObserver = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.network.PostApi getApi() {
        return null;
    }
    
    public final void setApi(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.utils.PreferencesUtil getPreferencesUtil() {
        return null;
    }
    
    public final void setPreferencesUtil(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil p0) {
    }
    
    private final alpha.soft.quber.ui.viewmodel.MapViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void injectDependency(alpha.soft.quber.ui.screens.MapScreen fragment) {
    }
    
    private final void loadData() {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onStop() {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @java.lang.Override()
    public void onObjectAdded(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.user_location.UserLocationView userLocationView) {
    }
    
    @java.lang.Override()
    public void onObjectRemoved(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.user_location.UserLocationView view) {
    }
    
    @java.lang.Override()
    public void onObjectUpdated(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.user_location.UserLocationView view, @org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.layers.ObjectEvent event) {
    }
    
    private final void submitQuery(java.lang.String query) {
    }
    
    private final void submitQuery(com.yandex.mapkit.geometry.Point query) {
    }
    
    @java.lang.Override()
    public void onSearchError(@org.jetbrains.annotations.NotNull()
    com.yandex.runtime.Error p0) {
    }
    
    @java.lang.Override()
    public void onSearchResponse(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.search.Response p0) {
    }
    
    private final void showSoftKeyboard() {
    }
    
    private final void hideSoftKeyboard() {
    }
    
    private final void submitRequest() {
    }
    
    @java.lang.Override()
    public void onDrivingRoutesError(@org.jetbrains.annotations.NotNull()
    com.yandex.runtime.Error p0) {
    }
    
    @java.lang.Override()
    public void onDrivingRoutes(@org.jetbrains.annotations.NotNull()
    java.util.List<com.yandex.mapkit.directions.driving.DrivingRoute> p0) {
    }
    
    @java.lang.Override()
    public void onCameraPositionChanged(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.map.Map p0, @org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.map.CameraPosition p1, @org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.map.CameraUpdateSource p2, boolean p3) {
    }
    
    @java.lang.Override()
    public boolean onMapObjectTap(@org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.map.MapObject p0, @org.jetbrains.annotations.NotNull()
    com.yandex.mapkit.geometry.Point p1) {
        return false;
    }
    
    public MapScreen() {
        super();
    }
}