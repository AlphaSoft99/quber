package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\t\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0005R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\f\u00a8\u0006\u0014"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/NotificationViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_openNotification", "Landroidx/lifecycle/MutableLiveData;", "", "_postLiveData", "", "Lalpha/soft/quber/model/PostData;", "openNotification", "Landroidx/lifecycle/LiveData;", "getOpenNotification", "()Landroidx/lifecycle/LiveData;", "postLiveData", "getPostLiveData", "init", "", "api", "Lalpha/soft/quber/network/PostApi;", "pos", "app_debug"})
public final class NotificationViewModel extends androidx.lifecycle.ViewModel {
    private final androidx.lifecycle.MutableLiveData<java.util.List<alpha.soft.quber.model.PostData>> _postLiveData = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<alpha.soft.quber.model.PostData>> postLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Integer> _openNotification = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Integer> openNotification = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<alpha.soft.quber.model.PostData>> getPostLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getOpenNotification() {
        return null;
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi api) {
    }
    
    public final void openNotification(int pos) {
    }
    
    public NotificationViewModel() {
        super();
    }
}