package alpha.soft.quber.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u0002\u0012\b\u0012\u00060\u0003R\u00020\u00000\u0001:\u0003\u0011\u0012\u0013B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0016J\u001c\u0010\n\u001a\u00020\u000b2\n\u0010\f\u001a\u00060\u0003R\u00020\u00002\u0006\u0010\t\u001a\u00020\bH\u0016J\u001c\u0010\r\u001a\u00060\u0003R\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lalpha/soft/quber/ui/adapters/ChatAdapter;", "Landroidx/recyclerview/widget/ListAdapter;", "Lalpha/soft/quber/model/ChatData;", "Lalpha/soft/quber/ui/adapters/ChatAdapter$ViewHolder;", "itemInterface", "Lalpha/soft/quber/ui/adapters/ChatAdapter$ItemInterface;", "(Lalpha/soft/quber/ui/adapters/ChatAdapter$ItemInterface;)V", "getItemViewType", "", "position", "onBindViewHolder", "", "holder", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "DiffUtilImp", "ItemInterface", "ViewHolder", "app_debug"})
public final class ChatAdapter extends androidx.recyclerview.widget.ListAdapter<alpha.soft.quber.model.ChatData, alpha.soft.quber.ui.adapters.ChatAdapter.ViewHolder> {
    private final alpha.soft.quber.ui.adapters.ChatAdapter.ItemInterface itemInterface = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public alpha.soft.quber.ui.adapters.ChatAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.adapters.ChatAdapter.ViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemViewType(int position) {
        return 0;
    }
    
    public ChatAdapter(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.ui.adapters.ChatAdapter.ItemInterface itemInterface) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016J\u0018\u0010\b\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0016\u00a8\u0006\t"}, d2 = {"Lalpha/soft/quber/ui/adapters/ChatAdapter$DiffUtilImp;", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lalpha/soft/quber/model/ChatData;", "()V", "areContentsTheSame", "", "oldItem", "newItem", "areItemsTheSame", "app_debug"})
    public static final class DiffUtilImp extends androidx.recyclerview.widget.DiffUtil.ItemCallback<alpha.soft.quber.model.ChatData> {
        public static final alpha.soft.quber.ui.adapters.ChatAdapter.DiffUtilImp INSTANCE = null;
        
        @java.lang.Override()
        public boolean areItemsTheSame(@org.jetbrains.annotations.NotNull()
        alpha.soft.quber.model.ChatData oldItem, @org.jetbrains.annotations.NotNull()
        alpha.soft.quber.model.ChatData newItem) {
            return false;
        }
        
        @java.lang.Override()
        public boolean areContentsTheSame(@org.jetbrains.annotations.NotNull()
        alpha.soft.quber.model.ChatData oldItem, @org.jetbrains.annotations.NotNull()
        alpha.soft.quber.model.ChatData newItem) {
            return false;
        }
        
        private DiffUtilImp() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"Lalpha/soft/quber/ui/adapters/ChatAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Lalpha/soft/quber/ui/adapters/ChatAdapter;Landroid/view/View;)V", "bind", "", "clientChat", "Lalpha/soft/quber/model/ClientChatData;", "driverChat", "Lalpha/soft/quber/model/DriverChatData;", "app_debug"})
    public final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        alpha.soft.quber.model.ClientChatData clientChat) {
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        alpha.soft.quber.model.DriverChatData driverChat) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lalpha/soft/quber/ui/adapters/ChatAdapter$ItemInterface;", "", "itemClick", "", "pos", "", "app_debug"})
    public static abstract interface ItemInterface {
        
        public abstract void itemClick(int pos);
    }
}