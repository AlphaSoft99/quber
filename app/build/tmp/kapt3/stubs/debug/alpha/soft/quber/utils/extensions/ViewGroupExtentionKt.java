package alpha.soft.quber.utils.extensions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"}, d2 = {"addView", "Landroid/view/View;", "Landroid/view/ViewGroup;", "resId", "", "app_debug"})
public final class ViewGroupExtentionKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final android.view.View addView(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup $this$addView, @androidx.annotation.LayoutRes()
    int resId) {
        return null;
    }
}