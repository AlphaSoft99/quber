package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010,\u001a\u00020\u00052\b\u0010-\u001a\u0004\u0018\u00010.J\u0006\u0010/\u001a\u00020\u0005J\u000e\u00100\u001a\u00020\u00052\u0006\u00101\u001a\u000202J \u00103\u001a\u00020\u00052\u0006\u00104\u001a\u00020\u001f2\u0006\u00105\u001a\u00020!2\b\u00106\u001a\u0004\u0018\u000107J\b\u00108\u001a\u00020\u0005H\u0002J\u0010\u00109\u001a\u00020\u00052\u0006\u0010:\u001a\u00020\tH\u0002J\u000e\u0010$\u001a\u00020\u00052\u0006\u0010;\u001a\u00020\tJ\u000e\u0010<\u001a\u00020\u00052\u0006\u0010:\u001a\u00020\tJ\b\u0010=\u001a\u00020\u0005H\u0002J\u000e\u0010>\u001a\u00020\u00052\u0006\u0010?\u001a\u00020@R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00070\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00070\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\f0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00050\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u001d\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00070\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0019R\u001d\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\u00070\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0019R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010$\u001a\b\u0012\u0004\u0012\u00020\t0\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0019R\u0017\u0010&\u001a\b\u0012\u0004\u0012\u00020\f0\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\u0019R\u0017\u0010(\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u0019R\u0017\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00050\u0017\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010\u0019\u00a8\u0006A"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/LoginViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_hideProgress", "Landroidx/lifecycle/MutableLiveData;", "", "_openMain", "Lalpha/soft/quber/utils/Event;", "_openVerifyScreen", "", "_showFailedMessage", "_showGoogleSignDialog", "Landroid/content/Intent;", "_showPhoneNumberError", "", "_showProgress", "auth", "Lcom/google/firebase/auth/FirebaseAuth;", "googleSignInClient", "Lcom/google/android/gms/auth/api/signin/GoogleSignInClient;", "gso", "Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;", "hideProgress", "Landroidx/lifecycle/LiveData;", "getHideProgress", "()Landroidx/lifecycle/LiveData;", "openMain", "getOpenMain", "openVerifyScreen", "getOpenVerifyScreen", "postApi", "Lalpha/soft/quber/network/PostApi;", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "provider", "Lcom/google/firebase/auth/OAuthProvider$Builder;", "showFailedMessage", "getShowFailedMessage", "showGoogleSignDialog", "getShowGoogleSignDialog", "showPhoneNumberError", "getShowPhoneNumberError", "showProgress", "getShowProgress", "firebaseAuthWithGoogle", "acct", "Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;", "google", "handleFacebookAccessToken", "token", "Lcom/facebook/AccessToken;", "init", "api", "preferencesUtil", "context", "Landroid/content/Context;", "revokeAccess", "sendPhoneNumber", "phoneNumber", "message", "signIn", "signOut", "twitter", "activity", "Landroidx/appcompat/app/AppCompatActivity;", "app_debug"})
public final class LoginViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.network.PostApi postApi;
    private com.google.android.gms.auth.api.signin.GoogleSignInOptions gso;
    private alpha.soft.quber.utils.PreferencesUtil preference;
    private com.google.firebase.auth.FirebaseAuth auth;
    private com.google.android.gms.auth.api.signin.GoogleSignInClient googleSignInClient;
    private com.google.firebase.auth.OAuthProvider.Builder provider;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> _openMain = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> openMain = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> _showFailedMessage = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> showFailedMessage = null;
    private final androidx.lifecycle.MutableLiveData<android.content.Intent> _showGoogleSignDialog = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<android.content.Intent> showGoogleSignDialog = null;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.utils.Event<java.lang.String>> _openVerifyScreen = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<java.lang.String>> openVerifyScreen = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Integer> _showPhoneNumberError = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.Integer> showPhoneNumberError = null;
    private final androidx.lifecycle.MutableLiveData<kotlin.Unit> _showProgress = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<kotlin.Unit> showProgress = null;
    private final androidx.lifecycle.MutableLiveData<kotlin.Unit> _hideProgress = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<kotlin.Unit> hideProgress = null;
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi api, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil, @org.jetbrains.annotations.Nullable()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<kotlin.Unit>> getOpenMain() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getShowFailedMessage() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<android.content.Intent> getShowGoogleSignDialog() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.utils.Event<java.lang.String>> getOpenVerifyScreen() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getShowPhoneNumberError() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<kotlin.Unit> getShowProgress() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<kotlin.Unit> getHideProgress() {
        return null;
    }
    
    public final void showFailedMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void google() {
    }
    
    public final void firebaseAuthWithGoogle(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.auth.api.signin.GoogleSignInAccount acct) {
    }
    
    private final void sendPhoneNumber(java.lang.String phoneNumber) {
    }
    
    public final void signIn(@org.jetbrains.annotations.NotNull()
    java.lang.String phoneNumber) {
    }
    
    public final void handleFacebookAccessToken(@org.jetbrains.annotations.NotNull()
    com.facebook.AccessToken token) {
    }
    
    public final void twitter(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity activity) {
    }
    
    private final void signOut() {
    }
    
    private final void revokeAccess() {
    }
    
    public LoginViewModel() {
        super();
    }
}