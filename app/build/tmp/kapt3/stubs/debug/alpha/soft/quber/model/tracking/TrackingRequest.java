package alpha.soft.quber.model.tracking;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u00a2\u0006\u0002\u0010\u0006J\u0019\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\u00c6\u0003J#\u0010\n\u001a\u00020\u00002\u0018\b\u0002\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R!\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lalpha/soft/quber/model/tracking/TrackingRequest;", "", "trackings", "Ljava/util/ArrayList;", "Lalpha/soft/quber/model/tracking/TrackingModel;", "Lkotlin/collections/ArrayList;", "(Ljava/util/ArrayList;)V", "getTrackings", "()Ljava/util/ArrayList;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_debug"})
public final class TrackingRequest {
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<alpha.soft.quber.model.tracking.TrackingModel> trackings = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<alpha.soft.quber.model.tracking.TrackingModel> getTrackings() {
        return null;
    }
    
    public TrackingRequest(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<alpha.soft.quber.model.tracking.TrackingModel> trackings) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<alpha.soft.quber.model.tracking.TrackingModel> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.tracking.TrackingRequest copy(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<alpha.soft.quber.model.tracking.TrackingModel> trackings) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}