package alpha.soft.quber.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lalpha/soft/quber/model/MetaDataProperty;", "", "GeocoderMetaData", "Lalpha/soft/quber/model/GeocoderMetaData;", "(Lalpha/soft/quber/model/GeocoderMetaData;)V", "getGeocoderMetaData", "()Lalpha/soft/quber/model/GeocoderMetaData;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_debug"})
public final class MetaDataProperty {
    @org.jetbrains.annotations.NotNull()
    private final alpha.soft.quber.model.GeocoderMetaData GeocoderMetaData = null;
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.GeocoderMetaData getGeocoderMetaData() {
        return null;
    }
    
    public MetaDataProperty(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.GeocoderMetaData GeocoderMetaData) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.GeocoderMetaData component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final alpha.soft.quber.model.MetaDataProperty copy(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.model.GeocoderMetaData GeocoderMetaData) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}