package alpha.soft.quber.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\'\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0019\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u001aJ\u0016\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\u00112\u0006\u0010\b\u001a\u00020\tJ\u0010\u0010\u001d\u001a\u00020\u00152\b\u0010\u001e\u001a\u0004\u0018\u00010\u0007R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00050\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00070\r\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000f\u00a8\u0006\u001f"}, d2 = {"Lalpha/soft/quber/ui/viewmodel/MapViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "_postLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lalpha/soft/quber/model/AddressData;", "_queryLiveData", "", "api", "Lalpha/soft/quber/network/PostApi;", "job", "Lkotlinx/coroutines/Job;", "postLiveData", "Landroidx/lifecycle/LiveData;", "getPostLiveData", "()Landroidx/lifecycle/LiveData;", "preference", "Lalpha/soft/quber/utils/PreferencesUtil;", "queryLiveData", "getQueryLiveData", "getDirection", "", "lat", "", "lng", "key", "(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;)V", "init", "preferencesUtil", "search", "query", "app_debug"})
public final class MapViewModel extends androidx.lifecycle.ViewModel {
    private alpha.soft.quber.utils.PreferencesUtil preference;
    private alpha.soft.quber.network.PostApi api;
    private kotlinx.coroutines.Job job;
    private final androidx.lifecycle.MutableLiveData<alpha.soft.quber.model.AddressData> _postLiveData = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<alpha.soft.quber.model.AddressData> postLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> _queryLiveData = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> queryLiveData = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<alpha.soft.quber.model.AddressData> getPostLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getQueryLiveData() {
        return null;
    }
    
    public final void getDirection(@org.jetbrains.annotations.Nullable()
    java.lang.Double lat, @org.jetbrains.annotations.Nullable()
    java.lang.Double lng, @org.jetbrains.annotations.NotNull()
    java.lang.String key) {
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    alpha.soft.quber.utils.PreferencesUtil preferencesUtil, @org.jetbrains.annotations.NotNull()
    alpha.soft.quber.network.PostApi api) {
    }
    
    public final void search(@org.jetbrains.annotations.Nullable()
    java.lang.String query) {
    }
    
    public MapViewModel() {
        super();
    }
}