package alpha.soft.quber.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0012\u0010\u0004\u001a\u00020\u00032\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\b\u0010\u0007\u001a\u00020\u0003H\u0016J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\u0003H\u0016J\b\u0010\f\u001a\u00020\u0003H\u0016J\b\u0010\r\u001a\u00020\u0003H\u0016J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u0012"}, d2 = {"Lalpha/soft/quber/base/BaseInterface;", "", "clearAll", "", "locationUpdated", "location", "Landroid/location/Location;", "logOut", "openPage", "position", "", "operationCall", "operationCancelOrder", "operationCancelOrderConfirm", "putValue", "str", "", "takeAnOrder", "app_debug"})
public abstract interface BaseInterface {
    
    public abstract void locationUpdated(@org.jetbrains.annotations.Nullable()
    android.location.Location location);
    
    public abstract void clearAll();
    
    public abstract void putValue(@org.jetbrains.annotations.NotNull()
    java.lang.String str);
    
    public abstract void takeAnOrder(int position);
    
    public abstract void openPage(int position);
    
    public abstract void operationCall();
    
    public abstract void operationCancelOrder();
    
    public abstract void operationCancelOrderConfirm();
    
    public abstract void logOut();
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        public static void locationUpdated(alpha.soft.quber.base.BaseInterface $this, @org.jetbrains.annotations.Nullable()
        android.location.Location location) {
        }
        
        public static void clearAll(alpha.soft.quber.base.BaseInterface $this) {
        }
        
        public static void putValue(alpha.soft.quber.base.BaseInterface $this, @org.jetbrains.annotations.NotNull()
        java.lang.String str) {
        }
        
        public static void takeAnOrder(alpha.soft.quber.base.BaseInterface $this, int position) {
        }
        
        public static void openPage(alpha.soft.quber.base.BaseInterface $this, int position) {
        }
        
        public static void operationCall(alpha.soft.quber.base.BaseInterface $this) {
        }
        
        public static void operationCancelOrder(alpha.soft.quber.base.BaseInterface $this) {
        }
        
        public static void operationCancelOrderConfirm(alpha.soft.quber.base.BaseInterface $this) {
        }
        
        public static void logOut(alpha.soft.quber.base.BaseInterface $this) {
        }
    }
}