package alpha.soft.quber.services;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0001\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u0000 \'2\u00020\u00012\u00020\u0002:\u0002\'(B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u001dH\u0002J\u0014\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\b\u0010 \u001a\u00020\u0019H\u0016J\b\u0010!\u001a\u00020\u0019H\u0016J\"\u0010\"\u001a\u00020#2\b\u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010$\u001a\u00020#2\u0006\u0010%\u001a\u00020#H\u0016J\b\u0010&\u001a\u00020\u0019H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0011R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017\u00a8\u0006)"}, d2 = {"Lalpha/soft/quber/services/AndroidLocationServices;", "Landroid/app/Service;", "Lalpha/soft/quber/base/BaseInterface;", "()V", "CHANNEL_DESC", "", "CHANNEL_ID", "CHANNEL_NAME", "intent", "Landroid/content/Intent;", "getIntent", "()Landroid/content/Intent;", "setIntent", "(Landroid/content/Intent;)V", "locationListeners", "", "Lalpha/soft/quber/services/AndroidLocationServices$LTRLocationListener;", "[Lalpha/soft/quber/services/AndroidLocationServices$LTRLocationListener;", "locationManager", "Landroid/location/LocationManager;", "getLocationManager", "()Landroid/location/LocationManager;", "setLocationManager", "(Landroid/location/LocationManager;)V", "locationUpdated", "", "location", "Landroid/location/Location;", "notifyUserThatLocationServiceStarted", "Landroid/app/Notification;", "onBind", "", "onCreate", "onDestroy", "onStartCommand", "", "flags", "startId", "startService", "Companion", "LTRLocationListener", "app_debug"})
public final class AndroidLocationServices extends android.app.Service implements alpha.soft.quber.base.BaseInterface {
    @org.jetbrains.annotations.Nullable()
    private android.location.LocationManager locationManager;
    @org.jetbrains.annotations.Nullable()
    private android.content.Intent intent;
    private final java.lang.String CHANNEL_ID = "Notification";
    private final java.lang.String CHANNEL_NAME = "Default";
    private final java.lang.String CHANNEL_DESC = "Default for notification";
    private final alpha.soft.quber.services.AndroidLocationServices.LTRLocationListener[] locationListeners = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TAG = "===locationService";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String str_receiver = "location.service.receiver";
    public static final long INTERVAL = 15000L;
    public static final float DISTANCE = 15.0F;
    public static final alpha.soft.quber.services.AndroidLocationServices.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final android.location.LocationManager getLocationManager() {
        return null;
    }
    
    public final void setLocationManager(@org.jetbrains.annotations.Nullable()
    android.location.LocationManager p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Intent getIntent() {
        return null;
    }
    
    public final void setIntent(@org.jetbrains.annotations.Nullable()
    android.content.Intent p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Void onBind(@org.jetbrains.annotations.Nullable()
    android.content.Intent intent) {
        return null;
    }
    
    @java.lang.Override()
    public int onStartCommand(@org.jetbrains.annotations.Nullable()
    android.content.Intent intent, int flags, int startId) {
        return 0;
    }
    
    private final android.app.Notification notifyUserThatLocationServiceStarted() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final void startService() {
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void locationUpdated(@org.jetbrains.annotations.Nullable()
    android.location.Location location) {
    }
    
    public AndroidLocationServices() {
        super();
    }
    
    public void clearAll() {
    }
    
    public void logOut() {
    }
    
    public void openPage(int position) {
    }
    
    public void operationCall() {
    }
    
    public void operationCancelOrder() {
    }
    
    public void operationCancelOrderConfirm() {
    }
    
    public void putValue(@org.jetbrains.annotations.NotNull()
    java.lang.String str) {
    }
    
    public void takeAnOrder(int position) {
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\bH\u0016J\u0010\u0010\f\u001a\u00020\n2\u0006\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u0002\u001a\u00020\u0003H\u0016J\"\u0010\u000e\u001a\u00020\n2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lalpha/soft/quber/services/AndroidLocationServices$LTRLocationListener;", "Landroid/location/LocationListener;", "provider", "", "baseInterface", "Lalpha/soft/quber/base/BaseInterface;", "(Ljava/lang/String;Lalpha/soft/quber/base/BaseInterface;)V", "lastLocation", "Landroid/location/Location;", "onLocationChanged", "", "location", "onProviderDisabled", "onProviderEnabled", "onStatusChanged", "status", "", "extras", "Landroid/os/Bundle;", "app_debug"})
    public static final class LTRLocationListener implements android.location.LocationListener {
        private final android.location.Location lastLocation = null;
        private final alpha.soft.quber.base.BaseInterface baseInterface = null;
        
        @java.lang.Override()
        public void onLocationChanged(@org.jetbrains.annotations.NotNull()
        android.location.Location location) {
        }
        
        @java.lang.Override()
        public void onProviderDisabled(@org.jetbrains.annotations.NotNull()
        java.lang.String provider) {
        }
        
        @java.lang.Override()
        public void onProviderEnabled(@org.jetbrains.annotations.NotNull()
        java.lang.String provider) {
        }
        
        @java.lang.Override()
        public void onStatusChanged(@org.jetbrains.annotations.NotNull()
        java.lang.String provider, int status, @org.jetbrains.annotations.Nullable()
        android.os.Bundle extras) {
        }
        
        public LTRLocationListener(@org.jetbrains.annotations.NotNull()
        java.lang.String provider, @org.jetbrains.annotations.NotNull()
        alpha.soft.quber.base.BaseInterface baseInterface) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lalpha/soft/quber/services/AndroidLocationServices$Companion;", "", "()V", "DISTANCE", "", "INTERVAL", "", "TAG", "", "str_receiver", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}