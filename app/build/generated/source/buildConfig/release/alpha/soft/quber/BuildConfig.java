/**
 * Automatically generated file. DO NOT MODIFY
 */
package alpha.soft.quber;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "alpha.soft.quber";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from default config.
  public static final String API_URL = "https://iplaymusic-25444.firebaseio.com/";
}
