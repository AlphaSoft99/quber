// Generated by Dagger (https://google.github.io/dagger).
package alpha.soft.quber.ui.screens;

import alpha.soft.quber.network.PostApi;
import alpha.soft.quber.utils.PreferencesUtil;
import dagger.MembersInjector;
import javax.inject.Provider;

public final class NotificationScreen_MembersInjector implements MembersInjector<NotificationScreen> {
  private final Provider<PostApi> apiProvider;

  private final Provider<PreferencesUtil> preferencesUtilProvider;

  public NotificationScreen_MembersInjector(Provider<PostApi> apiProvider,
      Provider<PreferencesUtil> preferencesUtilProvider) {
    this.apiProvider = apiProvider;
    this.preferencesUtilProvider = preferencesUtilProvider;
  }

  public static MembersInjector<NotificationScreen> create(Provider<PostApi> apiProvider,
      Provider<PreferencesUtil> preferencesUtilProvider) {
    return new NotificationScreen_MembersInjector(apiProvider, preferencesUtilProvider);}

  @Override
  public void injectMembers(NotificationScreen instance) {
    injectApi(instance, apiProvider.get());
    injectPreferencesUtil(instance, preferencesUtilProvider.get());
  }

  public static void injectApi(NotificationScreen instance, PostApi api) {
    instance.api = api;
  }

  public static void injectPreferencesUtil(NotificationScreen instance,
      PreferencesUtil preferencesUtil) {
    instance.preferencesUtil = preferencesUtil;
  }
}
