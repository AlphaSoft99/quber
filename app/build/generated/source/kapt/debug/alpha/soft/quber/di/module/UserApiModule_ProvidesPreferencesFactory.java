// Generated by Dagger (https://google.github.io/dagger).
package alpha.soft.quber.di.module;

import alpha.soft.quber.utils.PreferencesUtil;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class UserApiModule_ProvidesPreferencesFactory implements Factory<PreferencesUtil> {
  private final UserApiModule module;

  public UserApiModule_ProvidesPreferencesFactory(UserApiModule module) {
    this.module = module;
  }

  @Override
  public PreferencesUtil get() {
    return providesPreferences(module);
  }

  public static UserApiModule_ProvidesPreferencesFactory create(UserApiModule module) {
    return new UserApiModule_ProvidesPreferencesFactory(module);
  }

  public static PreferencesUtil providesPreferences(UserApiModule instance) {
    return Preconditions.checkNotNull(instance.providesPreferences(), "Cannot return null from a non-@Nullable @Provides method");
  }
}
