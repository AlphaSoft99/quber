package alpha.soft.quber.network

import alpha.soft.quber.model.AddressData
import alpha.soft.quber.model.PostData
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface PostApi {

    @GET("posts")
    suspend fun getPosts(): Response<List<PostData>>

    @GET
    suspend fun getDirection(@Url url: String): Response<AddressData>
}