package alpha.soft.quber.services

import alpha.soft.quber.R
import alpha.soft.quber.base.BaseInterface
import alpha.soft.quber.model.tracking.TrackingLocation
import alpha.soft.quber.model.tracking.TrackingModel
import alpha.soft.quber.ui.activities.MainActivity
import alpha.soft.quber.utils.Constants
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat


class AndroidLocationServices : Service(), BaseInterface {

    var locationManager: LocationManager? = null
    var intent: Intent? = null
    //    private var isForeground = false
    private val CHANNEL_ID = "Notification"
    private val CHANNEL_NAME = "Default"
    private val CHANNEL_DESC = "Default for notification"

    override fun onBind(intent: Intent?) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        if (intent != null && intent.action.equals(Constants.ACTION_START_SERVICE)) {
            startForeground(1, notifyUserThatLocationServiceStarted())
            startService()
        } else {
            stopForeground(true)
            stopSelf()
        }
        return START_NOT_STICKY
    }

    private fun notifyUserThatLocationServiceStarted(): Notification {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)
            channel.description = CHANNEL_DESC
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            notificationManager.createNotificationChannel(channel)
        }

        val notificationBuilder =
            NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("Get Location")
                .setContentText("Ish vaqtida joylashuvingiz haqida maʼlumot olinishi mumkin.")
                .setStyle(NotificationCompat.BigTextStyle().bigText("Ish vaqtida joylashuvingiz haqida maʼlumot olinishi mumkin."))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)

        return notificationBuilder.build()
    }

    override fun onCreate() {

    }

    private fun startService() {
        intent = Intent(str_receiver)
        if (locationManager == null)
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        try {
            locationManager?.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                INTERVAL,
                DISTANCE,
                locationListeners[1]
            )
            Log.d("====", "here")
        } catch (e: SecurityException) {
            Log.e(TAG, "Fail to request location update", e)
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "Network provider does not exist", e)
        }
        try {
            locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, INTERVAL, DISTANCE, locationListeners[0])
            Log.d("====", "here")
        } catch (e: SecurityException) {
            Log.e(TAG, "Fail to request location update", e)
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "GPS provider does not exist", e)
        }
    }

    override fun onDestroy() {
        Log.e(TAG, "destroyed service")
        if (locationManager != null)
            for (i in 0..locationListeners.size) {
                try {
                    locationManager?.removeUpdates(locationListeners[i])
                } catch (e: Exception) {
                    Log.w(TAG, "Failed to remove location listeners")
                }
            }
        super.onDestroy()
    }

    override fun locationUpdated(location: Location?) {
        intent?.putExtra(
            "location", TrackingModel(
                "", location = TrackingLocation(location!!.latitude.toFloat(), location.longitude.toFloat()),
                created_at = (location.time / 1000).toString()
            )
        )
        sendBroadcast(intent)
    }

    companion object {
        const val TAG = "===locationService"
        const val str_receiver = "location.service.receiver"
        const val INTERVAL = 15000.toLong() // In milliseconds
        const val DISTANCE = 15.toFloat() // In meters
    }

    private val locationListeners = arrayOf(
        LTRLocationListener(LocationManager.GPS_PROVIDER, this@AndroidLocationServices),
        LTRLocationListener(LocationManager.NETWORK_PROVIDER, this@AndroidLocationServices)
    )

    class LTRLocationListener(provider: String, private val baseInterface: BaseInterface) :
        android.location.LocationListener {

        private val lastLocation = Location(provider)

        override fun onLocationChanged(location: Location) {
            lastLocation.set(location)
            Log.d("====", "" + location.latitude + "\n" + location.longitude)
            baseInterface.locationUpdated(location)
        }

        override fun onProviderDisabled(provider: String) {
        }

        override fun onProviderEnabled(provider: String) {
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle?) {
        }

    }
}
