package alpha.soft.quber.base

import alpha.soft.quber.R
import alpha.soft.quber.utils.ProgressBar
import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity(){

    private var progress: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progress = ProgressBar(this)
    }

    fun setFullScreen(){
        val window: Window = window
        val winParams: WindowManager.LayoutParams = window.attributes
        winParams.flags = winParams.flags and WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS.inv()
        window.attributes = winParams
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }

    fun showProgress(){
        progress?.showProgressDialog()
    }

    fun hideProgress(){
        progress?.hideProgressDialog()
    }

    fun showAlertDialog(
        title: String = "",
        message: String = "",
        positiveTitle: String = getString(R.string.ok),
        positiveClick: DialogInterface.OnClickListener? = null,
        negativeTitle: String = getString(R.string.fui_cancel),
        negativeClick: DialogInterface.OnClickListener? = null,
        isCancelable: Boolean = true,
        dismissListener: DialogInterface.OnDismissListener? = null
    ) {
        val alertBuilder = AlertDialog.Builder(this)
        alertBuilder.setCancelable(isCancelable)
        if (title.isNotEmpty()) alertBuilder.setTitle(title)
        if (message.isNotEmpty()) alertBuilder.setMessage(message)
        if (positiveClick != null) alertBuilder.setPositiveButton(positiveTitle, positiveClick)
        if (negativeClick != null) alertBuilder.setNegativeButton(negativeTitle, negativeClick)
        if (dismissListener != null) alertBuilder.setOnDismissListener(dismissListener)
        val dialog = alertBuilder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor((Color.parseColor("#FFAA00")))
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor((Color.parseColor("#FFAA00")))
    }
}