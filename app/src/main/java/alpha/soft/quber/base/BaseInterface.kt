package alpha.soft.quber.base

import android.location.Location

interface BaseInterface {

    fun locationUpdated(location: Location?) {}
    fun clearAll() {}
    fun putValue(str: String) {}
    fun takeAnOrder(position: Int) {}
    fun openPage(position: Int) {}
    fun operationCall() {}
    fun operationCancelOrder() {}
    fun operationCancelOrderConfirm() {}
    fun logOut() {}
}