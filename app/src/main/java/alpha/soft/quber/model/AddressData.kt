package alpha.soft.quber.model

data class AddressData(
    val response: Response
)

data class Response(
    val GeoObjectCollection: GeoObjectCollection
)

data class GeoObjectCollection(
    val featureMember: List<FeatureMember>,
    val metaDataProperty: MetaDataPropertyX
)

data class FeatureMember(
    val GeoObject: GeoObject
)

data class MetaDataPropertyX(
    val GeocoderResponseMetaData: GeocoderResponseMetaData
)

data class GeoObject(
    val Point: Point,
    val boundedBy: BoundedBy,
    val metaDataProperty: MetaDataProperty,
    val name: String
)

data class Point(
    val pos: String
)

data class BoundedBy(
    val Envelope: Envelope
)

data class MetaDataProperty(
    val GeocoderMetaData: GeocoderMetaData
)

data class Envelope(
    val lowerCorner: String,
    val upperCorner: String
)

data class GeocoderMetaData(
    val Address: Address,
    val AddressDetails: AddressDetails,
    val kind: String,
    val precision: String,
    val text: String
)

data class Address(
    val Components: List<Component>,
    val formatted: String
)

data class AddressDetails(
    val Country: Country
)

data class Component(
    val kind: String,
    val name: String
)

data class Country(
    val AddressLine: String,
    val CountryName: String
)

data class GeocoderResponseMetaData(
    val Point: PointX,
    val found: String,
    val request: String,
    val results: String
)

data class PointX(
    val pos: String
)