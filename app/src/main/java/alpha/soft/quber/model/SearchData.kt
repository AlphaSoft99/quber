package alpha.soft.quber.model

data class SearchData(
    val id: Int,
    val name: String,
    val lat: Double,
    val lng : Double
)