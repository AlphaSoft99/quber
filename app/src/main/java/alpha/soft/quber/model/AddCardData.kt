package alpha.soft.quber.model

data class AddCardData(
    val id : Int,
    val icon : Int
)