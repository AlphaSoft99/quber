package alpha.soft.quber.model

import com.google.gson.annotations.SerializedName

data class ClientChatData(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("body")
    val body: String? = null,

    @field:SerializedName("userId")
    val userId: Int? = null
): ChatData {
    override fun getItemType()= 1
}

data class DriverChatData(
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("body")
    val body: String? = null,

    @field:SerializedName("userId")
    val userId: Int? = null
): ChatData {
    override fun getItemType()= 2
}

interface ChatData{
    fun getItemType(): Int
}

enum class ChatDataEnum(private val type: Int){
    ClientChat(1),
    DriverChatData(2);

    companion object {
        fun getType(type: Int): ChatDataEnum {
            values().forEach {
                if (it.type == type) return it
            }
            return ClientChat
        }

        fun getType(type: ChatDataEnum): Int {
            values().forEach {
                if (it == type) return it.type
            }
            return 1
        }
    }
}