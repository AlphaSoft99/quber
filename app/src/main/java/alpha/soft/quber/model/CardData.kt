package alpha.soft.quber.model

data class CardData(
    val id: Int,
    val name: String,
    val image: Int
)