package alpha.soft.quber.model.tracking

import java.io.Serializable

data class TrackingLocation(
    var lat: Float,
    var long: Float
): Serializable