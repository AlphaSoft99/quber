package alpha.soft.quber.model.tracking

import java.io.Serializable

data class TrackingModel(
    val courier_id: String,
    val created_at: String,
    val location: TrackingLocation
): Serializable