package alpha.soft.quber.model.tracking

data class TrackingRequest(
    val trackings: ArrayList<TrackingModel>
)