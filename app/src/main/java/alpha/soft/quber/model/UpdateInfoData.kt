package alpha.soft.quber.model

data class UpdateInfoData(
    val fullName: String,
    val phoneNumber : String,
    val email: String,
    val address: String,
    val street: String
)