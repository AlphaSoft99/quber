package alpha.soft.quber.di.component

import alpha.soft.quber.App
import alpha.soft.quber.di.module.UserApiModule
import alpha.soft.quber.di.scope.AppScope
import alpha.soft.quber.ui.screens.*
import alpha.soft.quber.ui.screens.onBoardingScreens.OnBoardingScreen
import dagger.Component


@AppScope
@Component(modules = [UserApiModule::class])
interface UserApiComponent {
    fun inject(app: App)
    fun inject(fragment: SplashScreen)
    fun inject(fragment: LoginScreen)
    fun inject(fragment: OnBoardingScreen)
    fun inject(fragment: VerifyScreen)
    fun inject(fragment: MyWalletScreen)
    fun inject(fragment: AddCardScreen)
    fun inject(fragment: ChatScreen)
    fun inject(fragment: NotificationScreen)
    fun inject(fragment: SettingsScreen)
    fun inject(fragment: HistoryScreen)
    fun inject(fragment: HistoryTypeFragment)
    fun inject(fragment: OnBoardingTypeFragment)
    fun inject(fragment: OrderDetailsScreen)
    fun inject(fragment: TVScreen)
    fun inject(fragment: MapScreen)
}