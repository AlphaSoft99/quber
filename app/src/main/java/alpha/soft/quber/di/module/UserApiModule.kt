package alpha.soft.quber.di.module

import alpha.soft.quber.BuildConfig
import alpha.soft.quber.di.scope.AppScope
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.utils.PreferencesUtil
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class UserApiModule(private val context: Context) {

    private val AUTH_HEADER_EXCEPTED_LIST = arrayOf(
        "signup",
        "verify"
    )

    @Provides
    @AppScope
    fun provideUserApiService(retrofit: Retrofit): PostApi {
        return retrofit.create(PostApi::class.java)
    }

    @Provides
    @AppScope
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")/*BuildConfig.API_URL*/
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    @Provides
    @AppScope
    fun provideRetrofitBuilder(preferencesUtil: PreferencesUtil): OkHttpClient {
        val authInterceptor = Interceptor { chain ->
            val original = chain.request()

            // Request customization: add request headers
            val requestBuilder = original.newBuilder()

            val url = original.url.toString()
            if (AUTH_HEADER_EXCEPTED_LIST.none { url == it }) {
                if (preferencesUtil.hasAuthToken()) {
                    requestBuilder.header("token", preferencesUtil.getAuthToken() ?: "")
                }
            }

            val request = requestBuilder.build()
            chain.proceed(request)
        }
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client =
            OkHttpClient.Builder().addInterceptor(interceptor).build()
        val gson = GsonBuilder()
            .create()
        return OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(authInterceptor)
            .addInterceptor(interceptor)
            .addInterceptor(ChuckInterceptor(context))
            .build()
    }

    @AppScope
    @Provides
    fun providesPreferences(): PreferencesUtil {
        val sharedPreferences = context.getSharedPreferences("OauthPrefs", Context.MODE_PRIVATE)
        val securePreferencesName = BuildConfig.APPLICATION_ID + "_preferences"
        return PreferencesUtil(context, sharedPreferences, securePreferencesName)
    }

    @Provides
    @AppScope
    fun providesGson(): Gson {
        return GsonBuilder().create()
    }
}
