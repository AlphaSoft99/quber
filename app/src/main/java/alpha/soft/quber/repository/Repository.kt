package alpha.soft.quber.repository

import alpha.soft.quber.network.PostApi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(private val apiService: PostApi){

    suspend fun getDataFromApi()  = apiService.getPosts()
}