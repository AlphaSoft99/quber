package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.activities.MainActivity
import alpha.soft.quber.ui.viewmodel.VerifyViewModel
import alpha.soft.quber.utils.Constants
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.*
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.screen_verify.*
import kotlinx.android.synthetic.main.screen_verify.tv_error
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class VerifyScreen : Fragment(R.layout.screen_verify) {

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil


    private val viewModel: VerifyViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
        bt_verify.setOnClickListener {
            viewModel.verify(et_sms_code.to())
        }
    }

    private fun injectDependency(fragment: VerifyScreen) {
        (activitySplash().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData() {
        val phoneNumber = arguments?.getString(Constants.EXTRA_PHONE) ?: ""

        viewModel.init(api, preferencesUtil, phoneNumber, activitySplash())
        toolbar.apply {
            tv_title.text = getString(R.string.verify)
            bt_home.setOnClickListener {
                findNavController().navigateUp()
            }
        }

        tv_phone_number.text = phoneNumber
        et_sms_code.onTextChanged(tv_error)

        viewModel.openMain.observe(this, Observer {
            it.getContentIfNotHandled()?.also {
                activitySplash().finish()
                startActivity(Intent(context, MainActivity::class.java))
            }
        })
        viewModel.showSMSCodeError.observe(viewLifecycleOwner, showSMSCodeErrorObserver)
        viewModel.showFailedMessage.observe(viewLifecycleOwner, showFailedMessageObserver)

    }

    private val showFailedMessageObserver = Observer<String> {
        Snackbar.make(bt_verify, it, Snackbar.LENGTH_LONG).show()
    }
    private val showSMSCodeErrorObserver = Observer<Int> {
        tv_error.visible()
        tv_error.setText(it)
    }


}