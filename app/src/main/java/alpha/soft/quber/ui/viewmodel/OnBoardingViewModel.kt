package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class OnBoardingViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil

    fun init(preferencesUtil: PreferencesUtil) {
        preference = preferencesUtil
    }

    private var page = 0

    private val _openLogInLiveData = MutableLiveData<Unit>()
    val openLogInLiveData: LiveData<Unit> = _openLogInLiveData

    private val _onPageSelected = MutableLiveData<Int>()
    val onPageSelected: LiveData<Int> = _onPageSelected

    fun open() {
        _openLogInLiveData.postValue(Unit)
    }

    fun onPageSelected(pos: Int) {
        page = pos
        _onPageSelected.postValue(pos)
    }

    fun next() {
        if (page == 2)
            _openLogInLiveData.postValue(Unit)
        _onPageSelected.postValue(++page)
    }
}