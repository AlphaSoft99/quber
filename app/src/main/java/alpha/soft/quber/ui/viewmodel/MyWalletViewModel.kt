package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MyWalletViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil

    private val _openAddCard = MutableLiveData<Unit>()
    val openAddCard: LiveData<Unit> = _openAddCard

    fun init(preferencesUtil: PreferencesUtil){
        preference= preferencesUtil
    }

    fun openAddCard(){
        _openAddCard.postValue(Unit)
    }

}