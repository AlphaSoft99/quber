package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.model.AddressData
import alpha.soft.quber.model.CardData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.adapters.CarsAdapter
import alpha.soft.quber.ui.adapters.SearchAdapter
import alpha.soft.quber.ui.itemDecorations.SearchItemDecoration
import alpha.soft.quber.ui.viewmodel.MapViewModel
import alpha.soft.quber.utils.IntentUtil
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.*
import android.app.Activity
import android.content.Context
import android.graphics.PointF
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.yandex.mapkit.*
import com.yandex.mapkit.directions.DirectionsFactory
import com.yandex.mapkit.directions.driving.DrivingOptions
import com.yandex.mapkit.directions.driving.DrivingRoute
import com.yandex.mapkit.directions.driving.DrivingRouter
import com.yandex.mapkit.directions.driving.DrivingSession
import com.yandex.mapkit.directions.driving.DrivingSession.DrivingRouteListener
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.layers.ObjectEvent
import com.yandex.mapkit.location.Location
import com.yandex.mapkit.location.LocationListener
import com.yandex.mapkit.location.LocationStatus
import com.yandex.mapkit.map.*
import com.yandex.mapkit.map.Map
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.search.*
import com.yandex.mapkit.user_location.UserLocationLayer
import com.yandex.mapkit.user_location.UserLocationObjectListener
import com.yandex.mapkit.user_location.UserLocationView
import com.yandex.runtime.Error
import com.yandex.runtime.image.ImageProvider
import com.yandex.runtime.network.NetworkError
import com.yandex.runtime.network.RemoteError
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.bottom_map_sheet_driver.*
import kotlinx.android.synthetic.main.screen_map.*
import javax.inject.Inject

class MapScreen : Fragment(R.layout.screen_map),
    Session.SearchListener, UserLocationObjectListener, DrivingRouteListener, CameraListener, MapObjectTapListener{

    @Inject
    lateinit var api: PostApi

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private var TARGET_LOCATION = Point(41.311151, 69.279687)
    private var FINAL_LOCATION = Point(41.311151, 69.279687)
    private val viewModel: MapViewModel by viewModels()
    private var mapView: MapView? = null
    private var marker: PlacemarkMapObject? = null

    //    private val geocode by lazy { Geocoder(context) }
    private var mapObjects: MapObjectCollection? = null
    private var userLocationLayer: UserLocationLayer? = null
    private var searchManager: SearchManager? = null
    private var searchSession: Session? = null
    private var drivingRouter: DrivingRouter? = null
    private var drivingSession: DrivingSession? = null
    private lateinit var resultAdapter: SearchAdapter
    private lateinit var carsAdapter: CarsAdapter
    private val searchResult = ArrayList<GeoObjectCollection.Item>()
    private val carsData = ArrayList<CardData>()
    private var myLocation = false
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
        bt_zoom_up.setOnClickListener {
            mapView!!.map.apply {
                move(
                    CameraPosition(
                        cameraPosition.target,
                        cameraPosition.zoom + 1,
                        0.0f,
                        0.0f
                    ), Animation(Animation.Type.SMOOTH, 1f), null
                )
            }
        }
        bt_zoom_down.setOnClickListener {
            mapView!!.map.apply {
                move(
                    CameraPosition(
                        cameraPosition.target,
                        cameraPosition.zoom - 1,
                        0.0f,
                        0.0f
                    ), Animation(Animation.Type.SMOOTH, 1f), null
                )
            }
        }
        bt_current_location.setOnClickListener {
            mapView!!.map.apply {
                move(
                    CameraPosition(
                        userLocationLayer?.cameraPosition()?.target ?: TARGET_LOCATION,
                        20f,
                        0.0f,
                        0.0f
                    ), Animation(Animation.Type.SMOOTH, 1f), null
                )
            }
        }

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.apply {
            /*tv_start_location.setOnClickListener {
                activity().toolbar.search.visible()
                showSoftKeyboard()
            }*/
            tv_end_location.setOnClickListener {
                activity().toolbar.search.visible()
                showSoftKeyboard()
            }
            bt_map.setOnClickListener {
                mapView?.map?.move(
                    CameraPosition(
                        userLocationLayer?.cameraPosition()?.target ?: TARGET_LOCATION,
                        20f, 0.0f, 0.0f
                    ), Animation(Animation.Type.SMOOTH, 1f), null
                )
                bottomSheetBehavior.expandedOffset = BottomSheetBehavior.STATE_COLLAPSED
            }
            bt_bottom_sheet.setOnClickListener {
                if (bt_bottom_sheet.text == getString(R.string.confirm)) {
                    layout_taxi.gone()
                    layout_driver.visible()
                    bt_bottom_sheet.text = getString(R.string.cancel)
                } else {
                    layout_driver.gone()
                    layout_taxi.visible()
                    bt_bottom_sheet.text = getString(R.string.confirm)
                }
            }
            bt_chat.setOnClickListener {
                findNavController().navigate(R.id.action_mapScreen_to_chatScreen)
            }
            bt_call.setOnClickListener {
                IntentUtil.openDialer(context, "+998997951530")
            }
        }
        activity().toolbar.apply {
            search.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
                androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    viewModel.search(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?) = true
            })
            bt_ready.setOnClickListener {
                submitRequest()
            }
        }

    }

    private fun injectDependency(fragment: MapScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData() {
        MapKitFactory.initialize(context)
        SearchFactory.initialize(context)
        DirectionsFactory.initialize(context)
        drivingRouter = DirectionsFactory.getInstance().createDrivingRouter()
        searchManager = SearchFactory.getInstance().createSearchManager(SearchManagerType.COMBINED)
        carsData.add(CardData(0, "Standard", R.drawable.ic_taxi_standart))
        carsData.add(CardData(1, "XL", R.drawable.ic_taxi_xl))
        carsData.add(CardData(2, "Premium", R.drawable.ic_taxi_premium))
        carsData.add(CardData(3, "Comfort", R.drawable.ic_taxi_comfort))
        carsData.add(CardData(4, "Vip", R.drawable.ic_taxi_vip))

//        mapView?.map?.addCameraListener(this)

        carsAdapter = CarsAdapter(object : CarsAdapter.ItemInterface {
            override fun itemClick(pos: Int) {

            }
        })
        rv_cars.adapter = carsAdapter
        rv_cars.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        carsAdapter.submitList(carsData)

        resultAdapter = SearchAdapter(searchResult, object : SearchAdapter.ItemInterface {
            override fun itemClick(pos: Int) {
                activity().bt_ready.visible()
                val point = searchResult[pos].obj?.geometry?.get(0)?.point ?: TARGET_LOCATION
                mapView!!.map.move(
                    CameraPosition(Point(point.latitude, point.longitude), 20.0f, 0.0f, 0.0f),
                    Animation(Animation.Type.SMOOTH, 1f),
                    null
                )
                if (marker != null)
                    mapObjects?.remove(marker!!)
                marker = mapObjects?.addPlacemark(
                    point,
                    ImageProvider.fromResource(context, R.drawable.location)
                )
                tv_end_location.text = searchResult[pos].obj?.descriptionText
                FINAL_LOCATION = point
                layout_search.gone()
                hideSoftKeyboard()
            }
        })
        rv_search.layoutManager = LinearLayoutManager(context)
        rv_search.addItemDecoration(SearchItemDecoration())
        rv_search.adapter = resultAdapter

        viewModel.init(preferencesUtil, api)
        mapView = view?.findViewById<View>(R.id.mapview) as MapView
        mapView?.map?.move(
            CameraPosition(TARGET_LOCATION, 14.0f, 0.0f, 0.0f),
            Animation(Animation.Type.SMOOTH, 5f),
            null
        )

        viewModel.postLiveData.observe(viewLifecycleOwner, postLiveDataObserver)
        viewModel.queryLiveData.observe(viewLifecycleOwner, queryLiveDataObserver)

        searchManager = SearchFactory.getInstance().createSearchManager(SearchManagerType.COMBINED)
        val mapKit = MapKitFactory.getInstance()
        mapObjects = mapView!!.map.mapObjects.addCollection()
        userLocationLayer = mapKit.createUserLocationLayer(mapView!!.mapWindow)
        userLocationLayer?.isVisible = true
        userLocationLayer?.isHeadingEnabled = true
        userLocationLayer?.setObjectListener(this)

        mapView?.map?.addInertiaMoveListener(object : InertiaMoveListener {
            override fun onFinish(p0: Map, p1: CameraPosition) {
                viewModel.getDirection(
                    mapView?.map?.cameraPosition?.target?.latitude,
                    mapView?.map?.cameraPosition?.target?.longitude,
                    getString(R.string.geocode_key)
                )
            }

            override fun onCancel(p0: Map, p1: CameraPosition) {}
            override fun onStart(p0: Map, p1: CameraPosition) {}
        })

        mapKit.createLocationManager().requestSingleUpdate(object : LocationListener {
            override fun onLocationStatusUpdated(p0: LocationStatus) {
            }

            override fun onLocationUpdated(location: Location) {
                val lat = location.position.latitude
                val lng = location.position.longitude
                TARGET_LOCATION = Point(lat, lng)
                mapView!!.map.move(
                    CameraPosition(TARGET_LOCATION, 20.0f, 0.0f, 0.0f),
                    Animation(Animation.Type.SMOOTH, 1f),
                    null
                )
                myLocation = true
                viewModel.getDirection(
                    TARGET_LOCATION.latitude,
                    TARGET_LOCATION.longitude,
                    getString(R.string.geocode_key)
                )
                /*val d= mapObjects?.addPlacemark(
                    TARGET_LOCATION,
                    ImageProvider.fromResource(context, R.drawable.pin)
                )*/
            }
        })

        mapView?.map?.mapObjects?.addPlacemark(
            Point(41.28533788346304, 69.21692381397813),
            ImageProvider.fromResource(
                context, R.drawable.cab
            ),
            IconStyle().setAnchor(PointF(0f, 0f))
                .setRotationType(RotationType.ROTATE)
                .setZIndex(1f)
                .setScale(1f)
        )
        mapView?.map?.mapObjects?.addPlacemark(
            Point(41.241221910983825, 69.13914212914628),
            ImageProvider.fromResource(
                context, R.drawable.cab
            ),
            IconStyle().setAnchor(PointF(0f, 0f))
                .setRotationType(RotationType.ROTATE)
                .setZIndex(1f)
                .setScale(1f)
        )
        mapView?.map?.mapObjects?.addPlacemark(
            Point(41.25036635170649, 69.1523219655347),
            ImageProvider.fromResource(
                context, R.drawable.cab
            ),
            IconStyle().setAnchor(PointF(0f, 0f))
                .setRotationType(RotationType.ROTATE)
                .setZIndex(1f)
                .setScale(1f)
        )
        mapView?.map?.mapObjects?.addPlacemark(
            Point(41.34553517604474, 69.21851570682367),
            ImageProvider.fromResource(
                context, R.drawable.cab
            ),
            IconStyle().setAnchor(PointF(0f, 0f))
                .setRotationType(RotationType.ROTATE)
                .setZIndex(1f)
                .setScale(1f)
        )


    }

    private val postLiveDataObserver = Observer<AddressData> {
        if (it.response.GeoObjectCollection.metaDataProperty.GeocoderResponseMetaData.found.toInt() > 0) {
            val text =
                it.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.text
            if (myLocation) {
                tv_start_location.text = text
                myLocation = false
            } else {
                Toast.makeText(context, text, Toast.LENGTH_LONG).show()
                Log.d("TTT", text)
            }
        }
    }

    private val queryLiveDataObserver = Observer<String> {
        submitQuery(it)
    }

    override fun onResume() {
        super.onResume()
        if (mainViewModel().isOpen)
            mainViewModel().openDrawer()
        else
            mainViewModel().isOpen = true

    }

    override fun onStop() { // Activity onStop call must be passed to both MapView and MapKit instance.
        mapView?.onStop()
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun onStart() { // Activity onStart call must be passed to both MapView and MapKit instance.
        super.onStart()
        MapKitFactory.getInstance().onStart()
        mapView?.onStart()
    }

    override fun onObjectAdded(userLocationView: UserLocationView) {
        userLocationLayer?.setAnchor(
            PointF((mapView!!.width * 0.5).toFloat(), (mapView!!.height * 0.5).toFloat()),
            PointF((mapView!!.width * 0.5).toFloat(), (mapView!!.height * 0.83).toFloat())
        )
        userLocationView.arrow.setIcon(
            ImageProvider.fromResource(
                context, R.drawable.pin
            ),
            IconStyle().setAnchor(PointF(0.5f, 0.5f))
                .setRotationType(RotationType.ROTATE)
                .setZIndex(1f)
                .setScale(0.8f)
        )
        userLocationLayer?.isAutoZoomEnabled = false
        val pinIcon = userLocationView.pin.useCompositeIcon()
        pinIcon.setIcon(
            "pin",
            ImageProvider.fromResource(context, R.drawable.pin),
            IconStyle().setAnchor(PointF(0.5f, 0.5f))
                .setRotationType(RotationType.ROTATE)
                .setZIndex(1f)
                .setScale(0.8f)
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            userLocationView.accuracyCircle.fillColor = context!!.getColor(R.color.colorTransparent)
        }
    }

    override fun onObjectRemoved(view: UserLocationView) {}

    override fun onObjectUpdated(view: UserLocationView, event: ObjectEvent) {}

    private fun submitQuery(query: String) {
        searchSession = searchManager?.submit(
            query,
            VisibleRegionUtils.toPolygon(mapView!!.map.visibleRegion),
            SearchOptions(),
            this
        )
    }

    private fun submitQuery(query: Point) {
        searchSession = searchManager?.submit(
            query,
            1,
            SearchOptions(),
            this
        )
    }

    override fun onSearchError(p0: Error) {
        var errorMessage = getString(R.string.unknown_error_message)
        if (p0 is RemoteError) {
            errorMessage = getString(R.string.remote_error_message)
        } else if (p0 is NetworkError) {
            errorMessage = getString(R.string.network_error_message)
        }
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onSearchResponse(p0: Response) {

        searchResult.clearAndAddAll(p0.collection.children)
        Log.d("TTT", "${p0.collection.children.size}")
        resultAdapter.notifyDataSetChanged()
        if (p0.collection.children.isNotEmpty())
            layout_search.visible()
    }

    /*private fun getGeoCoder(latLng: Point): String {
        try {
            val addresses = Geocoder(context).getFromLocation(
                latLng.latitude,
                latLng.longitude,
                1
            )
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            val address = addresses[0]
            var street = if (address.subThoroughfare != null)
                "${address.subThoroughfare} " else ""
            street += if (address.thoroughfare != null && address.thoroughfare != "Unnamed Road")
                "${address.thoroughfare}, " else ""
            val area = if (address.adminArea != null) "${address.adminArea}, " else ""
            val subArea = if (address.subAdminArea != null) "${address.subAdminArea}, " else ""
            val district = if (address.subLocality != null) "${address.subLocality}, " else ""
            val city = if (address.locality != null) " ${address.locality}," else ""
            val country = if (address.countryName != null) address.countryName else ""
            return "$street$district$subArea$city$area$country"
        } catch (e: Exception) {
            return "NO"
        }
    }*/

    private fun showSoftKeyboard() {
        val inputMethodManager: InputMethodManager =
            activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        )
    }

    private fun hideSoftKeyboard() {
        activity().toolbar.search.gone()
        if (activity().currentFocus == null) {
            return
        }
        val inputMethodManager =
            activity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(activity().currentFocus?.windowToken, 0)
    }

    private fun submitRequest() {
        val options = DrivingOptions()
        val requestPoints = java.util.ArrayList<RequestPoint>()
        requestPoints.add(
            RequestPoint(
                TARGET_LOCATION,
                RequestPointType.WAYPOINT,
                null
            )
        )
        requestPoints.add(
            RequestPoint(
                FINAL_LOCATION,
                RequestPointType.WAYPOINT,
                null
            )
        )
        drivingSession = drivingRouter?.requestRoutes(requestPoints, options, this)
    }

    override fun onDrivingRoutesError(p0: Error) {
        var errorMessage = getString(R.string.unknown_error_message)
        if (p0 is RemoteError) {
            errorMessage = getString(R.string.remote_error_message)
        } else if (p0 is NetworkError) {
            errorMessage = getString(R.string.network_error_message)
        }
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onDrivingRoutes(p0: MutableList<DrivingRoute>) {
        Log.d("TTT", "${p0.size}")
        center_pin.gone()
        activity().toolbar.bt_ready.gone()
        val centre = Point(
            (TARGET_LOCATION.latitude + FINAL_LOCATION.latitude) / 2,
            (TARGET_LOCATION.longitude + FINAL_LOCATION.longitude) / 2
        )
        mapView!!.map.move(CameraPosition(centre, 15f, 0f, 0f))
        if (p0.isNotEmpty()) {
            mapObjects?.addPolyline(p0[0].geometry)
            bottomSheetBehavior.apply {
                choose_location.gone()
                choose_taxi.visible()
                tv_distance.text = p0[0].metadata.weight.distance.text
                tv_time.text = p0[0].metadata.weight.timeWithTraffic.text
            }
        }
        /*for (route in p0) {
            mapObjects!!.addPolyline(route.geometry)
        }*/
    }

    override fun onCameraPositionChanged(
        p0: Map,
        p1: CameraPosition,
        p2: CameraUpdateSource,
        p3: Boolean
    ) {
        viewModel.getDirection(
            mapView?.map?.cameraPosition?.target?.latitude,
            mapView?.map?.cameraPosition?.target?.longitude,
            getString(R.string.geocode_key)
        )
    }

    override fun onMapObjectTap(p0: MapObject, p1: Point): Boolean {
        Toast.makeText(requireContext(), "${p0.zIndex}", Toast.LENGTH_SHORT).show()
        return true
    }
}
