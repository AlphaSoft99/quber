package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.model.PostData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.adapters.NotificationAdapter
import alpha.soft.quber.ui.viewmodel.NotificationViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.screen_notification.*
import javax.inject.Inject

class NotificationScreen : Fragment(R.layout.screen_notification){

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: NotificationViewModel by viewModels()
    private lateinit var adapter : NotificationAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
    }

    private fun injectDependency(fragment: NotificationScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData(){

        viewModel.init(api)

        viewModel.postLiveData.observe(this, postObserver)
        viewModel.openNotification.observe(this, openNotificationObserver)

        adapter = NotificationAdapter(object : NotificationAdapter.ItemInterface{
            override fun itemClick(pos: Int) {
                viewModel.openNotification(pos)
            }
        })
        rv_notification_list.adapter = adapter
        rv_notification_list.layoutManager = LinearLayoutManager(context)
    }

    private val postObserver = Observer<List<PostData>> {
        adapter.submitList(it)
    }

    private val openNotificationObserver = Observer<Int> {
        //TODO open notification
    }
}

