package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HistoryViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil

    fun init(preferencesUtil: PreferencesUtil){
        preference= preferencesUtil
    }
}