package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfileViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil

    private val _openUpdateInfo = MutableLiveData<Unit>()
    val openUpdateInfo: LiveData<Unit> = _openUpdateInfo

    fun init(preferencesUtil: PreferencesUtil){
        preference= preferencesUtil
    }

    fun openUpdateInfo(){
        _openUpdateInfo.postValue(Unit)
    }

}