package alpha.soft.quber.ui.adapters

import alpha.soft.quber.ui.screens.AddCardTypeFragment
import alpha.soft.quber.ui.screens.HistoryTypeFragment
import alpha.soft.quber.ui.screens.OnBoardingTypeFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class HistoryPagerAdapter(
    fragmentActivity: FragmentActivity, private val count: Int
) :
    FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int = count

    override fun createFragment(position: Int): Fragment {
        if (position == 0)
        return OnBoardingTypeFragment()
        return HistoryTypeFragment()
    }
}