package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.ui.activities.MainActivity
import alpha.soft.quber.ui.viewmodel.SplashViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activitySplash
import alpha.soft.quber.utils.extensions.welcomeViewModel
import androidx.fragment.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import javax.inject.Inject

class SplashScreen : Fragment(R.layout.screen_splash) {

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: SplashViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        injectDependency(this)
        loadViews()
    }

    private fun injectDependency(fragment: SplashScreen) {
        (activitySplash().application as App).getApiComponent().inject(fragment)
    }

    private fun loadViews() {

        viewModel.init(preferencesUtil)

        viewModel.responseWidgetLiveData().observe(this, Observer {
            it.getContentIfNotHandled()?.let {

            }
        })
        viewModel.openOnBoardingLiveData().observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                findNavController().navigate(R.id.action_splashScreen_to_onBoardingScreen)
            }
        })
        viewModel.openMainLiveData().observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                welcomeViewModel().finish()
                context?.startActivity(Intent(context, MainActivity::class.java))
            }
        })
        viewModel.openLogInLiveData().observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                findNavController().navigate(R.id.action_splashScreen_to_loginScreen)
            }
        })
    }
}