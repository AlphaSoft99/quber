package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.model.AddCardData
import alpha.soft.quber.ui.adapters.AddCardPagerAdapter
import alpha.soft.quber.ui.viewmodel.AddCardViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import alpha.soft.quber.utils.extensions.mainViewModel
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.screen_add_card.*
import kotlinx.android.synthetic.main.tab_custom_view.view.*
import javax.inject.Inject


class AddCardScreen : Fragment(R.layout.screen_add_card){

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: AddCardViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
        bt_save.setOnClickListener {
            viewModel.saveCard()
        }
    }

    private fun loadData(){

        val list = arrayListOf(AddCardData(0, R.drawable.ic_visa1), AddCardData(0, R.drawable.ic_amazon), AddCardData(0, R.drawable.ic_paypal))
        mainViewModel().isLock(true)
        val pagerAdapter = AddCardPagerAdapter(activity(), list.size)
        vp_intro.adapter = pagerAdapter

        TabLayoutMediator(tabLayout, vp_intro) { tab, position ->
            val customView: View = layoutInflater.inflate(R.layout.tab_custom_view, null)
            customView.icon.setBackgroundResource(list[position].icon)
            tab.customView = customView
            vp_intro.setCurrentItem(tab.position, true)
        }.attach()

        vp_intro.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                // No boilerplate, only useful
            }
        })
        viewModel.saveCard.observe(this, saveCardObserver)
    }

    private fun injectDependency(fragment: AddCardScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private val saveCardObserver = Observer<Unit> {
        findNavController().navigateUp()
    }

    override fun onDestroy() {
        mainViewModel().isLock(false)
        super.onDestroy()
    }
}