package alpha.soft.quber.ui.adapters

import alpha.soft.quber.ui.screens.AddCardTypeFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class AddCardPagerAdapter(
    fragmentActivity: FragmentActivity, private val count: Int
) :
    FragmentStateAdapter(fragmentActivity) {
    override fun getItemCount(): Int = count

    override fun createFragment(position: Int): Fragment {
        return AddCardTypeFragment()
    }
}