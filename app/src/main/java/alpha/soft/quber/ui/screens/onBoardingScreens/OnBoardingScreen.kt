package alpha.soft.quber.ui.screens.onBoardingScreens

import alpha.soft.quber.R
import alpha.soft.quber.ui.viewmodel.OnBoardingViewModel
import alpha.soft.quber.utils.Constants
import alpha.soft.quber.utils.extensions.activitySplash
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import kotlinx.android.synthetic.main.screen_on_boarding.*

class OnBoardingScreen : Fragment(R.layout.screen_on_boarding) {

    private val viewViewModel: OnBoardingViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadViews()

        bt_skip.setOnClickListener { viewViewModel.open() }

        bt_next.setOnClickListener {
            viewViewModel.next()
        }
    }

    private fun loadViews() {

        viewViewModel.openLogInLiveData.observe(this, openLogInObserver)
        viewViewModel.onPageSelected.observe(viewLifecycleOwner, onPageSelectedObserver)

        val pagerAdapter = ScreenSlidePagerAdapter(activitySplash())
        vp_intro.adapter = pagerAdapter

        vp_intro.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewViewModel.onPageSelected(position)
                // No boilerplate, only useful
            }
        })
    }

    private val openLogInObserver = Observer<Unit> {
        findNavController().navigate(R.id.action_onBoardingScreen_to_loginScreen)
    }

    private val onPageSelectedObserver = Observer<Int> {
        vp_intro.currentItem = it
        page_iv.setSelected(it)
    }

    private inner class ScreenSlidePagerAdapter(fragmentActivity: FragmentActivity) :
            FragmentStateAdapter(fragmentActivity) {
        override fun getItemCount(): Int = Constants.INTRO_PAGE_COUNT //because I have three Fragments

        override fun createFragment(position: Int): Fragment {
            when (position) {
                0 -> return OnBoarding1()
                1 -> return OnBoarding2()
                2 -> return OnBoarding3()
                else -> Exception("Index Out").printStackTrace()
            }
            return OnBoarding1()
        }
    }
}