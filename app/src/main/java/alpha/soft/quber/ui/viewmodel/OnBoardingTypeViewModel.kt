package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.model.PostData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OnBoardingTypeViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil

    fun init(preferencesUtil: PreferencesUtil, api: PostApi){
        viewModelScope.launch(Dispatchers.IO) {
        }
        preference= preferencesUtil
    }
}