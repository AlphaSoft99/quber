package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.model.DriverChatData
import alpha.soft.quber.model.ChatData
import alpha.soft.quber.model.ClientChatData
import alpha.soft.quber.network.PostApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.random.Random

class ChatViewModel : ViewModel() {

    private var sampleId = Random.nextInt()
    private val _postLiveData = MutableLiveData<List<ChatData>>()
    val postLiveData: LiveData<List<ChatData>> = _postLiveData

    fun init(api: PostApi) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = api.getPosts()
            if (response.isSuccessful) {
                val d = response.body()!!
                val chatData = ArrayList<ChatData>()
                d.forEach {
                    chatData.add(if (it.id!! % 2 ==1) DriverChatData(it.id, it.title, it.body) else ClientChatData(it.id, it.title, it.body))
                }
                _postLiveData.postValue(chatData)
            } else {
                _postLiveData.postValue(emptyList())
            }
        }
    }

    fun sendMessage(text: String){
        if (text.isNotEmpty()){
            val myChatData  = ClientChatData(sampleId++, text, text)
            val d= postLiveData.value as ArrayList
            d.add(myChatData)
            _postLiveData.postValue(d)
        }
    }
}