package alpha.soft.quber.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SplashActivityViewModel : ViewModel() {

    private val _showProgress = MutableLiveData<Unit>()
    val showProgress: LiveData<Unit> = _showProgress

    private val _hideProgress = MutableLiveData<Unit>()
    val hideProgress: LiveData<Unit> = _hideProgress

     private val _finishActivity = MutableLiveData<Unit>()
    val finishActivity : LiveData<Unit> = _finishActivity

    fun finish() {
        _finishActivity.postValue(Unit)
    }

    fun showProgress() {
        _showProgress.postValue(Unit)
    }

    fun hideProgress() {
        _hideProgress.postValue(Unit)
    }

}