package alpha.soft.quber.ui.adapters

import alpha.soft.quber.R
import alpha.soft.quber.model.PostData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_history.view.*

class HistoryAdapter(private val itemInterface: ItemInterface) : ListAdapter<PostData, HistoryAdapter.ViewHolder>(DiffUtilImp) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    object DiffUtilImp : DiffUtil.ItemCallback<PostData>() {
        override fun areItemsTheSame(oldItem: PostData, newItem: PostData): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: PostData, newItem: PostData): Boolean {
            return oldItem == newItem
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind() {
            val d= getItem(adapterPosition)
            d?.let {
                itemView.apply {
                    tv_start_location.text = it.title
                    tv_end_location.text = it.title
                    tv_cost.text = "15 $"
                    tv_rating.text = "4.5"
                    tv_taxi_name.text = if ((d.id?:0)%2==0) "XL" else "Standard"
                    iv_taxi.setImageDrawable(context.getDrawable(if((d.id?:0)%2==0) R.drawable.ic_taxi_xl else R.drawable.ic_taxi_standart))
                    setOnClickListener {
                        itemInterface.itemClick(adapterPosition)
                    }
                }
            }
        }
    }

    interface ItemInterface{
        fun itemClick(pos: Int)
    }

}