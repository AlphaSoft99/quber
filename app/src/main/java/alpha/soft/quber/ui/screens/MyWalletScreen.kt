package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.viewmodel.MyWalletViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import alpha.soft.quber.utils.extensions.activitySplash
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.screen_my_wallet.*
import javax.inject.Inject

class MyWalletScreen : Fragment(R.layout.screen_my_wallet){

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: MyWalletViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
        bt_continue.setOnClickListener {
            viewModel.openAddCard()
        }
    }

    private fun loadData(){
        viewModel.init(preferencesUtil)
        viewModel.openAddCard.observe(this, openAddCardObserver)
    }

    private fun injectDependency(fragment: MyWalletScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private val openAddCardObserver = Observer<Unit> {
        findNavController().navigate(R.id.action_myWalletScreen_to_addCardScreen)
    }
}