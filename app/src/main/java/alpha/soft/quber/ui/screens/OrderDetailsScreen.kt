package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.viewmodel.OrderDetailsViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import javax.inject.Inject

class OrderDetailsScreen : Fragment(R.layout.screem_order_details) {

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: OrderDetailsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
    }

    private fun injectDependency(fragment: OrderDetailsScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData() {
        viewModel.init(preferencesUtil, api)
    }
}
