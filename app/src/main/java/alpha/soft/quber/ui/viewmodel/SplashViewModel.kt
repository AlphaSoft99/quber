package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.utils.Event
import alpha.soft.quber.utils.PreferencesUtil
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SplashViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil

    private val _openOnBoardingLiveData = MutableLiveData<Event<Unit>>()
    fun openOnBoardingLiveData(): LiveData<Event<Unit>> = _openOnBoardingLiveData

    private val _responseWidgetLiveData = MutableLiveData<Event<Unit>>()
    fun responseWidgetLiveData(): LiveData<Event<Unit>> = _responseWidgetLiveData

    private val _openLogInLiveData = MutableLiveData<Event<Unit>>()
    fun openLogInLiveData(): LiveData<Event<Unit>> = _openLogInLiveData

    private val _openMainLiveData = MutableLiveData<Event<Unit>>()
    fun openMainLiveData(): LiveData<Event<Unit>> = _openMainLiveData

    fun init(preferencesUtil: PreferencesUtil) {
        preference = preferencesUtil
        viewModelScope.launch(Dispatchers.Main) {
            delay(3000)
            withContext(coroutineContext) {
                when {
                    preference.checkOnline() -> _openLogInLiveData.value = Event(Unit)
                    preference.hasAccount() -> _openMainLiveData.value = Event(Unit)
                    else -> _openOnBoardingLiveData.value = Event(Unit)
                }
            }
        }
    }
}