package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.R
import alpha.soft.quber.model.UpdateInfoData
import android.content.Context
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class UpdateInfoModel : ViewModel() {

    private val _updatePhotoLiveData = MutableLiveData<Unit>()
    val updatePhotoLiveData : LiveData<Unit> = _updatePhotoLiveData

    private val _logOutLiveData= MutableLiveData<Unit>()
    val logOutLiveData: LiveData<Unit> = _logOutLiveData

    private val _showEmailError = MutableLiveData<String>()
    val showEmailError : LiveData<String> = _showEmailError

    private val _showFullNameError = MutableLiveData<String>()
    val showFullNameError  : LiveData<String> = _showFullNameError

    private val _showMobileNumberError = MutableLiveData<String>()
    val showMobileNumberError  : LiveData<String> = _showMobileNumberError

    private val _showAddressError = MutableLiveData<String>()
    val showAddressError  : LiveData<String> = _showAddressError

    private val _saveInfoLiveData= MutableLiveData<Unit>()
    val saveInfoLiveData: LiveData<Unit> = _saveInfoLiveData

   fun saveInfo(fullName: String, phoneNumber: String, email: String, address: String, street: String, context: Context){
        context.apply {
            if (fullName.isEmpty()) {
                _showFullNameError.postValue(getString(R.string.full_name_empty))
                return
            }
            if (phoneNumber.isEmpty()) {
                _showMobileNumberError.postValue(getString(R.string.phone_number_empty))
                return
            }
            if (email.isEmpty()  && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                _showEmailError.postValue(getString(R.string.email_is_error))
                return
            }
            if (address.isEmpty()) {
                _showAddressError.postValue(getString(R.string.address_empty))
                return
            }
            if (street.isEmpty()) {
                _showFullNameError.postValue(getString(R.string.street_empty))
                return
            }

            val updateInfoData = UpdateInfoData(fullName, phoneNumber, email, address, street)
            _saveInfoLiveData.postValue(Unit)
        }
    }

    fun updatePhoto(){
        _updatePhotoLiveData.postValue(Unit)
    }


    fun logOut() {
        _logOutLiveData.postValue(Unit)
    }

}