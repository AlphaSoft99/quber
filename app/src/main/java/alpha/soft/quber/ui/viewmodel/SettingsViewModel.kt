package alpha.soft.quber.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SettingsViewModel : ViewModel() {

    private val _changeLanguageDropDown = MutableLiveData<Boolean>()
    val changeLanguageDropDown: LiveData<Boolean> = _changeLanguageDropDown

    private val _aboutAppDropDown = MutableLiveData<Boolean>()
    val aboutAppDropDown: LiveData<Boolean> = _aboutAppDropDown

    private val _helpDropDown = MutableLiveData<Boolean>()
    val helpDropDown: LiveData<Boolean> = _helpDropDown

    private val _aboutFactoryDropDown = MutableLiveData<Boolean>()
    val aboutFactoryDropDown: LiveData<Boolean> = _aboutFactoryDropDown

    fun changeLanguageDropDown() {
        _changeLanguageDropDown.postValue(!(changeLanguageDropDown.value ?: false))
    }

    fun aboutAppDropDown() {
        _aboutAppDropDown.postValue(!(aboutFactoryDropDown.value ?: false))
    }

    fun helpDropDown() {
        _helpDropDown.postValue(!(helpDropDown.value ?: false))
    }

    fun aboutFactoryDropDown() {
        _aboutFactoryDropDown.postValue(!(aboutFactoryDropDown.value ?: false))
    }
}

