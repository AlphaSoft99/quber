package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.model.ChatData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.adapters.ChatAdapter
import alpha.soft.quber.ui.viewmodel.ChatViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import alpha.soft.quber.utils.extensions.to
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.screen_chat.*
import javax.inject.Inject

class ChatScreen : Fragment(R.layout.screen_chat){

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: ChatViewModel by viewModels()
    private lateinit var adapter : ChatAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
        bt_send.setOnClickListener {
            viewModel.sendMessage(et_user_type.to())
        }
    }

    private fun injectDependency(fragment: ChatScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData(){

        viewModel.init(api)

        viewModel.postLiveData.observe(this, postObserver)

        adapter = ChatAdapter(object : ChatAdapter.ItemInterface{
            override fun itemClick(pos: Int) {

            }
        })
        rv_chat_list.adapter = adapter
        rv_chat_list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, true)
    }

    private val postObserver = Observer<List<ChatData>> {
        adapter.submitList(it)
    }
}