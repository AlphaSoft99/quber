package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.model.PostData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.adapters.HistoryAdapter
import alpha.soft.quber.ui.viewmodel.HistoryTypeViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.screem_history_type.*
import javax.inject.Inject

class HistoryTypeFragment : Fragment(R.layout.screem_history_type) {

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: HistoryTypeViewModel by viewModels()
    private lateinit var adapter : HistoryAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
    }

    private fun injectDependency(fragment: HistoryTypeFragment) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData() {

        viewModel.init(preferencesUtil, api)
        viewModel.postLiveData.observe(viewLifecycleOwner, postHistoryDataObserver)
        viewModel.openHistoryDetails.observe(this, openHistoryDetailsObserver)
        adapter = HistoryAdapter(object : HistoryAdapter.ItemInterface{
            override fun itemClick(pos: Int) {
                viewModel.openHistoryDetails(pos)
            }
        })
        rv_list.adapter = adapter
        rv_list.layoutManager = LinearLayoutManager(context)
    }

    private val postHistoryDataObserver = Observer<List<PostData>>{
        adapter.submitList(it)
    }
    private val openHistoryDetailsObserver = Observer<Int>{
        findNavController().navigate(R.id.action_historyScreen_to_orderDetailsScreen)
    }
}
