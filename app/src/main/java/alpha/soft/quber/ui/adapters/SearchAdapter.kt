package alpha.soft.quber.ui.adapters

import alpha.soft.quber.R
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yandex.mapkit.GeoObjectCollection
import com.yandex.mapkit.search.SuggestItem
import kotlinx.android.synthetic.main.item_search.view.*

class SearchAdapter(private val data: List<GeoObjectCollection.Item>, private val itemInterface: ItemInterface) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false))
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: GeoObjectCollection.Item) {
                itemView.apply {
                    if (adapterPosition == 0) {
                        Log.d("TTT", "${item.obj?.name}")
                        Log.d("TTT", "${item.obj?.descriptionText}")
                        Log.d("TTT", "${item.obj?.geometry?.get(0)?.boundingBox}")
                        Log.d("TTT", "${item.obj?.geometry?.get(0)?.polyline}")
                        Log.d("TTT", "${item.obj?.boundingBox}")
                        Log.d("TTT", "${item.obj?.aref}")
                        Log.d("TTT", "${item.obj?.metadataContainer}")
                    }
                    tv_title.text = item.obj?.descriptionText
                    setOnClickListener {
                        itemInterface.itemClick(adapterPosition)
                    }
                }
        }
    }

    interface ItemInterface{
        fun itemClick(pos: Int)
    }
}