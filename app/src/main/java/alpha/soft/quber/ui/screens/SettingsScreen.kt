package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.viewmodel.SettingsViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import alpha.soft.quber.utils.extensions.onFinish
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.screen_settings.*
import javax.inject.Inject

class SettingsScreen : Fragment(R.layout.screen_settings){

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: SettingsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadView()

        bt_change_language.setOnClickListener {
            viewModel.changeLanguageDropDown()
        }
        bt_about_app.setOnClickListener {
            viewModel.aboutAppDropDown()
        }
        bt_help.setOnClickListener {
            viewModel.helpDropDown()
        }
        bt_about_factory.setOnClickListener {
            viewModel.aboutFactoryDropDown()
        }
    }

    private fun injectDependency(fragment: SettingsScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private fun loadView(){
        viewModel.changeLanguageDropDown.observe(this, changeLanguageObserver)
        viewModel.aboutAppDropDown.observe(this, aboutAppObserver)
        viewModel.helpDropDown.observe(this, helpObserver)
        viewModel.aboutFactoryDropDown.observe(this, aboutFactoryObserver)
    }

    private val changeLanguageObserver= Observer<Boolean> {
        el_change_language.toggle()
        bt_change_language.animate().onFinish {}.rotationBy(180f).setDuration(100).start()
    }

    private val aboutAppObserver= Observer<Boolean> {
        el_about_app.toggle()
        bt_about_app.animate().onFinish {}.rotationBy(180f).setDuration(100).start()
    }
    private val helpObserver= Observer<Boolean> {
        el_help.toggle()
        bt_help.animate().onFinish {}.rotationBy(180f).setDuration(100).start()
    }
    private val aboutFactoryObserver= Observer<Boolean> {
        el_about_factory.toggle()
        bt_about_factory.animate().onFinish {}.rotationBy(180f).setDuration(100).start()
    }

}