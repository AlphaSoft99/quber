package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.model.PostData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HistoryTypeViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil


    private val _postLiveData = MutableLiveData<List<PostData>>()
    val postLiveData: LiveData<List<PostData>> = _postLiveData

    private val _openHistoryDetails = MutableLiveData<Int>()
    val openHistoryDetails: LiveData<Int> = _openHistoryDetails

    fun init(preferencesUtil: PreferencesUtil, api: PostApi){
        viewModelScope.launch(Dispatchers.IO) {
            val response = api.getPosts()
            if (response.isSuccessful) {
                _postLiveData.postValue(response.body())
            } else {
                _postLiveData.postValue(emptyList())
            }
        }
        preference= preferencesUtil
    }

    fun openHistoryDetails(pos: Int) {
        _openHistoryDetails.postValue(pos)
    }
}