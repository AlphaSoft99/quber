package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.R
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.utils.Constants.TAG
import alpha.soft.quber.utils.Event
import alpha.soft.quber.utils.PreferencesUtil
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import java.util.concurrent.TimeUnit

class VerifyViewModel : ViewModel() {

    private lateinit var postApi: PostApi
    private lateinit var preference: PreferencesUtil
    private var verificationInProgress = false
    private var storedVerificationId = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private lateinit var auth: FirebaseAuth
    private lateinit var phoneNumber: String
    private val _showFailedMessage = MutableLiveData<String>()
    val showFailedMessage: LiveData<String> = _showFailedMessage

    private val _openMain = MutableLiveData<Event<Unit>>()
    val openMain: LiveData<Event<Unit>> = _openMain

    private val _showSMSCodeError = MutableLiveData<Int>()
    val showSMSCodeError: LiveData<Int> = _showSMSCodeError

    fun init(
        api: PostApi,
        preferencesUtil: PreferencesUtil,
        phoneNumber: String,
        activity: AppCompatActivity
    ) {
        this.postApi = api
        preference = preferencesUtil
        auth = FirebaseAuth.getInstance()
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:$credential")
                verificationInProgress = false
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Log.w(TAG, "onVerificationFailed", e)
                verificationInProgress = false
                if (e is FirebaseAuthInvalidCredentialsException) {
                    _showFailedMessage.postValue("Invalid phone number.")
                } else if (e is FirebaseTooManyRequestsException) {
                    _showFailedMessage.postValue("Quota exceeded.")
                }
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:$verificationId")
                storedVerificationId = verificationId
                resendToken = token
            }
        }
        this.phoneNumber = phoneNumber
        if (phoneNumber != "") {
            Log.d(TAG, phoneNumber)
            PhoneAuthProvider.getInstance()
                .verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, activity, callbacks)
        }
    }

    private fun sendSMSCode(sms_code: String) {
        val credential = PhoneAuthProvider.getCredential(storedVerificationId, sms_code)
        signInWithPhoneAuthCredential(credential)
    }

    fun verify(sms_code: String) {
        if (sms_code.isNotEmpty())
            sendSMSCode(sms_code)
        else
            _showSMSCodeError.postValue(R.string.sms_code_empty)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithCredential:success")
                    val user = task.result?.user
                    preference.saveUser(user.toString())
                    preference.setAccount(true)
                    preference.setPhoneNumber(phoneNumber)
                    _openMain.postValue(Event(Unit))
                } else {
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        _showFailedMessage.postValue("Invalid code.")
                    }
                }
            }
    }
}