package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.model.AddressData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MapViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil
    private lateinit var api: PostApi
    private var job : Job? = null

    private val _postLiveData = MutableLiveData<AddressData>()
    val postLiveData: LiveData<AddressData> = _postLiveData

    private val _queryLiveData = MutableLiveData<String>()
    val queryLiveData: LiveData<String> = _queryLiveData

    fun getDirection(lat: Double?, lng: Double?, key: String){
        val directionUrl = "https://geocode-maps.yandex.ru/1.x/?"
        val url = directionUrl
            .plus("apikey=")
            .plus(key)
            .plus("&format=json&geocode=")
            .plus(lat)
            .plus(",")
            .plus(lng)
            .plus("&sco=latlong")

//        if (job == null || job?.isCompleted == true) {
            job = viewModelScope.launch(Dispatchers.IO) {
                val response = api.getDirection(url)
                if (response.isSuccessful) {
                    _postLiveData.postValue(response.body())
                } else {
                    _postLiveData.postValue(null)
                }
            }
//        }
    }

    fun init(preferencesUtil: PreferencesUtil, api: PostApi){

        this.api = api
        this.preference= preferencesUtil


    }

    fun search(query: String?) {
        _queryLiveData.postValue(query)
    }
}