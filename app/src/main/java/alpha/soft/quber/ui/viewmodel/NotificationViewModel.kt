package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.model.ChatData
import alpha.soft.quber.model.PostData
import alpha.soft.quber.network.PostApi
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NotificationViewModel : ViewModel() {

    private val _postLiveData = MutableLiveData<List<PostData>>()
    val postLiveData: LiveData<List<PostData>> = _postLiveData

    private val _openNotification = MutableLiveData<Int>()
    val openNotification: LiveData<Int> = _openNotification

    fun init(api: PostApi) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = api.getPosts()
            if (response.isSuccessful) {
                _postLiveData.postValue(response.body())
            } else {
                _postLiveData.postValue(emptyList())
            }
        }
    }

    fun openNotification(pos: Int) {
        _openNotification.postValue(pos)
    }
}