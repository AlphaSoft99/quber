package alpha.soft.quber.ui.activities

import alpha.soft.quber.R
import alpha.soft.quber.base.BaseActivity
import alpha.soft.quber.model.tracking.TrackingLocation
import alpha.soft.quber.model.tracking.TrackingModel
import alpha.soft.quber.services.AndroidLocationServices
import alpha.soft.quber.ui.viewmodel.MainActivityViewModel
import alpha.soft.quber.utils.Constants
import alpha.soft.quber.utils.Constants.LOCATION_PERMISSION
import alpha.soft.quber.utils.GpsUtils
import alpha.soft.quber.utils.GpsUtils.onGpsListener
import alpha.soft.quber.utils.Utils
import alpha.soft.quber.utils.extensions.gone
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.yandex.mapkit.MapKitFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.activity_main_drawer.*

@SuppressLint("RtlHardcoded")
class MainActivity : BaseActivity() {
    private lateinit var intentService: Intent
    private var updateLocationList = ArrayList<TrackingModel>()

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val location = intent.getSerializableExtra("location") as TrackingModel
            updateLocationList.add(
                TrackingModel(
                    "User ID",
                    location.created_at,
                    TrackingLocation(
                        location.location.lat,
                        location.location.long
                    )
                )
            )
        }
    }

    private val navController by lazy { findNavController(R.id.main_nav_host) }
    private val appBarConfiguration by lazy {
        AppBarConfiguration(
            setOf(
                R.id.mapScreen,
                R.id.myWalletScreen,
                R.id.profileScreen,
                R.id.historyScreen,
                R.id.notificationScreen,
                R.id.settingsScreen
            ),
            drawerLayout
        )
    }
    private lateinit var viewModel: MainActivityViewModel
    private var doubleBackToExitPressedOnce = false
    private var isGPS = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.setApiKey(getString(R.string.yandex_key))
        MapKitFactory.initialize(this)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        checkPermission()
        drawerLayout.openDrawer(Gravity.LEFT)
        loadViews()
        navigationItemClick()
    }

    @SuppressLint("LogNotTimber")
    private fun loadViews() {

        viewModel = ViewModelProviders.of(this)[MainActivityViewModel::class.java]

        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout)
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration)

        drawerLayout.setScrimColor(Color.TRANSPARENT)
        drawerLayout.addDrawerListener(object : DrawerLayout.SimpleDrawerListener() {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

                // Scale the View based on current slide offset
                val diffScaledOffset = slideOffset * (1 - 0.9f)
                val offsetScale = 1 - diffScaledOffset
                contentView.scaleX = offsetScale
                contentView.scaleY = offsetScale

                // Translate the View, accounting for the scaled width
                val xOffset = drawerView.width * slideOffset
                val xOffsetDiff = contentView.width * diffScaledOffset / 2
                val xTranslation = xOffset - xOffsetDiff
                contentView.translationX = xTranslation
            }
        })

        viewModel.closeDrawer.observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                drawerLayout.closeDrawer(Gravity.LEFT)
            }
        })

        viewModel.openDrawer.observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                drawerLayout.openDrawer(Gravity.LEFT)
            }
        })
        viewModel.logout.observe(this, showLogoutDialogObserver)
        viewModel.isLockDrawer.observe(this, isLockDrawerObserver)

        if (checkAndRequestPermissions())
            if (!Constants.isServiceWorking) {
                intentService = Intent(this, AndroidLocationServices::class.java)
                intentService.action = Constants.ACTION_START_SERVICE
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    startForegroundService(intentService)
                else startService(intentService)
                Constants.isServiceWorking = true
            }
    }

    private val isLockDrawerObserver = Observer<Boolean> {
        drawerLayout.setDrawerLockMode(if (it) DrawerLayout.LOCK_MODE_LOCKED_CLOSED else DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    private fun navigationItemClick() {
        menu_tv.setOnClickListener {
            if (navController.currentDestination?.id != R.id.TVScreen) {
                navController.navigateUp()
                navController.navigate(R.id.action_mapScreen_to_TVScreen)
                drawerLayout.closeDrawer(Gravity.LEFT)
                hideSoftKeyboard()
            }
        }

        menu_profile.setOnClickListener {
            if (navController.currentDestination?.id != R.id.profileScreen) {
                navController.navigateUp()
                navController.navigate(R.id.action_mapScreen_to_profileFragment)
                drawerLayout.closeDrawer(Gravity.LEFT)
                hideSoftKeyboard()
            }
        }

        menu_wallet.setOnClickListener {
            if (navController.currentDestination?.id != R.id.myWalletScreen) {
                navController.navigateUp()
                navController.navigate(R.id.action_mapScreen_to_myWalletScreen)
                drawerLayout.closeDrawer(Gravity.LEFT)
                hideSoftKeyboard()
            }
        }

        menu_history.setOnClickListener {
            if (navController.currentDestination?.id != R.id.historyScreen) {
                navController.navigateUp()
                navController.navigate(R.id.action_mapScreen_to_historyScreen)
                drawerLayout.closeDrawer(Gravity.LEFT)
                hideSoftKeyboard()
            }
        }

        menu_notifications.setOnClickListener {
            if (navController.currentDestination?.id != R.id.notificationScreen) {
                navController.navigateUp()
                navController.navigate(R.id.action_mapScreen_to_notificationScreen)
                drawerLayout.closeDrawer(Gravity.LEFT)
                hideSoftKeyboard()
            }
        }

        menu_settings.setOnClickListener {
            if (navController.currentDestination?.id != R.id.settingsScreen) {
                navController.navigateUp()
                navController.navigate(R.id.action_mapScreen_to_settingsScreen)
                drawerLayout.closeDrawer(Gravity.LEFT)
                hideSoftKeyboard()
            }
        }

        menu_logout.setOnClickListener {
            viewModel.logout()
        }
    }

    private val showLogoutDialogObserver = Observer<Unit>{
        AlertDialog.Builder(this)
            .setTitle(R.string.logout)
            .setMessage(R.string.logout_text)
            .setPositiveButton(R.string.ok) { _, _ ->
                finish()
            }
            .setNegativeButton(R.string.cancel, null)
            .setCancelable(false)
            .show()
    }

    override fun onRestart() {
        super.onRestart()
        if (navController.currentDestination?.id == R.id.mapScreen)
            viewModel.openDrawer()
    }

    private fun hideSoftKeyboard() {
        toolbar.search.gone()
        if (currentFocus == null) {
            return
        }
        val inputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }

    private fun checkPermission() {
        if (Utils.hasPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            GpsUtils(this).turnGPSOn(object : onGpsListener {
                override fun gpsStatus(isGPSEnable: Boolean) {
                    // turn on GPS
                    isGPS = isGPSEnable
                }
            })
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                Constants.REQUEST_PERMISSION
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        run loop@{
            grantResults.forEach {
                if (it != 0){
                    checkPermission()
                    return@loop
                }
            }
        }
        GpsUtils(this).turnGPSOn(object : onGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                // turn on GPS
                isGPS = isGPSEnable
            }
        })
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.GPS_REQUEST) {
                isGPS = true // flag maintain before get location
            }
        }
    }

    private fun checkAndRequestPermissions(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    return true
                } else ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                    LOCATION_PERMISSION
                )
            } else ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                ), LOCATION_PERMISSION
            )
        } else if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                showAlertDialog(
                    title = "Soʻrov ruxsati",
                    message = "Ilova sizning joylashuvingizni, restoran va mijozlarning joylashuvini toʼgʼri koʼrsatishi uchun ilovaga GPS maʼlumotlarga ruxsat bering",
                    positiveClick = DialogInterface.OnClickListener { dialog, _ ->
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            LOCATION_PERMISSION
                        )
                        dialog.dismiss()
                    }
                )
            } else ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION
            )
            return false
        }
        return true
    }

    private fun stopService() {
        if (Constants.isServiceWorking) {
            stopService(intentService)
            Constants.isServiceWorking = false
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver, IntentFilter(AndroidLocationServices.str_receiver))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadcastReceiver)
        //TODO if you need
        stopService()
    }

    override fun onBackPressed() {
        toolbar.search.gone()
        when {
            drawerLayout.isDrawerOpen(Gravity.LEFT) -> {
                viewModel.closeDrawer()
            }
            navController.graph.startDestination == navController.currentDestination?.id -> {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed()
                    return
                }
                this.doubleBackToExitPressedOnce = true
                Toast.makeText(this, getString(R.string.double_click), Toast.LENGTH_SHORT).show()
                Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

}
