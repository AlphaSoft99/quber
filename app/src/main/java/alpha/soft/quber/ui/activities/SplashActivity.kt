package alpha.soft.quber.ui.activities

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.base.BaseActivity
import alpha.soft.quber.ui.viewmodel.SplashActivityViewModel
import android.os.Bundle
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger

class SplashActivity : BaseActivity() {

    private lateinit var viewModel: SplashActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        FacebookSdk.sdkInitialize(FacebookSdk.getApplicationContext())
//        AppEventsLogger.activateApp(this)
        applyLocale()
        setFullScreen()
        setContentView(R.layout.activity_splash)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

        viewModel = ViewModelProviders.of(this)[SplashActivityViewModel::class.java]

        viewModel.finishActivity.observe(this, finishObserver)
        viewModel.showProgress.observe(this, showProgressObserver)
        viewModel.hideProgress.observe(this, hideProgressObserver)
    }

    private fun applyLocale() {
        val app = application as App
        app.applyLocale(this)
    }

    private val finishObserver = Observer<Unit> {
        finish()
    }

    private val showProgressObserver = Observer<Unit> {
        showProgress()
    }

    private val hideProgressObserver = Observer<Unit> {
        hideProgress()
    }
}