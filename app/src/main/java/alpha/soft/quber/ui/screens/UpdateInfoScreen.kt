package alpha.soft.quber.ui.screens

import alpha.soft.quber.R
import alpha.soft.quber.ui.viewmodel.UpdateInfoModel
import alpha.soft.quber.utils.extensions.*
import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.enums.EPickType
import id.zelory.compressor.Compressor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.bottom_sheet_get_picture.*
import kotlinx.android.synthetic.main.screen_update_info.*
import java.io.File

class UpdateInfoScreen : Fragment(R.layout.screen_update_info) {

    private val viewModel: UpdateInfoModel by viewModels()
    private var imageFile : File? = null
    private var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>? = null
    private var tempImgUrl: String? = null
        set(value) {
            field = value
            setPic(value)
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadView()

        bt_update_info.setOnClickListener {
            viewModel.saveInfo(et_first_name.to(), et_last_name.to(), et_email.to(), et_address.to(), et_street_address.to(), context!!)
        }

        bt_update_photo.setOnClickListener {
            viewModel.updatePhoto()
            background.visible()
        }

        bt_camera.setOnClickListener {
            pictureClicked(EPickType.CAMERA)
        }

        bt_gallery.setOnClickListener {
            pictureClicked(EPickType.GALLERY)
        }
        background.setOnClickListener {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            background.gone()
        }
    }

    private fun loadView() {

        mainViewModel().isLock(true)
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_layout)

        et_first_name.onTextChanged(tv_full_name_error)
        et_last_name.onTextChanged(tv_mobile_number_error)
        et_email.onTextChanged(tv_email_error)
        et_address.onTextChanged(tv_address_error)
        et_street_address.onTextChanged(tv_address_error)

        viewModel.updatePhotoLiveData.observe(this, updatePhotoObserver)
        viewModel.showFullNameError.observe(this, fullNameObserver)
        viewModel.showMobileNumberError.observe(this, mobilePhoneObserver)
        viewModel.showEmailError.observe(this, emailObserver)
        viewModel.showAddressError.observe(this, addressObserver)
        viewModel.logOutLiveData.observe(this, logOutObserver)
        viewModel.saveInfoLiveData.observe(this, saveInfoObserver)
    }

    private fun pictureClicked(type: EPickType) {
        background.gone()
        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        layout_picture_background.gone()
        pickImage(type)
    }

    private val updatePhotoObserver = Observer<Unit> {
        if (bottomSheetBehavior?.state != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
        }
        else {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }

    private val fullNameObserver= Observer<String> {
        tv_full_name_error.text = it
    }

    private val mobilePhoneObserver= Observer<String> {
        tv_mobile_number_error.text = it
    }

    private val emailObserver= Observer<String> {
        tv_email_error.text = it
    }

    private val addressObserver= Observer<String> {
        tv_address_error.text = it
    }


    private val logOutObserver= Observer<Unit> {
        showLogOutDialog()
    }

    private val saveInfoObserver= Observer<Unit> {
//        activity().onBackPressed()
    }

    private fun showLogOutDialog(){
        AlertDialog.Builder(context!!)
            .setTitle(R.string.logout)
            .setMessage(R.string.logout_text)
            .setPositiveButton(R.string.ok) { _, _ ->
                activity().finish()
            }
            .setNegativeButton(R.string.cancel, null)
            .show()

    }

    //get Image
    private fun pickImage(type: EPickType) {
        val setup = PickSetup().apply {
            setPickTypes(type)
        }

        setup.title = getString(R.string.choose_app)
        setup.galleryButtonText = getString(R.string.dialog_gallery)
        setup.cameraButtonText = getString(R.string.dialog_camera)
        setup.cancelText= getString(R.string.cancel)
        setup.cameraIcon = R.drawable.ic_camera
        setup.galleryIcon= R.drawable.ic_gallery

        PickImageDialog.build(setup)
            .setOnPickResult {
                tempImgUrl = it.path
            }
            .setOnPickCancel {
            }.show(activity())
    }

    private fun setPic(imgUrl: String?) {
        if (imgUrl != null) {
            val file = File(imgUrl)
            val dispose = Compressor(context)
                .compressToFileAsFlowable(file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    imageFile = it
                    iv_avatar.setImageURI(Uri.fromFile(it))
                }, { throwable ->
                    throwable.printStackTrace()
                    throwable.message?.let { /*showError() */ }
                })
        }
    }
    override fun onDestroy() {
        mainViewModel().isLock(false)
        super.onDestroy()
    }
}