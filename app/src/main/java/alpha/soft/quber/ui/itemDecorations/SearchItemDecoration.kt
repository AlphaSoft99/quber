package alpha.soft.quber.ui.itemDecorations

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration


class SearchItemDecoration() : ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val itemPosition = parent.getChildAdapterPosition(view)

        outRect.right = 16
        outRect.left = 16
        when (itemPosition) {
            0 -> {
                outRect.top = 24
                outRect.bottom = 32
            }
            parent.childCount-1 -> {
                outRect.bottom = 32
            }
            else -> {
                outRect.bottom = 32
            }
        }
    }
}