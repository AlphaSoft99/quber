package alpha.soft.quber.ui.adapters

import alpha.soft.quber.R
import alpha.soft.quber.model.DriverChatData
import alpha.soft.quber.model.ChatData
import alpha.soft.quber.model.ChatDataEnum
import alpha.soft.quber.model.ClientChatData
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_driver_message.view.*
import kotlinx.android.synthetic.main.item_client_chat.view.*

class ChatAdapter(private val itemInterface: ItemInterface) :
    ListAdapter<ChatData, ChatAdapter.ViewHolder>(DiffUtilImp) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(when (ChatDataEnum.getType(viewType)) {
                    ChatDataEnum.DriverChatData -> R.layout.item_driver_message
                    ChatDataEnum.ClientChat -> R.layout.item_client_chat }, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        when (ChatDataEnum.getType(getItem(position).getItemType())) {
            ChatDataEnum.ClientChat -> {
                holder.bind(getItem(position) as ClientChatData)
            }
            ChatDataEnum.DriverChatData -> {
                holder.bind(getItem(position) as DriverChatData)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).getItemType()
    }

    object DiffUtilImp : DiffUtil.ItemCallback<ChatData>() {
        override fun areItemsTheSame(oldItem: ChatData, newItem: ChatData): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ChatData, newItem: ChatData): Boolean {
            return oldItem != newItem
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(clientChat: ClientChatData) {
            itemView.apply {
                tv_user_text.text = clientChat.body
            }
        }

        fun bind(driverChat: DriverChatData) {
            itemView.apply {
                tv_driver_text.text = driverChat.body
            }
        }
    }

    interface ItemInterface {
        fun itemClick(pos: Int)
    }

}