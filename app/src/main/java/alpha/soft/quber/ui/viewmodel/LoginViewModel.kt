package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.R
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.utils.Constants
import alpha.soft.quber.utils.Event
import alpha.soft.quber.utils.PreferencesUtil
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.*


class LoginViewModel : ViewModel() {

    private lateinit var postApi: PostApi
    private lateinit var gso: GoogleSignInOptions
    private lateinit var preference: PreferencesUtil
    private lateinit var auth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var provider: OAuthProvider.Builder

    fun init(api: PostApi, preferencesUtil: PreferencesUtil, context: Context?) {
        this.postApi = api
        preference = preferencesUtil
        provider = OAuthProvider.newBuilder("twitter.com")
        provider.addCustomParameter("lang", "uz")
        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context?.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(context!!, gso)
        auth = FirebaseAuth.getInstance()
        auth.setLanguageCode("uz")
        signOut()
    }

    private val _openMain = MutableLiveData<Event<Unit>>()
    val openMain: LiveData<Event<Unit>> = _openMain

    private val _showFailedMessage = MutableLiveData<String>()
    val showFailedMessage: LiveData<String> = _showFailedMessage

    private val _showGoogleSignDialog = MutableLiveData<Intent>()
    val showGoogleSignDialog: LiveData<Intent> = _showGoogleSignDialog

    private val _openVerifyScreen = MutableLiveData<Event<String>>()
    val openVerifyScreen: LiveData<Event<String>> = _openVerifyScreen

    private val _showPhoneNumberError = MutableLiveData<Int>()
    val showPhoneNumberError: LiveData<Int> = _showPhoneNumberError

    private val _showProgress = MutableLiveData<Unit>()
    val showProgress: LiveData<Unit> = _showProgress

    private val _hideProgress = MutableLiveData<Unit>()
    val hideProgress: LiveData<Unit> = _hideProgress

    fun showFailedMessage(message: String) {
        _showFailedMessage.postValue(message)
    }

    //GoogleSignIn
    fun google() {
        _showGoogleSignDialog.postValue(googleSignInClient.signInIntent)
    }

    fun firebaseAuthWithGoogle(acct: GoogleSignInAccount?) {
        Log.d(Constants.TAG, "firebaseAuthWithGoogle:" + acct?.id)
        val credential = GoogleAuthProvider.getCredential(acct?.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(Constants.TAG, "signInWithCredential:success${auth.currentUser?.uid}")
                    val user = auth.currentUser
                    preference.saveUser(user.toString())
                    preference.setAccount(true)
                    _openMain.postValue(Event(Unit))
                } else {
                    Log.d(Constants.TAG, "signInWithCredential:failure", task.exception)
                    _showFailedMessage.postValue("Authentication Failed")
                }
            }
    }

    //PhoneNumber
    private fun sendPhoneNumber(phoneNumber: String) {
        _openVerifyScreen.postValue(Event(phoneNumber))
    }

    fun signIn(phoneNumber: String) {
        if (phoneNumber.isNotEmpty())
            sendPhoneNumber(phoneNumber)
        else
            _showPhoneNumberError.postValue(R.string.phone_number_empty)
    }

    //Facebook
    fun handleFacebookAccessToken(token: AccessToken) {
        Log.d(Constants.TAG, "handleFacebookAccessToken:$token")
        _showProgress.postValue(Unit)
        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(Constants.TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    preference.saveUser(user.toString())
                    preference.setAccount(true)
                    _openMain.postValue(Event(Unit))
                } else {
                    Log.w(Constants.TAG, "signInWithCredential:failure", task.exception)
                    _showFailedMessage.postValue("Authentication failed.")
                }
                _hideProgress.postValue(Unit)
            }
    }


    //Twitter
    fun twitter(activity: AppCompatActivity) {
        signOut()
        val pendingResultTask = auth.pendingAuthResult
        pendingResultTask?.addOnSuccessListener {
            // User is signed in.
            // IdP data available in
            // authResult.getAdditionalUserInfo().getProfile().
            // The OAuth access token can also be retrieved:
            // authResult.getCredential().getAccessToken().
            // The OAuth secret can be retrieved by calling:
            // authResult.getCredential().getSecret().
            Log.d(Constants.TAG, "signInWithCredential:success")
            preference.saveUser(it.toString())
            preference.setAccount(true)
            _openMain.postValue(Event(Unit))
        }
            ?.addOnFailureListener {
                Log.d(Constants.TAG, "pendingResultTask$it")
            }


        auth.startActivityForSignInWithProvider(activity, provider.build())
            .addOnSuccessListener {
                // User is signed in.
                // IdP data available in
                // authResult.getAdditionalUserInfo().getProfile().
                // The OAuth access token can also be retrieved:
                // authResult.getCredential().getAccessToken().
                // The OAuth secret can be retrieved by calling:
                // authResult.getCredential().getSecret().
                Log.d(Constants.TAG, "signInWithCredential:success")
                preference.saveUser(it.toString())
                preference.setAccount(true)
                _openMain.postValue(Event(Unit))
            }
            .addOnFailureListener {
                Log.d(Constants.TAG, "startActivityForSignInWithProvider$it")
            }

        // The user is already signed-in.
        val firebaseUser = auth.currentUser
        firebaseUser?.startActivityForLinkWithProvider(activity, provider.build())
            ?.addOnSuccessListener {
                // Twitter credential is linked to the current user.
                // IdP data available in
                // authResult.getAdditionalUserInfo().getProfile().
                // The OAuth access token can also be retrieved:
                // authResult.getCredential().getAccessToken().
                // The OAuth secret can be retrieved by calling:
                // authResult.getCredential().getSecret().
                Log.d(Constants.TAG, "signInWithCredential:success")
                preference.saveUser(it.toString())
                preference.setAccount(true)
                _openMain.postValue(Event(Unit))
            }
            ?.addOnFailureListener {
                Log.d(Constants.TAG, "firebaseUser:startActivityForSignInWithProvider$it")
            }

        // The user is already signed-in.
        firebaseUser?.startActivityForReauthenticateWithProvider(activity, provider.build())
            ?.addOnSuccessListener {
                // User is re-authenticated with fresh tokens and
                // should be able to perform sensitive operations
                // like account deletion and email or password
                // update.
                Log.d(Constants.TAG, "signInWithCredential:success")
                preference.saveUser(it.toString())
                preference.setAccount(true)
                _openMain.postValue(Event(Unit))
            }
            ?.addOnFailureListener {
                Log.d(Constants.TAG, "firebaseUser:startActivityForReauthenticateWithProvider$it")
            }
    }

    //cancel
    private fun signOut() {
        auth.signOut()
        googleSignInClient.signOut().addOnCompleteListener {}
        LoginManager.getInstance().logOut()
    }

    private fun revokeAccess() {
        auth.signOut()
        googleSignInClient.revokeAccess().addOnCompleteListener {}
        LoginManager.getInstance().logOut()
    }
}