package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.utils.Event
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    var isOpen: Boolean = false

    private val _showProgress = MutableLiveData<Unit>()
    val showProgress: LiveData<Unit> = _showProgress

    private val _hideProgress = MutableLiveData<Unit>()
    val hideProgress: LiveData<Unit> = _hideProgress

    private val _closeDrawer = MutableLiveData<Event<Unit>>()
    val closeDrawer : LiveData<Event<Unit>> = _closeDrawer

     private val _logout = MutableLiveData<Unit>()
    val logout : LiveData<Unit> = _logout

    private val _openDrawer = MutableLiveData<Event<Unit>>()
    val openDrawer : LiveData<Event<Unit>> = _openDrawer

    private val _isLockDrawer = MutableLiveData<Boolean>()
    val isLockDrawer : LiveData<Boolean> = _isLockDrawer

    init {
        closeDrawer()
    }

    fun isLock(b: Boolean){
        _isLockDrawer.postValue(b)
    }

    fun closeDrawer() {
        _closeDrawer.postValue(Event(Unit))
    }

    fun showProgress() {
        _showProgress.postValue(Unit)
    }

    fun hideProgress() {
        _hideProgress.postValue(Unit)
    }

    fun openDrawer() {
        _openDrawer.postValue(Event(Unit))
    }

    fun logout() {
        _logout.postValue(Unit)
    }

}