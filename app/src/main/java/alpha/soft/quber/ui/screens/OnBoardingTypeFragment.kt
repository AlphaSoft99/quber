package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.model.PostData
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.adapters.HistoryAdapter
import alpha.soft.quber.ui.viewmodel.HistoryTypeViewModel
import alpha.soft.quber.ui.viewmodel.OnBoardingTypeViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.screem_history_type.*
import javax.inject.Inject

class OnBoardingTypeFragment : Fragment(R.layout.screem_on_boarding) {

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: OnBoardingTypeViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
    }

    private fun injectDependency(fragment: OnBoardingTypeFragment) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData() {
        viewModel.init(preferencesUtil, api)
    }
}
