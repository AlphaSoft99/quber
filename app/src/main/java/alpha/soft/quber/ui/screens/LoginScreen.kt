package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.network.PostApi
import alpha.soft.quber.ui.activities.MainActivity
import alpha.soft.quber.ui.viewmodel.LoginViewModel
import alpha.soft.quber.utils.Constants
import alpha.soft.quber.utils.Constants.TAG
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activitySplash
import alpha.soft.quber.utils.extensions.onTextChanged
import alpha.soft.quber.utils.extensions.to
import alpha.soft.quber.utils.extensions.visible
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.screen_login.*
import javax.inject.Inject


class LoginScreen : Fragment(R.layout.screen_login) {

    @Inject
    lateinit var api: PostApi
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: LoginViewModel by viewModels()
    private lateinit var callbackManager: CallbackManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()

        bt_sign_in.setOnClickListener {
            viewModel.signIn(et_phone_number.to())
        }

        bt_google.setOnClickListener {
            viewModel.google()
        }

        bt_twitter.setOnClickListener {
            viewModel.twitter(activitySplash())
        }
    }

    private fun injectDependency(fragment: LoginScreen) {
        (activitySplash().application as App).getApiComponent().inject(fragment)
    }

    private fun loadData() {
        viewModel.init(api, preferencesUtil, context)

        et_phone_number.onTextChanged(tv_error)
        bt_facebook.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_facebook, 0, 0, 0)

        viewModel.openMain.observe(this, Observer {
            it.getContentIfNotHandled()?.also {
                activitySplash().finish()
                startActivity(Intent(context, MainActivity::class.java))
            }
        })
        viewModel.openVerifyScreen.observe(this, Observer { it ->
            it.getContentIfNotHandled()?.also {
                findNavController().navigate(
                    R.id.action_loginScreen_to_verifyScreen,
                    bundleOf(Constants.EXTRA_PHONE to it)
                )
            }
        })
        viewModel.showPhoneNumberError.observe(viewLifecycleOwner, showPhoneNumberErrorObserver)
        viewModel.showGoogleSignDialog.observe(viewLifecycleOwner, showGoogleSignDialogObserver)
        viewModel.showProgress.observe(viewLifecycleOwner, showProgressObserver)
        viewModel.hideProgress.observe(viewLifecycleOwner, hideProgressObserver)
        viewModel.showFailedMessage.observe(viewLifecycleOwner, showFailedMessageObserver)

        callbackManager = CallbackManager.Factory.create()
        bt_facebook.fragment = this
        bt_facebook.setReadPermissions("public_profile", "user_friends", "email")
        bt_facebook.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d(TAG, "facebook:onSuccess:$loginResult")
                viewModel.handleFacebookAccessToken(loginResult.accessToken)
            }
            override fun onCancel() { Log.d(TAG, "facebook:onCancel") }
            override fun onError(error: FacebookException) { Log.d(TAG, "facebook:onError", error) }
        })
    }

    private val showPhoneNumberErrorObserver = Observer<Int> {
        tv_error.visible()
        tv_error.setText(it)
    }


    private val showGoogleSignDialogObserver = Observer<Intent> {
        activitySplash().showProgress()
        startActivityForResult(it, Constants.RC_SIGN_IN)
    }


    private val showProgressObserver = Observer<Unit> {
        activitySplash().showProgress()
    }


    private val hideProgressObserver = Observer<Unit> {
        activitySplash().hideProgress()
    }


    private val showFailedMessageObserver = Observer<String> {
        Snackbar.make(bt_sign_in, it, Snackbar.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        activitySplash().hideProgress()
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                viewModel.firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                viewModel.showFailedMessage("Google sign in failed")
            }
        }
    }
}