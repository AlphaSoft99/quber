package alpha.soft.quber.ui.screens

import alpha.soft.quber.R
import alpha.soft.quber.ui.viewmodel.ProfileViewModel
import alpha.soft.quber.utils.Constants
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.synthetic.main.screem_add_card_type.*
import kotlinx.android.synthetic.main.screem_profile.*

class ProfileFragment : Fragment(R.layout.screem_profile) {

    private val viewModel: ProfileViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
        bt_update_info.setOnClickListener {
            viewModel.openUpdateInfo()
        }
    }

    private fun loadData() {
        viewModel.openUpdateInfo.observe(this, openUpdateInfoObserver)
    }

    private val openUpdateInfoObserver = Observer<Unit>{
        findNavController().navigate(R.id.action_profileFragment_to_updateInfoScreen)
    }
}
