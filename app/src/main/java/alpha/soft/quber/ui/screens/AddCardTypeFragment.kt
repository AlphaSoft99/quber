package alpha.soft.quber.ui.screens

import alpha.soft.quber.R
import alpha.soft.quber.utils.Constants
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.redmadrobot.inputmask.MaskedTextChangedListener
import kotlinx.android.synthetic.main.screem_add_card_type.*

class AddCardTypeFragment : Fragment(R.layout.screem_add_card_type) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
    }

    private fun loadData() {
        et_card_date.addTextChangedListener(
            MaskedTextChangedListener(
                Constants.CARD_DATE_FORMAT, false,
                et_card_date,
                null,
                null
            )
        )
        et_card_serial.addTextChangedListener(
            MaskedTextChangedListener(
                Constants.CARD_SERIAL_FORMAT, false,
                et_card_serial,
                null,
                null
            )
        )
    }
}
