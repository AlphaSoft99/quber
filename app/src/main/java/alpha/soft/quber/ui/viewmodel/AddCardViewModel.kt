package alpha.soft.quber.ui.viewmodel

import alpha.soft.quber.utils.PreferencesUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AddCardViewModel : ViewModel() {

    private lateinit var preference: PreferencesUtil

    private val _saveCard = MutableLiveData<Unit>()
    val saveCard: LiveData<Unit> = _saveCard

    fun init(preferencesUtil: PreferencesUtil){
        preference= preferencesUtil
    }

    fun saveCard(){
        _saveCard.postValue(Unit)
    }

}