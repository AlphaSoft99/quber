package alpha.soft.quber.ui.screens

import alpha.soft.quber.App
import alpha.soft.quber.R
import alpha.soft.quber.model.AddCardData
import alpha.soft.quber.ui.adapters.AddCardPagerAdapter
import alpha.soft.quber.ui.adapters.HistoryPagerAdapter
import alpha.soft.quber.ui.viewmodel.AddCardViewModel
import alpha.soft.quber.ui.viewmodel.HistoryViewModel
import alpha.soft.quber.utils.PreferencesUtil
import alpha.soft.quber.utils.extensions.activity
import alpha.soft.quber.utils.extensions.mainViewModel
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.screen_add_card.*
import kotlinx.android.synthetic.main.tab_custom_view.view.*
import javax.inject.Inject


class HistoryScreen : Fragment(R.layout.screen_history){

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private val viewModel: HistoryViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependency(this)
        loadData()
    }

    private fun loadData(){

        val list = arrayListOf(getString(R.string.on_boarding), getString(R.string.history))
        val pagerAdapter = HistoryPagerAdapter(activity(), list.size)
        vp_intro.adapter = pagerAdapter

        TabLayoutMediator(tabLayout, vp_intro) { tab, position ->
            tab.text = list[position]
            vp_intro.setCurrentItem(tab.position, true)
        }.attach()

        vp_intro.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                // No boilerplate, only useful
            }
        })
    }

    private fun injectDependency(fragment: HistoryScreen) {
        (activity().application as App).getApiComponent().inject(fragment)
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}