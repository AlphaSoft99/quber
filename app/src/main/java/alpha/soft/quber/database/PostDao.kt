package alpha.soft.quber.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PostDao {
    @get:Query("SELECT * FROM post")
    val allPost: List<Post>

    @Insert
    fun insertAll(vararg post: Post)
}