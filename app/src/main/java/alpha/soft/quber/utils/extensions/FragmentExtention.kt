package alpha.soft.quber.utils.extensions

import alpha.soft.quber.ui.activities.MainActivity
import alpha.soft.quber.ui.activities.SplashActivity
import alpha.soft.quber.ui.viewmodel.MainActivityViewModel
import alpha.soft.quber.ui.viewmodel.SplashActivityViewModel
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders

fun Fragment.onCreateMenu(){
    setHasOptionsMenu(true)
    setMenuVisibility(true)
    setHasOptionsMenu(true)
}

fun Fragment.welcomeViewModel() = ViewModelProviders.of(activitySplash())[SplashActivityViewModel::class.java]
fun Fragment.mainViewModel() = ViewModelProviders.of(activity())[MainActivityViewModel::class.java]

fun Fragment.activity() = activity as MainActivity
fun Fragment.activitySplash() = activity as SplashActivity
