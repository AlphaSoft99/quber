package alpha.soft.quber.utils

/**
 * Utility class for constants
 */
object Constants {
    const val LOCATION_PERMISSION = 1
    var isServiceWorking: Boolean = false
    val ACTION_START_SERVICE: String = "SERVICE_START"
    val GPS_REQUEST: Int = 998
    val REQUEST_PERMISSION: Int = 999
    val USER: String = "USER"
    val RC_SIGN_IN: Int = 9901
    val INTRO_PAGE_COUNT: Int = 3
    val CHECK_ONLINE: String = "CHECK_ONLINE"
    val HAS_ACCOUNT: String = "HAS ACCOUNT"
    const val KEY = "qPNIYxwpmT6jWEWYurxjGLIJKX7tNSCq"
    const val VERIFICATION_CODE_LENGTH = 6
    const val PHONE_FORMAT_UZ = "+998[00]-[000]-[00]-[00]"
    const val MONEY_FORMAT = "[999] [999] [999]"
    const val NUMBER_FORMAT = "[000] [000] [000]"
    const val SMS_FORMAT = "[0] [0] [0] [0] [0] [0]"
    const val PROMO_CODE_FORMAT = "[0] [0] [0] [0] [0]"
    const val PHONE_PREFIX_UZ = "+998"
    const val EXTRA_PHONE = "phone"
    const val TAG = "TTT"
    const val LATITUDE = "latitude"
    const val LONGITUDE = "longitude"
    const val CARD_SERIAL_FORMAT = "[0000] [0000] [0000] [0000]"
    const val CARD_DATE_FORMAT = "[00] {/} [00]"
    const val LANGUAGE_CODE_EN = "en"
    const val LANGUAGE_CODE_RU = "ru"
    const val LANGUAGE_CODE_UZ = "uz"


    const val COUNTRY_CODE_UZB = "UZ"
    var DEFAULT_LATITUDE = 41.311151
    var DEFAULT_LONGITUDE = 69.279687
}
