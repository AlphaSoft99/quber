package alpha.soft.quber.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.text.TextUtils
import androidx.annotation.RequiresApi
import java.security.GeneralSecurityException
import com.securepreferences.SecurePreferences
import com.tozny.crypto.android.AesCbcWithIntegrity

class PreferencesUtil(
    private val context: Context,
    private val preferences: SharedPreferences,
    private val securedPreferencesName: String
){
    private val ABOUT_ROAD ="About road"
    private val NEW_ORDER_NOTIFICATION= "NEW_ORDER_NOTIFICATION"
    private val COMMENTS_NOTIFICATION_ACTIVE= "COMMENTS_NOTIFICATION_ACTIVE"
    private val PREFS_IS_FIRST_LAUNCH = "isFirstLaunch"
    private val PREFS_PHONE_NUMBER = "phoneNumber"
    private val PREFS_FIRST_NAME = "firstName"
    private val FIREBASE_TOKEN= "firebase token"
    private val NEW_COMMENT_NOTIFICATION_ID = "NEW_COMMENT_NOTIFICATION_ID"
    private val PREFS_UI_LOCALE = "uiLocale"
    private val HAS_PROFILE= "has profile"
    private val NOTIFICATION_CHECKED= "isChecked"
    private val LOCATION = "location"
    private val FRIEND_PROMO_CODE= "friend promo code"
    private val PROMO_CODE= "promo code"
    private val VERSION_APP= "VERSION_APP"
    private val IS_CHOOSE_LANGUAGE= "is language"
    private val HAS_NEW_ORDER = "has new order"
    private var secure: Secure? = null
    private var authTokenCached: String? = null

    @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
    fun saveTokenData(
        token: String, phone: String,
        fullName: String
    ) {
        preferences.edit()
            .putString(PREFS_FIRST_NAME, fullName)
            .putString(PREFS_PHONE_NUMBER, phone)
            .apply()
        // Reset secure preferences
        secure = null
        setAuthToken(token)
    }

    fun clearAll() {
        setFirstLaunch(true)
        getSecure()!!.clearAll()
        authTokenCached = null
    }


    fun hasAuthToken(): Boolean {
        return !(getAuthToken()?.isEmpty()?:false)
    }

    private fun setAuthToken(token: String) {
        authTokenCached = token
        getSecure()!!.authToken = token
    }

    fun getAuthToken(): String? {
        if (authTokenCached == null) {
            authTokenCached = getSecure()!!.authToken
        }
        return if (authTokenCached != null) authTokenCached else ""
    }

    fun isFirstLaunch(): Boolean {
        return preferences.getBoolean(PREFS_IS_FIRST_LAUNCH, true)
    }

    fun setFirstLaunch(isFirst: Boolean){
        preferences.edit().putBoolean(PREFS_IS_FIRST_LAUNCH, isFirst).apply()
    }

    fun getPhoneNumber(): String {
        return preferences.getString(PREFS_PHONE_NUMBER, "")!!.trim { it <= ' ' }
    }


    fun setPhoneNumber(phoneNumber: String){
        preferences.edit().putString(PREFS_PHONE_NUMBER, "").apply()
    }

    fun getUiLocale(): String {
        return preferences.getString(PREFS_UI_LOCALE, Constants.LANGUAGE_CODE_RU)?:Constants.LANGUAGE_CODE_RU
    }

    @SuppressLint("NewApi")
    fun setUiLocale(locale: String) {
        preferences.edit()
            .putString(PREFS_UI_LOCALE, locale)
            .apply()
    }

    private fun getSecure(): Secure? {
        if (secure == null && securedPreferencesName != "") {
            var password = getPhoneNumber()
            if (TextUtils.isEmpty(password)) {
                password = "default"
            }
            secure = Secure(context, securedPreferencesName, password)
        }
        return secure
    }

    fun hasProfile(): Boolean {
        return preferences.getBoolean(HAS_PROFILE, false)
    }

    fun hasProfile(has : Boolean){
        preferences.edit().putBoolean(HAS_PROFILE, has).apply()
    }

    fun getNotification(): Boolean {
        return preferences.getBoolean(NOTIFICATION_CHECKED, true)
    }

    fun setNotification(checked: Boolean) {
        preferences.edit().putBoolean(NOTIFICATION_CHECKED, checked).apply()
    }

    fun getLocation() = preferences.getString(LOCATION, "Yozilmadi")

    fun setLocation(location: String?) {
        preferences.edit().putString(LOCATION, location).apply()
    }

    fun isChooseLanguage(): Boolean {
        return preferences.getBoolean(IS_CHOOSE_LANGUAGE, false)
    }

    fun setChooseLanguage(choose: Boolean){
        preferences.edit().putBoolean(IS_CHOOSE_LANGUAGE, choose).apply()
    }
/*
    fun setFriendPromoCode(promoCode: String) {
        preferences.edit().putString(FRIEND_PROMO_CODE, promoCode).apply()
    }*/

    fun setPromoCode(promoCode: String) {
        preferences.edit().putString(PROMO_CODE, promoCode).apply()
    }

//    fun getFriendPromoCode()= preferences.getString(FRIEND_PROMO_CODE, "")

    fun getPromoCode()= preferences.getString(PROMO_CODE, "")
    fun hasNewOrder(): Boolean {
        return preferences.getBoolean(HAS_NEW_ORDER, true)
    }

    fun getFToken(): String {
        return preferences.getString(FIREBASE_TOKEN, "")?:""
    }

    fun setFToken(token : String){
        preferences.edit().putString(FIREBASE_TOKEN, token).apply()
    }

    fun setNewCommentsNotification(longExtra: Long) {
        preferences.edit().putLong(NEW_COMMENT_NOTIFICATION_ID, longExtra).apply()
    }

    fun getVersionApp(): String {
        return preferences.getString(VERSION_APP, "4.0")?:"4.0"
    }

    fun setCommentsNotificationActive(b: Boolean) {
        preferences.edit().putBoolean(COMMENTS_NOTIFICATION_ACTIVE, b).apply()
    }

    fun getCommentsNotification(): Long {
        return preferences.getLong(NEW_COMMENT_NOTIFICATION_ID, 0L)
    }

    fun aboutRoad(b: Boolean) {
        preferences.edit().putBoolean(ABOUT_ROAD, b).apply()
    }

    fun getAboutRoad() : Boolean{
        return preferences.getBoolean(ABOUT_ROAD, false)
    }

    fun getNewOrderNotification(): Boolean {
        return preferences.getBoolean(NEW_ORDER_NOTIFICATION, true)
    }

    fun setNewOrderNotification(isBoolean: Boolean){
        preferences.edit().putBoolean(NEW_ORDER_NOTIFICATION, isBoolean).apply()
    }

    fun hasAccount(): Boolean {
        return getSecure()?.hasAccount()?:false
    }

    fun setAccount(b: Boolean) {
        getSecure()?.setAccount(b)
    }

    fun checkOnline(): Boolean {
        return preferences.getBoolean(Constants.CHECK_ONLINE, false)
    }

    fun saveUser(user: String) {
        preferences.edit().putString(Constants.USER, user).apply()
    }

    private class Secure(context: Context, name: String, password: String) {

        private var securePreferences: SecurePreferences? = null

        var authToken: String?
            get() = securePreferences?.getString(AUTH_TOKEN, null)
            @RequiresApi(Build.VERSION_CODES.GINGERBREAD)
            set(token) {
                securePreferences?.edit()?.putString(AUTH_TOKEN, token)?.apply()
            }

        init {
            try {
                val key = AesCbcWithIntegrity.generateKeyFromPassword(password, "@%^~".toByteArray(), 1000)
                securePreferences = SecurePreferences(context, key, name)
            } catch (e: GeneralSecurityException) {
            }

        }

        fun clearAll() {
            securePreferences?.edit()?.clear()?.apply()
        }

        fun hasAccount(): Boolean {
            return securePreferences?.getBoolean(Constants.HAS_ACCOUNT, false)?:false
        }

        fun setAccount(b: Boolean){
            securePreferences?.edit()?.putBoolean(Constants.HAS_ACCOUNT, b)?.apply()
        }
        companion object {
            private const val AUTH_TOKEN = "auth_token"
            private const val LANGUAGE = "language"
            private const val TYPE_ROLE ="role"
        }
    }
}

