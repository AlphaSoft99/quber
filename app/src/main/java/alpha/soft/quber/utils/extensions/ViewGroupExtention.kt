package alpha.soft.quber.utils.extensions

import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun ViewGroup.addView(@LayoutRes resId: Int): View {
    val view = inflate(resId)
    addView(view)
    return view
}
